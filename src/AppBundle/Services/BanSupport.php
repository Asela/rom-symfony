<?php

namespace AppBundle\Services;

use AppBundle\Controller\ENUM\BanDetails;
use AppBundle\Controller\ENUM\Gamestate;
use AppBundle\Entity\GameAccount;
use AppBundle\Entity\User;
use AppBundle\Entity\WebActionLog;
use AppBundle\Entity\WebBan;
use Doctrine\ORM\EntityManager;

class BanSupport
{

    /**
     * Entity Manager
     *
     * @var EntityManager
     */
    protected $_em;

    public function __construct(EntityManager $em)
    {
        $this->_em = $em;
    }

    /**
     * Bans the User(master account) and all it's GameAccounts
     * @param integer $src
     * @param User $user
     * @param string $reason
     */
    public function banUser($src, User $user, $reason) {
        $user->setState(Gamestate::MASTER_BAN);
        $this->_em->persist(new WebActionLog($src, $user->getMasterId(),0,BanDetails::TYPE_MASTER_BAN,$reason));

        /** @var GameAccount $gameAccount */
        foreach($user->getGameAccounts() as $gameAccount) {
            $gameAccount->setState(Gamestate::WEB_BAN);
            $this->_em->persist(new WebBan($gameAccount->getAccountId()));
        }

        $this->_em->flush();
    }

    /**
     * UnBans the User(master account) and all it's GameAccounts
     * @param integer $src
     * @param User $user
     * @param string $reason
     */
    public function unbanUser($src, User $user, $reason) {
        $user->setState(Gamestate::OK);
        $this->_em->persist(new WebActionLog($src, $user->getMasterId(),0,BanDetails::TYPE_MASTER_UNBAN,$reason));

        /** @var GameAccount $gameAccount */
        foreach($user->getGameAccounts() as $gameAccount) {
            $gameAccount->setState(Gamestate::OK);
            $webBan = $this->_em->getRepository('AppBundle:WebBan')->find($gameAccount->getAccountId());
            if ($webBan)
                $this->_em->remove($webBan);
        }

        $this->_em->flush();
    }


    /**
     * Bans a GameAccount
     * @param integer $src
     * @param GameAccount $gameAccount
     * @param string $reason
     */
    public function banGameAccount($src, GameAccount $gameAccount, $reason) {
        $gameAccount->setState(Gamestate::WEB_BAN);
        $this->_em->persist(new WebActionLog($src, $gameAccount->getMasterId(),$gameAccount->getAccountId(),BanDetails::TYPE_BAN,$reason));
        $this->_em->persist(new WebBan($gameAccount->getAccountId()));
        $this->_em->flush();
    }

    /**
     * UnBans a GameAccount
     * @param integer $src
     * @param GameAccount $gameAccount
     * @param string $reason
     */
    public function unbanGameAccount($src, GameAccount $gameAccount, $reason) {
        if ($gameAccount->getMasterAccount()->getState() == Gamestate::MASTER_BAN) {
            $this->_em->persist(new WebActionLog($src, $gameAccount->getMasterId(),$gameAccount->getAccountId(),BanDetails::TYPE_UNBAN_FAIL,BanDetails::MASTERBANNED));
        } else {
            if ($gameAccount->getMasterAccount()->getState() == Gamestate::EMAIL_UNCONFIRMED) {
                $gameAccount->setState(Gamestate::EMAIL_UNCONFIRMED);
            } else {
                $gameAccount->setState(Gamestate::OK);
            }
            $this->_em->persist(new WebActionLog($src, $gameAccount->getMasterId(),$gameAccount->getAccountId(),BanDetails::TYPE_UNBAN,$reason));

            $webBan = $this->_em->getRepository('AppBundle:WebBan')->find($gameAccount->getAccountId());
            if ($webBan)
                $this->_em->remove($webBan);
            
        }
        $this->_em->flush();
    }
}
