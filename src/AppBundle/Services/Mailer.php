<?php

namespace AppBundle\Services;

use AppBundle\Entity\PaypalLog;
use AppBundle\Entity\GameAccount;
use Symfony\Component\Templating\EngineInterface;

class Mailer
{
    /** @var EngineInterface  */
    private $_templating;

    /** @var \Swift_Mailer  */
    private $_mailer;

    /** @var \Swift_Mailer  */
    private $_broad_mailer;

    /** @var \Swift_Mailer  */
    private $_amazon;

    public function __construct(\Swift_Mailer $mailer, \Swift_Mailer $broad_mailer, \Swift_Mailer $amazon, EngineInterface $templating)
    {
        $this->_templating = $templating;
        $this->_mailer = $mailer;
        $this->_broad_mailer = $broad_mailer;
        $this->_amazon = $amazon;
    }

    /**
     * @param string $email
     * @param string $code
     */
    public function sendConfirmationMail($email, $code)
    {
        /** @var \Swift_Mime_Message $message */

        $message = \Swift_Message::newInstance()
            ->setSubject('Rise of Midgard - Validate your account!')
            ->setFrom(['no-reply@riseofmidgard.com' => 'Rise of Midgard'])
            ->setTo($email)
            ->setBody(
                $this->_templating->render(
                    'AppBundle:Emails:email_confirmation.html.twig',['code' => $code]
                ),
                'text/html'
            );
        /* @var \Swift_Message $message */
        $message->addPart(
                $this->_templating->render(
                    'AppBundle:Emails:email_confirmation.txt.twig',['code' => $code]
                ),
                'text/plain'
            );

        $this->_broad_mailer->send($message);
    }

    /**
     * @param string $email
     */
    public function sendValidationMail($email)
    {
        /** @var \Swift_Mime_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject('Rise of Midgard - Now we can get started!')
            ->setFrom(['no-reply@riseofmidgard.com' => 'Rise of Midgard'])
            ->setTo($email)
            ->setBody(
                $this->_templating->render('AppBundle:Emails:email_validated.html.twig'),
                'text/html'
            );
        /* @var \Swift_Message $message */
        $message->addPart(
            $this->_templating->render('AppBundle:Emails:email_validated.txt.twig'),
            'text/plain'
        );
        $this->_broad_mailer->send($message);
    }

    /**
     * @param string $email
     * @param string $code
     */
    public function sendPasswordReset($email, $code)
    {
        /** @var \Swift_Mime_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject('Rise of Midgard - Password Reset')
            ->setFrom(['no-reply@riseofmidgard.com' => 'Rise of Midgard'])
            ->setTo($email)
            ->setBody(
                $this->_templating->render(
                    'AppBundle:Emails:password_reset.html.twig',['code' => $code]
                ),
                'text/html'
            );
        /* @var \Swift_Message $message */
        $message->addPart(
            $this->_templating->render(
                'AppBundle:Emails:password_reset.txt.twig',['code' => $code]
            ),
            'text/plain'
        );
        $this->_broad_mailer->send($message);
    }

    /**
     * @param PaypalLog $paypal_log
     * @param GameAccount $gameAccount
     * @param integer $credits_given
     */
    public function sendDonationSuccess(PaypalLog $paypal_log, GameAccount $gameAccount, $credits_given)
    {
        /** @var \Swift_Mime_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject('Rise of Midgard - Your $'.$paypal_log->getMcGross().' order is complete!')
            ->setFrom(['no-reply@riseofmidgard.com' => 'Rise of Midgard'])
            ->setTo($paypal_log->getPayerEmail())
            ->setBcc('billing.riseofmidgard+pointssuccess@gmail.com')
            ->setBody(
                $this->_templating->render(
                    'AppBundle:Emails:donation_confirmation.html.twig',[
                        'payment_date' => $paypal_log->getPaymentDate(),
                        'txn_id' => $paypal_log->getTxnId(),
                        'payer_email' => $paypal_log->getPayerEmail(),
                        'country' => $gameAccount->getMasterAccount()->getCountryName(),
                        'credits_given' => $credits_given,
                        'gross' => $paypal_log->getMcGross(),
                        'gamelogin' => $gameAccount->getUserId(),
                        'user_email' => $gameAccount->getMasterAccount()->getEmail()
                    ]
                ),
                'text/html'
            );

        /* @var \Swift_Message $message */
        $message->addPart(
            $this->_templating->render(
                'AppBundle:Emails:donation_confirmation.txt.twig',[
                    'payment_date' => $paypal_log->getPaymentDate(),
                    'txn_id' => $paypal_log->getTxnId(),
                    'payer_email' => $paypal_log->getPayerEmail(),
                    'country' => $gameAccount->getMasterAccount()->getCountryName(),
                    'credits_given' => $credits_given,
                    'gross' => $paypal_log->getMcGross(),
                    'gamelogin' => $gameAccount->getUserId(),
                    'user_email' => $gameAccount->getMasterAccount()->getEmail()
                ]
            ),
            'text/plain'
        );

        $this->_broad_mailer->send($message);
    }


    /**
     * @param PaypalLog $paypal_log
     */
    public function sendDonationFail(PaypalLog $paypal_log)
    {
        /** @var \Swift_Mime_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject('Rise of Midgard - Order: Manual Processing Required')
            ->setFrom(['no-reply@riseofmidgard.com' => 'Rise of Midgard'])
            ->setTo($paypal_log->getPayerEmail())
            ->setBcc('billing.riseofmidgard+pointsfail@gmail.com')
            ->setBody(
                $this->_templating->render(
                    'AppBundle:Emails:donation_fail.html.twig',[
                        'payment_date' => $paypal_log->getPaymentDate(),
                        'txn_id' => $paypal_log->getTxnId(),
                        'payer_email' => $paypal_log->getPayerEmail(),
                        'country' => $paypal_log->getAddressCountry(),
                        'credits_given' => 0,
                        'gross' => $paypal_log->getMcGross(),
                        'currency' => $paypal_log->getMcCurrency()
                    ]
                ),
                'text/html'
            );

        /* @var \Swift_Message $message */
        $message->addPart(
            $this->_templating->render(
                'AppBundle:Emails:donation_fail.txt.twig',[
                    'payment_date' => $paypal_log->getPaymentDate(),
                    'txn_id' => $paypal_log->getTxnId(),
                    'payer_email' => $paypal_log->getPayerEmail(),
                    'country' => $paypal_log->getAddressCountry(),
                    'credits_given' => 0,
                    'gross' => $paypal_log->getMcGross(),
                    'currency' => $paypal_log->getMcCurrency()
                ]
            ),
            'text/plain'
        );

        $this->_broad_mailer->send($message);
    }

    /**
     * @param GameAccount $gameAccount
     * @param integer $credit_change
     * @param string $reason
     */
    public function sendAdminPointUpdate(GameAccount $gameAccount, $credit_change, $reason)
    {
        /** @var \Swift_Mime_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject('Rise of Midgard - Cash Points Delivered!')
            ->setFrom(['no-reply@riseofmidgard.com' => 'Rise of Midgard'])
            ->setTo($gameAccount->getEmail())
            ->setBcc('billing.riseofmidgard+pointsadmin@gmail.com')
            ->setBody(
                $this->_templating->render(
                    'AppBundle:Emails:admin_point_update.html.twig',[
                        'gamelogin' => $gameAccount->getUserId(),
                        'credit_change' => $credit_change,
                        'reason' => $reason
                    ]
                ),
                'text/html'
            );

        /* @var \Swift_Message $message */
        $message->addPart(
            $this->_templating->render(
                'AppBundle:Emails:admin_point_update.txt.twig',[
                    'gamelogin' => $gameAccount->getUserId(),
                    'credit_change' => $credit_change,
                    'reason' => $reason
                ]
            ),
            'text/plain'
        );

        $this->_broad_mailer->send($message);
    }

    /**
     * @param string $email
     * @param string $gamelogin
     */
    public function sendBirthday($email, $gamelogin)
    {
        /** @var \Swift_Mime_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject('Rise of Midgard - Happy Birthday!')
            ->setFrom(['no-reply@riseofmidgard.com' => 'Rise of Midgard'])
            ->setTo($email)
            ->setBody(
                $this->_templating->render(
                    'AppBundle:Emails:birthday.html.twig',[
                        'gamelogin' => $gamelogin
                    ]
                ),
                'text/html'
            );

        /* @var \Swift_Message $message */
        $message->addPart(
            $this->_templating->render(
                'AppBundle:Emails:birthday.txt.twig',[
                    'gamelogin' => $gamelogin
                ]
            ),
            'text/plain'
        );

        $this->_broad_mailer->send($message);
    }

    /**
     * @param string $emails
     */
    public function sendReleaseDateDom($email)
    {
        /** @var \Swift_Mime_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject('Dawn of Midgard comes back on January 6th!')
            ->setFrom(['no-reply@riseofmidgard.com' => 'Rise of Midgard'])
            ->setTo($email)
            ->setBody(
                $this->_templating->render('AppBundle:Emails/bulk:release_date_for_dom.html.twig'),
                'text/html'
            );

        /* @var \Swift_Message $message */
        $message->addPart(
            $this->_templating->render('AppBundle:Emails/bulk:release_date_for_dom.txt.twig'),
            'text/plain'
        );

        $this->_broad_mailer->send($message);
    }

    /**
     * @param string $emails
     */
    public function sendReleaseDateRom($email)
    {
        /** @var \Swift_Mime_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject('Rise of Midgard is officially launching this January 6th!')
            ->setFrom(['no-reply@riseofmidgard.com' => 'Rise of Midgard'])
            ->setTo($email)
            ->setBody(
                $this->_templating->render('AppBundle:Emails/bulk:release_date_for_rom.html.twig'),
                'text/html'
            );

        /* @var \Swift_Message $message */
        $message->addPart(
            $this->_templating->render('AppBundle:Emails/bulk:release_date_for_rom.txt.twig'),
            'text/plain'
        );

        $this->_amazon->send($message);
    }
}
