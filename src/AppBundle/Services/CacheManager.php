<?php

namespace AppBundle\Services;


use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class CacheManager
{
    // 30 minutes
    const EXPIRE30M = 1800;

    // 5 minutes
    const EXPIRE5M = 300;

    // 1 minute
    const EXPIRE1M = 60;

    // 30 seconds
    const EXPIRE30S = 30;

    /**
     * @var EntityManager $_em
     */
    protected $_em;

    /**
     * @var FilesystemAdapter $_cache
     */
    protected $_cache;

    /**
     * @var Connection $_conn
     */
    protected $_conn;

    public function __construct(EntityManager $em, FilesystemAdapter $cache, Connection $conn)
    {
        $this->_em = $em;
        $this->_cache = $cache;
        $this->_conn = $conn;
    }

    /**
     * Returns cached value for online count.
     * @return null|int
     */
    public function getOnlineCount()
    {
        $onlineCount = $this->_cache->getItem('onlineCount');
        if (!$onlineCount->isHit()) {
            $num = $this->_em->getRepository('AppBundle:Character')->countOnlineAccounts();
            $onlineCount->set($num);
            $onlineCount->expiresAfter(CacheManager::EXPIRE5M);
            $this->_cache->save($onlineCount);
        } else {
            $num = $onlineCount->get();
        }

        return $num;
    }

    /**
     * Returns cached value for daily active count.
     * @return null|int
     */
    public function getDailyActive()
    {
        $dailyActive = $this->_cache->getItem('dailyActive');
        if (!$dailyActive->isHit()) {
            $num = $this->_em->getRepository('AppBundle:GameAccount')->countDailyActiveUsers();
            $dailyActive->set($num);
            $dailyActive->expiresAfter(CacheManager::EXPIRE5M);
            $this->_cache->save($dailyActive);
        } else {
            $num = $dailyActive->get();
        }

        return $num;
    }

    /**
     * Returns cached value for weekly active count.
     * @return null|int
     */
    public function getWeeklyActive()
    {
        $weeklyActive = $this->_cache->getItem('weeklyActive');
        if (!$weeklyActive->isHit()) {
            $num = $this->_em->getRepository('AppBundle:GameAccount')->countWeeklyActiveUsers();
            $weeklyActive->set($num);
            $weeklyActive->expiresAfter(CacheManager::EXPIRE5M);
            $this->_cache->save($weeklyActive);
        } else {
            $num = $weeklyActive->get();
        }

        return $num;
    }

    /**
     * Returns cached value for total number of master accounts.
     * @return null|int
     */
    public function getTotalMasterAccounts()
    {
        $totalMasterAccounts = $this->_cache->getItem('totalMasterAccounts');
        if (!$totalMasterAccounts->isHit()) {
            $num = $this->_em->getRepository('AppBundle:User')->countTotalAccounts();
            $totalMasterAccounts->set($num);
            $totalMasterAccounts->expiresAfter(CacheManager::EXPIRE30M);
            $this->_cache->save($totalMasterAccounts);
        } else {
            $num = $totalMasterAccounts->get();
        }

        return $num;
    }

    /**
     * Returns cached value for total number of game accounts.
     * @return null|int
     */
    public function getTotalGameAccounts()
    {
        $totalGameAccounts = $this->_cache->getItem('totalGameAccounts');
        if (!$totalGameAccounts->isHit()) {
            $num = $this->_em->getRepository('AppBundle:User')->countTotalAccounts();
            $totalGameAccounts->set($num);
            $totalGameAccounts->expiresAfter(CacheManager::EXPIRE30M);
            $this->_cache->save($totalGameAccounts);
        } else {
            $num = $totalGameAccounts->get();
        }

        return $num;
    }

    /**
     * Returns cached value for number of daily registrations.
     * @return null|int
     */
    public function getDailyRegistrations()
    {
        $dailyRegistrations = $this->_cache->getItem('dailyRegistrations');
        if (!$dailyRegistrations->isHit()) {
            $num = $this->_em->getRepository('AppBundle:User')->getAccountsCreatedLastXDays(1);
            $dailyRegistrations->set($num);
            $dailyRegistrations->expiresAfter(CacheManager::EXPIRE30M);
            $this->_cache->save($dailyRegistrations);
        } else {
            $num = $dailyRegistrations->get();
        }

        return $num;
    }

    /**
     * Returns cached value for number of weekly registrations.
     * @return null|int
     */
    public function getWeeklyRegistrations()
    {
        $weeklyRegistrations = $this->_cache->getItem('weeklyRegistrations');
        if (!$weeklyRegistrations->isHit()) {
            $num = $this->_em->getRepository('AppBundle:User')->getAccountsCreatedLastXDays(7);
            $weeklyRegistrations->set($num);
            $weeklyRegistrations->expiresAfter(CacheManager::EXPIRE30M);
            $this->_cache->save($weeklyRegistrations);
        } else {
            $num = $weeklyRegistrations->get();
        }

        return $num;
    }

    /**
     * Returns cached value for total number of bans.
     * @return null|int
     */
    public function getTotalBans()
    {
        $totalBans = $this->_cache->getItem('totalBans');
        if (!$totalBans->isHit()) {
            $num = $this->_em->getRepository('AppBundle:User')->countBannedUsers();
            $totalBans->set($num);
            $totalBans->expiresAfter(CacheManager::EXPIRE30M);
            $this->_cache->save($totalBans);
        } else {
            $num = $totalBans->get();
        }

        return $num;
    }

    /**
     * Returns cached value for number of bans this week.
     * @return null|int
     */
    public function getWeeklyBans()
    {
        $weeklyBans = $this->_cache->getItem('weeklyBans');
        if (!$weeklyBans->isHit()) {
            $num = $this->_conn->executeQuery("CALL getWeeklyBansIssued()")->fetchColumn();
            $weeklyBans->set($num);
            $weeklyBans->expiresAfter(CacheManager::EXPIRE30M);
            $this->_cache->save($weeklyBans);
        } else {
            $num = $weeklyBans->get();
        }

        return $num;
    }

    /**
     * Returns cached array of demographic data
     * @return null|array
     */
    public function getDemographics()
    {
        $demographics = $this->_cache->getItem('demographics');
        if (!$demographics->isHit()) {
            $array = $this->_em->getRepository('AppBundle:User')->getDemographics();
            $demographics->set($array);
            $demographics->expiresAfter(CacheManager::EXPIRE30M);
            $this->_cache->save($demographics);
        } else {
            $array = $demographics->get();
        }

        return $array;
    }

    /**
     * Returns cached array of active accounts.
     * @return null|array
     */
    public function getActiveIDs()
    {
        $activeIDs = $this->_cache->getItem('activeIDs');
        if (!$activeIDs->isHit()) {
            $array = $this->_conn->executeQuery("CALL getActiveAccountIds()")->fetchAll(\PDO::FETCH_COLUMN);
            $activeIDs->set($array);
            $activeIDs->expiresAfter(CacheManager::EXPIRE30M);
            $this->_cache->save($activeIDs);
        } else {
            $array = $activeIDs->get();
        }
        return $array;
    }

    /**
     * Returns cached value for number of active zeny.
     * @return null|int
     */
    public function getActiveZeny()
    {
        $activeZeny = $this->_cache->getItem('activeZeny');
        if (!$activeZeny->isHit()) {
            $active_account_ids = $this->getActiveIDs();
            $num = $this->_em->getRepository('AppBundle:Zeny')->countActiveZeny($active_account_ids);
            $activeZeny->set($num);
            $activeZeny->expiresAfter(CacheManager::EXPIRE30M);
            $this->_cache->save($activeZeny);
        } else {
            $num = $activeZeny->get();
        }
        return $num;
    }

    /**
     * Returns cached value for number of active zeny earned this week.
     * @return null|int
     */
    public function getZenyEarned()
    {
        $zenyEarned = $this->_cache->getItem('zenyEarned');
        if (!$zenyEarned->isHit()) {
            $num = $this->_conn->executeQuery("CALL getZenyEarned()")->fetchColumn();
            $zenyEarned->set($num);
            $zenyEarned->expiresAfter(CacheManager::EXPIRE30M);
            $this->_cache->save($zenyEarned);
        } else {
            $num = $zenyEarned->get();
        }
        return $num;
    }

    /**
     * Returns cached value for number of active points.
     * @return null|int
     */
    public function getActivePoints()
    {
        $activePoints = $this->_cache->getItem('activePoints');
        if (!$activePoints->isHit()) {
            $active_account_ids = $this->getActiveIDs();
            $num = $this->_em->getRepository('AppBundle:AccRegNum')->countActiveValue('#CASHPOINTS',$active_account_ids);
            $activePoints->set($num);
            $activePoints->expiresAfter(CacheManager::EXPIRE30M);
            $this->_cache->save($activePoints);
        } else {
            $num = $activePoints->get();
        }
        return $num;
    }

    /**
     * Returns cached value for number of points spent this week.
     * @return null|int
     */
    public function getPointsSpent()
    {
        $pointsSpent = $this->_cache->getItem('pointsSpent');
        if (!$pointsSpent->isHit()) {
            $num = $this->_conn->executeQuery("CALL getPointsSpent()")->fetchColumn();
            $pointsSpent->set($num);
            $pointsSpent->expiresAfter(CacheManager::EXPIRE30M);
            $this->_cache->save($pointsSpent);
        } else {
            $num = $pointsSpent->get();
        }
        return $num;
    }

    /**
     * Returns cached array for top zeny holders
     * @return array|mixed
     */
    public function getTopZenyHolders()
    {
        $topZenyHolders = $this->_cache->getItem('topZenyHolders');
        if (!$topZenyHolders->isHit()) {
            $array = $this->_conn->fetchAll("CALL getTopZenyHolders()");
            $topZenyHolders->set($array);
            $topZenyHolders->expiresAfter(CacheManager::EXPIRE30M);
            $this->_cache->save($topZenyHolders);
        } else {
            $array = $topZenyHolders->get();
        }
        return $array;
    }

    /**
     * Returns cached array for top points holders
     * @return array|mixed
     */
    public function getTopPointsHolders()
    {
        $topPointsHolders = $this->_cache->getItem('topPointsHolders');
        if (!$topPointsHolders->isHit()) {
            $array = $this->_conn->fetchAll("CALL getTopPointsHolders()");
            $topPointsHolders->set($array);
            $topPointsHolders->expiresAfter(CacheManager::EXPIRE30M);
            $this->_cache->save($topPointsHolders);
        } else {
            $array = $topPointsHolders->get();
        }
        return $array;
    }

    /**
     * Returns cached array of all mvp cards
     * @return array|mixed
     */
    public function getMVPCards()
    {
        $mvpCards = $this->_cache->getItem('mvpCards');
        if (!$mvpCards->isHit()) {
            $array = $this->_conn->fetchAll("CALL getMVPCards()");
            $mvpCards->set($array);
            $mvpCards->expiresAfter(CacheManager::EXPIRE30M);
            $this->_cache->save($mvpCards);
        } else {
            $array = $mvpCards->get();
        }
        return $array;
    }


    /**
     * Returns cached array of all mvps killed
     * @return array|mixed
     */
    public function getMVPSKilled()
    {
        $mvpsKilled = $this->_cache->getItem('mvpsKilled');
        if (!$mvpsKilled->isHit()) {
            $array = $this->_conn->fetchAll("CALL getMVPsKilled()");
            $mvpsKilled->set($array);
            $mvpsKilled->expiresAfter(CacheManager::EXPIRE30M);
            $this->_cache->save($mvpsKilled);
        } else {
            $array = $mvpsKilled->get();
        }
        return $array;
    }

    /**
     * Returns cached array of top mvp killers
     * @return array|mixed
     */
    public function getTopMVPKillers()
    {
        $mvpKillers = $this->_cache->getItem('topMVPKillers');
        if (!$mvpKillers->isHit()) {
            $array = $this->_conn->fetchAll("CALL getTopMVPKillers()");
            $mvpKillers->set($array);
            $mvpKillers->expiresAfter(CacheManager::EXPIRE30M);
            $this->_cache->save($mvpKillers);
        } else {
            $array = $mvpKillers->get();
        }
        return $array;
    }

    /**
     * Returns cached array of GM logs
     * @return array|mixed
     */
    public function getGMLogs()
    {
        $gmLogs = $this->_cache->getItem('gmLogs');
        if (!$gmLogs->isHit()) {
            $array = $this->_conn->fetchAll("CALL getGMLogs()");
            $gmLogs->set($array);
            $gmLogs->expiresAfter(CacheManager::EXPIRE1M);
            $this->_cache->save($gmLogs);
        } else {
            $array = $gmLogs->get();
        }
        return $array;
    }

    /**
     * Returns cached array of vending data
     * @return array|mixed
     */
    public function getVendingList()
    {
        $vendingList = $this->_cache->getItem('vendingList');
        if (!$vendingList->isHit()) {
            $array = $this->_conn->fetchAll("CALL vendinglist()");
            $vendingList->set($array);
            $vendingList->expiresAfter(CacheManager::EXPIRE30S);
            $this->_cache->save($vendingList);
        } else {
            $array = $vendingList->get();
        }
        return $array;
    }

    /**
     * Returns cached array of buyer data
     * @return array|mixed
     */
    public function getBuyingStoreList()
    {
        $buyingStoreList = $this->_cache->getItem('buyingStoreList');
        if (!$buyingStoreList->isHit()) {
            $array = $this->_conn->fetchAll("CALL buyingstorelist()");
            $buyingStoreList->set($array);
            $buyingStoreList->expiresAfter(CacheManager::EXPIRE30S);
            $this->_cache->save($buyingStoreList);
        } else {
            $array = $buyingStoreList->get();
        }
        return $array;
    }
}
