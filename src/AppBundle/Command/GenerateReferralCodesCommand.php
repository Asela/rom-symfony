<?php

namespace AppBundle\Command;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateReferralCodesCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:generate_referral_codes')
            ->setDescription('Generates referral codes for master accounts without one.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $codeless = $em->getRepository('AppBundle:User')->findBy(['referralCode' => 'X']);
        if ($codeless) {
            /** @var User $user */
            foreach($codeless as $user) {
                $user->setReferralCode(str_replace(array('+','/'), array('-','_'), openssl_encrypt($user->getMasterId(),
                    $this->getContainer()->getParameter('app.referral_method'),
                    $this->getContainer()->getParameter('app.referral_password'), 0,
                    $this->getContainer()->getParameter('app.referral_iv'))));
            }
            $em->flush();
        }
    }
}
