<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestMailCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:test_mail')
            ->setDescription('Test an email')
            ->addArgument("email", InputArgument::REQUIRED,"Send to who?")
            ->addArgument("which", InputArgument::OPTIONAL,"Which Email?");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $context = $this->getContainer()->get('router')->getContext();
        $context->setHost('riseofmidgard.com');
        $context->setScheme('https');
        $context->setBaseUrl('/');

        $mailer = $this->getContainer()->get('app.mailer');

        if ($input->getArgument('which') == "DOM_DATE")
            $mailer->sendReleaseDateDom($input->getArgument('email'));
        if ($input->getArgument('which') == "ROM_DATE")
            $mailer->sendReleaseDateRom($input->getArgument('email'));
    }
}
