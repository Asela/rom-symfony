<?php

namespace AppBundle\Command;

use AppBundle\Entity\AccRegNum;
use AppBundle\Entity\GameAccount;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BirthdayEmailCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:birthday_email')
            ->setDescription('Send out a birthday email to users');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $context = $this->getContainer()->get('router')->getContext();
        $context->setHost('riseofmidgard.com');
        $context->setScheme('https');
        $context->setBaseUrl('/');

        $em = $this->getContainer()->get('doctrine')->getManager();
        $bdays = $em->getRepository('AppBundle:User')->findBirthdays();
        $mailer = $this->getContainer()->get('app.mailer');

        if ($bdays) {
            /** @var User $user */
            foreach($bdays as $user) {
                /** @var GameAccount $gameAccount */
                $gameAccount = $user->getGameAccounts()->first();
                if ($gameAccount !== NULL) {
                    $accRegNum = $gameAccount->getAccRegNum()->get('#Birthday');
                    if ($accRegNum === NULL) {
                        $accRegNum = new AccRegNum("#Birthday", 1);
                        $gameAccount->addAccRegNum($accRegNum);
                        $em->persist($accRegNum);
                    } else {
                        $accRegNum->setValue(1);
                    }
                    $mailer->sendBirthday($user->getEmail(), $gameAccount->getUserid());
                }
            }
            $em->flush();
        }
    }
}
