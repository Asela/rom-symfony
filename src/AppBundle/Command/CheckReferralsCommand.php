<?php

namespace AppBundle\Command;

use AppBundle\Entity\Referral;
use AppBundle\Entity\UserPoints;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckReferralsCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:check_referrals')
            ->setDescription('Checks referrals that have not be rewarded for matching conditions');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $referrals = $em->getRepository('AppBundle:Referral')->findBy(['received' => '0']);
        if ($referrals) {
            /** @var Referral $ref */
            foreach($referrals as $ref) {
                $referrer = $em->getRepository('AppBundle:User')->find($ref->getReferrer());
                if ($referrer) {
                    // Add more checks here
                    if($em->getRepository('AppBundle:Character')->hasCharOverLevel($ref->getMasterId(),70)) {
                        if ($referrer->getUserPoints() === NULL) {
                            $userPoints = new UserPoints();
                            $userPoints->updateReferralPoints(1);
                            $referrer->setUserPoints($userPoints);
                            $em->persist($userPoints);
                        } else {
                            $referrer->getUserPoints()->updateReferralPoints(1);
                        }
                        $ref->setReceived(true);
                    } else {
                        // NO POINTS FOR YOU
                    }
                }
            }
            $em->flush();
        }
    }
}
