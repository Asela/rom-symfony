<?php

namespace AppBundle\Controller;

use AppBundle\Controller\ENUM\BanDetails;
use AppBundle\Entity\AccRegNum;
use AppBundle\Entity\GameAccount;
use AppBundle\Entity\IpnLog;
use AppBundle\Entity\PaypalLog;
use AppBundle\Entity\PendingCashpoints;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class PagesController
 * @package AppBundle\Controller
 *
 */
class DonationController extends Controller
{
    // Logging Location
    private $IPN_LOGFILE = '../var/logs/ipn_info.log';

    private $BASE_POINTS = 10;
    private $POINTS_BONUS = array( // Specify bonuses here
				0 => array('min_amount' => 10, 'bonus' => 10),
				1 => array('min_amount' => 20, 'bonus' => 40),
				2 => array('min_amount' => 35, 'bonus' => 70),
				3 => array('min_amount' => 50, 'bonus' => 200),
				4 => array('min_amount' => 100, 'bonus' => 500),
			);

	private $SANDBOX = false;
	private $PAYPAL_EMAIL = 'billing.riseofmidgard@gmail.com';
	private $PAYPAL_CURRENCY = 'USD';

    private $BAN_CONDITIONS = array('Reversed');
    private $LOG_CONDITIONS = array('Reversed', 'Refunded'); // Skip processing for transactions with these statuses; log only

    private $IPN_FAILED = 0;
    private $IPN_SUCCESS = 1;
    private $IPN_LOGGED_ONLY = 2;
    private $IPN_CANCELLED = 3;

    // Declare logging functions here because they'll only ever be used on this page ayyyy
    /**
     * @param array $ipn_info
     * @param string $log_message
     * @param EntityManager|null $em
     * @param bool|false $email
     * @param integer $credits_given
     * @return null|PaypalLog
     */
    private function ipnLog($ipn_info, $log_message, $em = null, $email = false, $credits_given = 0) {
        if ($ipn_info !== NULL) {
            $message = 'TXN_ID:' . $ipn_info['txn_id'] . ' - ' . $log_message;
        } else {
            $message = $log_message;
        }
        file_put_contents($this->IPN_LOGFILE, date('[Y-m-d H:i e] ').''.$message."\r\n", FILE_APPEND);

        if ($em) {
            $paypal_log = new PaypalLog($ipn_info,$credits_given);
            $ipn_log = new IpnLog($ipn_info['txn_id'], $credits_given > 0, $log_message, $paypal_log);

            $em->persist($paypal_log);
            $em->persist($ipn_log);
            $em->flush();

            if($email && !$credits_given) {
                $this->get('app.mailer')->sendDonationFail($paypal_log);
            }
			return $paypal_log;
        }
		return null;
    }

    /**
     * @Route("/donate/ipn")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function donateAction(Request $request)
    {
		$this->ipnLog(null,'Received notification from '.$_SERVER['REMOTE_ADDR'].' ('.gethostbyaddr($_SERVER['REMOTE_ADDR']).')');

		$parameters = $request->request->all();
		if ($request->getMethod() == 'POST' && !empty($parameters)) {
            $raw_post_data = file_get_contents('php://input');
            $raw_post_array = explode('&', $raw_post_data);
        } else {
            $this->ipnLog(null,'No POST data received');
            return new Response('1');
        }

        $ipn_info = array();
        foreach ($raw_post_array as $keyval)
        {
            $keyval = explode('=', $keyval);
            if (count($keyval) == 2)
            {
                // Since we do not want the plus in the datetime string to be encoded to a space, we manually encode it.
                if ($keyval[0] === 'payment_date') {
                    if (substr_count($keyval[1], '+') === 1) {
                        $keyval[1] = str_replace('+', '%2B', $keyval[1]);
                    }
                }
                $ipn_info[$keyval[0]] = urldecode($keyval[1]);
            }
        }

        if ($ipn_info['custom']) {
            $ipn_info['account_id'] = $ipn_info['custom'];
        }

        // Put the postData into a string
        $postString = 'cmd=_notify-validate';
        foreach ($ipn_info as $key=>$value) {
            $postString .= '&'.$key . '=' . urlencode(stripslashes($value));
        }

        // Post IPN data back to PayPal to validate the IPN data is genuine - AIN'T NOBODY FAKIN' US OUT HERE
		if($this->SANDBOX) $paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
		else $paypal_url = "https://www.paypal.com/cgi-bin/webscr";

        $curl = \curl_init();
        \curl_setopt($curl, CURLOPT_URL, $paypal_url);
        \curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        \curl_setopt($curl, CURLOPT_POST, 1);
        \curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        \curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        \curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        \curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2); // PayPal only accepts TLS 1.2
        \curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
        \curl_setopt($curl, CURLOPT_POSTFIELDS, $postString);

        // Set TCP timeout to 30 seconds because this is the PayPal default, and apparently they expect their servers to take half a decade to respond
		\curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
		\curl_setopt($curl, CURLOPT_HTTPHEADER, array('Connection: Close'));

        $res = \curl_exec($curl);
		if (\curl_error($curl)) { // EVERYONE PANIC, CURL EXPLODED
			$this->ipnLog($ipn_info,'Failed to connect to PayPal to verify request ('.$postString.') (Error: '.\curl_errno($curl).' - '.\curl_error($curl).')');
			curl_close($curl);
			return new Response($this->IPN_FAILED);
		}
		else curl_close($curl);

		// We be checkin' if this IPN got validated because once again, ain't nobody pulling some trickster bollocks on us! PayPal says explosions are good on $res.
		$tokens = explode("\r\n\r\n", trim($res));
		$res = trim(end($tokens));

		if (strcmp ($res, "VERIFIED") == 0) {
			$this->ipnLog($ipn_info,'IPN request verified by PayPal');

			$em = $this->getDoctrine()->getManager();

			if($ipn_info['receiver_email'] != $this->PAYPAL_EMAIL) { // Receiver email doesn't match our PayPal email
                $log_message = 'Incorrect Receiver Email ('.$ipn_info['receiver_email'].')';
                $this->ipnLog($ipn_info, $log_message, $em, true);
                return new Response($this->IPN_LOGGED_ONLY);
			}
			else if(in_array($ipn_info['payment_status'], $this->LOG_CONDITIONS)) { // Transaction status is flagged to be skipped; log and exit
                if(in_array($ipn_info['payment_status'], $this->BAN_CONDITIONS)) $log_message = 'Invalid transaction status - **Status found in BAN_CONDITIONS, may need manual review** ('.$ipn_info['payment_status'].')';
                else $log_message = 'Invalid transaction status ('.$ipn_info['payment_status'].')';
                $this->ipnLog($ipn_info, $log_message, $em, false); // WE DO NOT EMAIL ON THESE
                return new Response($this->IPN_LOGGED_ONLY);
			}
			else if($ipn_info['txn_type'] != 'web_accept') { // Not a web_accept transaction (probably received from a source other than the website)
                $log_message = 'Incorrect Transaction Type ('.$ipn_info['txn_type'].') - Valid type is web_accept.';
                $this->ipnLog($ipn_info, $log_message, $em, true);
                return new Response($this->IPN_LOGGED_ONLY);
			}
			else if($ipn_info['mc_currency'] != $this->PAYPAL_CURRENCY) { // Wrong currency was sent
				$log_message = 'Incorrect Currency Sent ('.$ipn_info['mc_currency'].') - Valid currency is USD';
                $this->ipnLog($ipn_info, $log_message, $em, true);
				return new Response($this->IPN_LOGGED_ONLY);
			}
			else if(!$ipn_info['account_id']) { // No Account ID sent
                $log_message = 'No Account ID linked to transaction';
                $this->ipnLog($ipn_info, $log_message, $em, true);
				return new Response($this->IPN_LOGGED_ONLY);
			}
			else {
				/** @var GameAccount $addAccount */
				$addAccount = $em->getRepository('AppBundle:GameAccount')->find($ipn_info['account_id']);
				if($addAccount === NULL) {
                    $log_message = 'Account ID '.$ipn_info['account_id'].' does not exist';
                    $this->ipnLog($ipn_info, $log_message, $em, true);
					return new Response($this->IPN_LOGGED_ONLY);
				} else {
					if($ipn_info['payment_status'] == 'Completed') { // AYYYYYYY, we got money!
						$duplicate = $em->getRepository('AppBundle:PaypalLog')->isCompletedDuplicate($ipn_info['txn_id']);
						if($duplicate) { // We've already received this payment
							$this->ipnLog($ipn_info, 'Payment for this transaction has already been received, cancelling');
							return new Response($this->IPN_CANCELLED);
						}
						else {
							$this->ipnLog($ipn_info, 'Payment of '.$ipn_info['mc_gross'].' '.$ipn_info['mc_currency'].' has been completed');
							$credits_base = floor($ipn_info['mc_gross'] * $this->BASE_POINTS);
							$credits_bonus = 0;
							for($i = 0; isset($this->POINTS_BONUS[$i]); $i++) { // Use hardcoded bonus amounts
								if($ipn_info['mc_gross'] >= $this->POINTS_BONUS[$i]['min_amount'] && $this->POINTS_BONUS[$i]['bonus'] > $credits_bonus)
									$credits_bonus = $this->POINTS_BONUS[$i]['bonus'];
							}

							$credits_given = $credits_base + $credits_bonus;

							// Check if the user has an active CP bonus to apply (if multiple exist, only use the highest bonus)
                            $cp_bonus = $em->getRepository('AppBundle:CpBonus')->getUserBonus($addAccount->getMasterId());
							if($cp_bonus) $credits_given = floor(($credits_given * (100 + $cp_bonus->getAmount())) / 100);

                            $log_message = 'Player '.$ipn_info['account_id'].' will receive '.($credits_given).' ('.$credits_base.' + '.$credits_bonus.''.($cp_bonus?' + '.$cp_bonus->getAmount().'%':'').') credits';
                            $paypal_log = $this->ipnLog($ipn_info, $log_message, $em, true, $credits_given);
                            $this->get('app.mailer')->sendDonationSuccess($paypal_log, $addAccount, $credits_given);

							$isOnline = $em->getRepository('AppBundle:Character')->isAccountOnline($ipn_info['account_id']);
							if(!$isOnline) {
								// Add Cash Points
                                if ($addAccount->getPoints() === NULL) {
                                    $points = new AccRegNum('#CASHPOINTS',$credits_given);
                                    $addAccount->addAccRegNum($points);
                                    $em->persist($points);
                                } else {
                                    $addAccount->getPoints()->updateValue($credits_given);
                                }
								// Update total money spent
								$addAccount->getMasterAccount()->updateMoneySpent($ipn_info['mc_gross']);
								$pendingPoints = new PendingCashpoints($addAccount->getMasterId(),$addAccount->getAccountId(), $credits_given, true);
							}
							else { // Player is online, fall back to alternate point transfer
								// Mark points as not received
								$pendingPoints = new PendingCashpoints($addAccount->getMasterId(),$addAccount->getAccountId(), $credits_given, false);
							}

                            if($cp_bonus) $em->remove($cp_bonus);

                            $woeCashPoints = new \AppBundle\Entity\PendingCashpoints($addAccount->getMasterId(),$addAccount->getAccountId(), $credits_given, false);
                            $wm = $this->getDoctrine()->getManager('woe');

							$em->persist($pendingPoints);
                            $wm->persist($woeCashPoints);

							$em->flush();
                            $wm->flush();
						}
					} else { // Relax, don't do it~! srsly tho don't do it, payment hasn't been processed yet, or it's a chargeback
						$this->ipnLog($ipn_info, 'Payment of '.$ipn_info['mc_gross'].' '.$ipn_info['mc_currency'].' is not completed (status: '.$ipn_info['payment_status'].') - credits will not be given');
                        $em->persist(new PaypalLog($ipn_info));
                        $em->flush();
						// If status is found in $BAN_CONDITIONS, automatically ban the user's account
						if (in_array($ipn_info['payment_status'], $this->BAN_CONDITIONS)) {
                            $log_message = 'Payment status '.$ipn_info['payment_status'].' is an auto-ban. Banning user now.';
                            $this->ipnLog($ipn_info, $log_message, $em, false);
                            $this->get('app.ban_support')->banUser(0, $addAccount->getMasterAccount(), BanDetails::CHARGEBACK);
                        }
					}
				}
			}

			return new Response($this->IPN_SUCCESS);
		}
		else {
			$this->ipnLog(null, 'IPN request failed to verify by PayPal: '.$res.' ('.$postString.')');
			return new Response($this->IPN_FAILED);
		}
    }

}
