<?php

namespace AppBundle\Controller\ENUM;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ClassParser extends Controller
{

    /**
     * @param integer
     * @return string
     */
    static public function getClassName ( $class_id ) {
        switch($class_id) {
            case 0: return "Novice"; break;
            case 1: return "Swordman"; break;
            case 2: return "Magician"; break;
            case 3: return "Archer"; break;
            case 4: return "Acolyte"; break;
            case 5: return "Merchant"; break;
            case 6: return "Thief"; break;
            case 7: return "Knight"; break;
            case 8: return "Priest"; break;
            case 9: return "Wizard"; break;
            case 10: return "Blacksmith"; break;
            case 11: return "Hunter"; break;
            case 12: return "Assassin"; break;
            case 14: return "Crusader"; break;
            case 15: return "Monk"; break;
            case 16: return "Sage"; break;
            case 17: return "Rogue"; break;
            case 18: return "Alchemist"; break;
            case 19: return "Bard"; break;
            case 20: return "Dancer"; break;
            case 23: return "Super Novice"; break;
            case 24: return "Gunslinger"; break;
            case 25: return "Ninja"; break;
            case 4001: return "Novice High"; break;
            case 4002: return "Swordman High"; break;
            case 4003: return "Magician High"; break;
            case 4004: return "Archer High"; break;
            case 4005: return "Acolyte High"; break;
            case 4006: return "Merchant High"; break;
            case 4007: return "Thief High"; break;
            case 4008: return "Lord Knight"; break;
            case 4009: return "High Priest"; break;
            case 4010: return "High Wizard"; break;
            case 4011: return "Whitesmith"; break;
            case 4012: return "Sniper"; break;
            case 4013: return "Assassin Cross"; break;
            case 4015: return "Paladin"; break;
            case 4016: return "Champion"; break;
            case 4017: return "Professor"; break;
            case 4018: return "Stalker"; break;
            case 4019: return "Creator"; break;
            case 4020: return "Clown"; break;
            case 4021: return "Gypsy"; break;
            case 4023: return "Baby Novice"; break;
            case 4024: return "Baby Swordman"; break;
            case 4025: return "Baby Magician"; break;
            case 4026: return "Baby Archer"; break;
            case 4027: return "Baby Acolyte"; break;
            case 4028: return "Baby Merchant"; break;
            case 4029: return "Baby Thief"; break;
            case 4030: return "Baby Knight"; break;
            case 4031: return "Baby Priest"; break;
            case 4032: return "Baby Wizard "; break;
            case 4033: return "Baby Blacksmith"; break;
            case 4034: return "Baby Hunter"; break;
            case 4035: return "Baby Assassin"; break;
            case 4037: return "Baby Crusader"; break;
            case 4038: return "Baby Monk"; break;
            case 4039: return "Baby Sage"; break;
            case 4040: return "Baby Rogue "; break;
            case 4041: return "Baby Alchemist"; break;
            case 4042: return "Baby Bard"; break;
            case 4043: return "Baby Dancer"; break;
            case 4045: return "Super Baby"; break;
            case 4046: return "Taekwon"; break;
            case 4047: return "Star Gladiator"; break;
            case 4048: return "Star Gladiator"; break;
            case 4049: return "Soul Linker"; break;
            case 4050: return "Gangsi"; break;
            case 4051: return "Death Knight"; break;
            case 4052: return "Dark Collector"; break;
            case 4054: return "Rune Knight"; break;
            case 4055: return "Warlock"; break;
            case 4056: return "Ranger"; break;
            case 4057: return "Arch Bishop"; break;
            case 4058: return "Mechanic"; break;
            case 4059: return "Guillotine Cross"; break;
            case 4060: return "Rune Knight"; break;
            case 4061: return "Warlock"; break;
            case 4062: return "Ranger"; break;
            case 4063: return "Arch Bishop"; break;
            case 4064: return "Mechanic"; break;
            case 4065: return "Guillotine Cross"; break;
            case 4066: return "Royal Guard"; break;
            case 4067: return "Sorcerer"; break;
            case 4068: return "Minstrel"; break;
            case 4069: return "Wanderer"; break;
            case 4070: return "Sura"; break;
            case 4071: return "Genetic"; break;
            case 4072: return "Shadow Chaser"; break;
            case 4073: return "Royal Guard"; break;
            case 4074: return "Sorcerer"; break;
            case 4075: return "Minstrel"; break;
            case 4076: return "Wanderer"; break;
            case 4077: return "Sura"; break;
            case 4078: return "Genetic"; break;
            case 4079: return "Shadow Chaser"; break;
            case 4096: return "Baby Rune Knight"; break;
            case 4097: return "Baby Warlock"; break;
            case 4098: return "Baby Ranger"; break;
            case 4099: return "Baby Arch Bishop"; break;
            case 4100: return "Baby Mechanic"; break;
            case 4101: return "Baby Glt. Cross"; break;
            case 4102: return "Baby Royal Guard"; break;
            case 4103: return "Baby Sorcerer"; break;
            case 4104: return "Baby Minstrel"; break;
            case 4105: return "Baby Wanderer"; break;
            case 4106: return "Baby Sura"; break;
            case 4107: return "Baby Genetic"; break;
            case 4108: return "Baby Shadow Chaser"; break;
            case 4190: return "Ex. Super Novice"; break;
            case 4191: return "Ex. Super Baby"; break;
            case 4211: return "Kagerou"; break;
            case 4212: return "Oboro"; break;
            case 4215: return "Rebellion"; break;
            default: return "Unknown"; break;
        }
        return "Unknown";
    }
}
