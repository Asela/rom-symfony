<?php

namespace AppBundle\Controller\ENUM;


class BanDetails
{
    const TYPE_MASTER_BAN = "Master Ban";
    const TYPE_MASTER_UNBAN = "Master UnBan";
    const TYPE_BAN = "Game Account Ban";
    const TYPE_UNBAN = "Game Account Unban";
    const TYPE_UNBAN_FAIL = "Failed Unban";

    const CHARGEBACK = "Chargeback";
    const MASTERBANNED = "Account is master banned";
}
