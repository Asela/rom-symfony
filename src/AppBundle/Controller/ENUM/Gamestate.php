<?php
namespace AppBundle\Controller\ENUM;

// Different game states for Login.state, Char.online, etc
// Fancy constants to make life easier!
class Gamestate {
    const OK = 0; // Account in good standing.

    // 1 Unregistered ID. Please make sure you have a registered account and you have correctly typed in the user ID.
    // 2 Incorrect User ID or Password. Please try again.
    // 3 This ID is expired.
    const REJECTED = 4; //Rejected from Server.
    const GM_BAN = 5; // Your account has been terminated. If you don't know why, please contact our support.
    // 6 Your game client is not the latest version. Please close all clients and patch. Still an issue? You will need to remove dawn.dat and repatch.
    // 7 You are prohibited to log in until %s.
    // 8 Server is jammed due to overpopulation. Please try again after few minutes.
    // 9 This account can't connect the Sakray server.
    const DBA_BAN = 10; // You have been banned by a Database Admin. Please send a support ticket.
    const EMAIL_UNCONFIRMED = 11; // You must confirm your email.
    const WEB_BAN = 12; // Your account has been terminated. If you don't know why, please contact our support.
    const DBA_TEMP = 13; // Your account is temporarily blocked for database work.
    const SELF_LOCK = 14; //Your account has been locked from the Control Panel.
    // 15 Your account group is not permitted to login.
    // 16 Server is not active.
    // 18 This account has been used for illegal program or hacking program. Block Time: %s
    // 19 The possibility of exposure to illegal program, PC virus infection or Hacking Tool has been detected. Please execute licensed client. Our team is trying to make a best environment for Ro players.
    // 20 OTP information is unavailable. Please contact your administrator.
    // 21 OTP authentication failed.
    // 22 Failed to recognize SSO.
    // 101 Login information remains at %s.
    const HACKING = 102; //Account has been locked for a hacking investigation. Please contact the GM Team for more information.
    const BUG_CHECK = 103; //This account has been temporarily prohibited from login due to a bug-related investigation.
    // 104 Login is temporarily unavailable while this character is being deleted.
    // 105 Login is temporarily unavailable while your spouse character is being deleted.
    // 106 This account has not been confirmed by connecting to the safe communication key. Please connect to the key first, and then log into the game.
    // 107 Mobile Authentication
    // 108 Rejected from Server.
    // 109 An user of this server cannot connect to free server
    // 110 Your password has expired. Please log in again
    const CHANGE_PWD = 111; // 111 Please change your password
    // 112 Rejected from Server.
    // 113 CBT is not an invited user

    const ONLINE = 1;
    const OFFLINE = 0;

    const MASTER_BAN = 666;
}


