<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AccRegNum;
use AppBundle\Entity\Character;
use AppBundle\Entity\User;
use AppBundle\Entity\Zeny;
use AppBundle\Form\GameAccountEditForm;
use AppBundle\Form\GameAccountCreationForm;
use AppBundle\Form\Model\EditCharacter;
use AppBundle\Form\Model\EditGameAccount;
use AppBundle\Controller\ENUM\Gamestate;
use AppBundle\Form\ResetCharacter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use AppBundle\Entity\GameAccount;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GameAccountController
 * Handles majority of player game account interaction such as moving Zeny or Cash Points.
 * @package AppBundle\Controller
 * @Security("has_role('ROLE_USER')")
 * @Route("/panel")
 */
class GameAccountController extends Controller
{
    /**
     * @Route("/", name="panel_index")
     */
    public function manageGameAccountsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $data = $this->getUser()->jsonGameAccounts();

        /** @var GameAccount $gameAccount */
        foreach ($data as $gaKey => $gameAccount) {
            /** @var Character $char */
            foreach ($gameAccount['chars'] as $chKey => $char) {
                $g = $em->getRepository('AppBundle:Guild')->find($char['guild']);

                $data[$gaKey]['chars'][$chKey]['guild'] = '';
                if ($g !== NULL)
                    $data[$gaKey]['chars'][$chKey]['guild'] = $g->getName();
            }
        }

        $gameAccounts['data'] = $data;

        $pendingPoints = $em->getRepository('AppBundle:PendingCashpoints')->getMasterPendingPoints($this->getUser()->getMasterId());

        return $this->render('AppBundle:Panel:index.html.twig', [
            'email_unconfirmed' => ($this->getUser()->getState() == Gamestate::EMAIL_UNCONFIRMED),
            'gameaccounts' => json_encode($gameAccounts),
            'pending_points'=> number_format($pendingPoints),
            ]);
    }

    /**
     * @Route("/add", name="panel_add_gameaccount")
     * @param Request $request
     * @param bool $modal
     * @return JsonResponse|Response
     */
    public function addGameAccountAction(Request $request, $modal = false)
    {
        if (!$request->isXmlHttpRequest() && !$modal) $this->redirectToRoute('panel_index');

        /** @var User $user */
        $user = $this->getUser();

        $gameAccount = new GameAccount();

        if($user->getState() == Gamestate::EMAIL_UNCONFIRMED)
            $gameAccount->setState(Gamestate::EMAIL_UNCONFIRMED);
        else if ($user->getState() == Gamestate::MASTER_BAN)
            $gameAccount->setState(Gamestate::WEB_BAN); // Nice try

        // Replace with non-teaser version post teaser
        $form = $this->createForm(GameAccountCreationForm::class, $gameAccount);
        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            if ($form->isSubmitted() && $form->isValid()) {
                $password = $gameAccount->getPlainPassword();
                $gameAccount->setUserPass(md5($password));
                $user->addGameAccount($gameAccount);

                $em = $this->getDoctrine()->getManager();
                $em->persist($gameAccount);
                $em->flush();
                return new JsonResponse(array('success' => true), Response::HTTP_OK);
            }

            $errorsList = $form->getErrors(true);
            $errors = array();
            foreach ($errorsList as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }
            return new JsonResponse($errors, Response::HTTP_OK);
        }

        return $this->render('AppBundle:Panel/Modal:addaccount.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/change", name="panel_change_password")
     * @param Request $request
     * @param bool $modal
     * @return JsonResponse|Response
     */
    public function changeGameAccountPasswordAction(Request $request, $modal = false)
    {
        if (!$request->isXmlHttpRequest() && !$modal) $this->redirectToRoute('panel_index');

        $editGameAccount = new EditGameAccount();
        $form = $this->createForm(GameAccountEditForm::class, $editGameAccount);
        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            $errors = array();
            if ($form->isSubmitted() && $form->isValid()) {
                /** @var GameAccount $gameAccount */
                $gameAccount = $this->getUser()->getGameAccount($editGameAccount->getAccountId());
                if ($gameAccount === NULL) {
                    $errors['missing_account'] = $this->get('translator')->trans('trans.game_account.missing');
                } else {
                    $gameAccount->setUserPass(md5($editGameAccount->getNewPassword()));
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($gameAccount);
                    $em->flush();
                    return new JsonResponse(array('success' => true), Response::HTTP_OK);
                }
            }
            $errorsList = $form->getErrors(true);
            foreach ($errorsList as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }
            return new JsonResponse($errors, Response::HTTP_OK);
        }

        return $this->render('AppBundle:Panel/Modal:change_password.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param $choices
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createMoveForm(User $user, $zeny = false)
    {
        $choices = array();
        /**
         * @var GameAccount $gameAccount
         */
        foreach($user->getGameAccounts() as $gameAccount) {
            if ($zeny) $choiceKey = $gameAccount->getUserid() . " (" . $gameAccount->getZenyFormatted() . " " .$this->get('translator')->trans('trans.game_account.zeny') . ")";
            else $choiceKey = $choiceKey = $gameAccount->getUserid() . " (" . $gameAccount->getPointsFormatted() . " " .$this->get('translator')->trans('trans.game_account.points') . ")";
            $choices[$choiceKey] = $gameAccount->getAccountId();
        }

        return $this->createFormBuilder()
            ->add('from_account', ChoiceType::class, array(
                'choices' => $choices,
                'expanded' => false,
                'multiple' => false,
                'placeholder' => 'trans.form.select_placeholder',
                'label' => 'trans.form.transfer_from',
                'error_bubbling' => true,
            ))
            ->add('to_account', ChoiceType::class, array(
                'choices' => $choices,
                'expanded' => false,
                'multiple' => false,
                'placeholder' => 'trans.form.select_placeholder',
                'label' => 'trans.form.transfer_to',
                'error_bubbling' => true,
            ))
            ->add('value', NumberType::class, [
                'scale' => 0,
                'label' => 'trans.form.transfer_amount',
                'error_bubbling' => true,

            ])
            ->getForm();

    }
    /**
     * @Route("/move/{type}", name="panel_move", requirements={"type": "zeny|points"})
     * @param Request $request
     * @param bool $modal
     * @return JsonResponse|Response
     */
    public function moveAction(Request $request, $type = "zeny", $modal = false)
    {
        if (!$request->isXmlHttpRequest() && !$modal) $this->redirectToRoute('panel_index');

        $zeny = strcmp ($type, "zeny") == 0;

        /** @var User $user */
        $user = $this->getUser();

        $form = $this->createMoveForm($user,$zeny);
        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            $errors = array();
            $em = $this->getDoctrine()->getManager();
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();

                $fromAccount = $user->getGameAccount($data['from_account']);
                $toAccount = $user->getGameAccount($data['to_account']);
                $value = $data['value'];

                if ($fromAccount === NULL || $toAccount === NULL) {
                    $errors['error'] = $this->get('translator')->trans('trans.game_account.missing');
                } else if ($fromAccount->getAccountId() == $toAccount->getAccountId()) {
                    $errors['error'] = $this->get('translator')->trans('trans.game_account.same_account');
                } else {
                    $fromOnline = $em->getRepository('AppBundle:Character')->isAccountOnline($fromAccount->getAccountId());
                    $toOnline = $em->getRepository('AppBundle:Character')->isAccountOnline($toAccount->getAccountId());

                    if ($fromOnline || $toOnline) {
                        $errors['error'] = $this->get('translator')->trans('trans.game_account.need_offline');
                    } else {
                        if ($zeny) {
                            $fromAccount->getZeny()->updateAmount(0 - $value);
                            if ($value < 0 ||  $value > $fromAccount->getZeny()->getAmount()) {
                                $errors['error'] = $this->get('translator')->trans('trans.game_account.not_enough_zeny');
                            } else {
                                if ($toAccount->getZeny() === NULL) {
                                    $zeny = new Zeny();
                                    $zeny->updateAmount($value);
                                    $toAccount->setZeny($zeny);
                                    $em->persist($zeny);
                                } else {
                                    $toAccount->getZeny()->updateAmount($value);
                                }
                                $em->flush();
                                return new JsonResponse(array('success' => true), Response::HTTP_OK);
                            }
                        } else {
                            if ($value < 0 ||  $value > $fromAccount->getPoints()->getValue()) {
                                $errors['error'] = $this->get('translator')->trans('trans.game_account.not_enough_points');
                            } else {
                                $fromAccount->getPoints()->updateValue(0 - $value);

                                if($toAccount->getPoints() === NULL) {
                                    $points = new AccRegNum('#CASHPOINTS', $value);
                                    $toAccount->addAccRegNum($points);
                                    $em->persist($points);
                                } else {
                                    $toAccount->getPoints()->updateValue($value);
                                }
                                $em->flush();
                                return new JsonResponse(array('success' => true), Response::HTTP_OK);
                            }
                        }
                    }
                }
            }

            $errorsList = $form->getErrors(true);
            foreach ($errorsList as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }
            return new JsonResponse($errors, Response::HTTP_OK);
        }

        if ($zeny) {
            $path = 'AppBundle:Panel/Modal:move_zeny.html.twig';
        } else {
            $path = 'AppBundle:Panel/Modal:move_points.html.twig';
        }

        return $this->render($path, [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/move/points", name="panel_move_points")
     * @param Request $request
     * @param bool $modal
     * @return JsonResponse|Response
     */
    public function moveCashPointsAction(Request $request, $modal = false)
    {
        if (!$request->isXmlHttpRequest() && !$modal) $this->redirectToRoute('panel_index');
        /** @var User $user */
        $user = $this->getUser();

        $form = $this->createMoveForm($user,false);
        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            $errors = array();
            $em = $this->getDoctrine()->getManager();
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();

                $fromAccount = $user->getGameAccount($data['from_account']);
                $toAccount = $user->getGameAccount($data['to_account']);
                $value = $data['value'];

                if ($fromAccount === NULL || $toAccount === NULL) {
                    $errors['error'] = $this->get('translator')->trans('trans.game_account.missing');
                } else if ($fromAccount->getAccountId() == $toAccount->getAccountId()) {
                    $errors['error'] = $this->get('translator')->trans('trans.game_account.same_account');
                } else {
                    $fromOnline = $em->getRepository('AppBundle:Character')->isAccountOnline($fromAccount->getAccountId());
                    $toOnline = $em->getRepository('AppBundle:Character')->isAccountOnline($toAccount->getAccountId());

                    if ($fromOnline || $toOnline) {
                        $errors['error'] = $this->get('translator')->trans('trans.game_account.need_offline');
                    } else {
                        if ($value < 0 ||  $value > $fromAccount->getPoints()->getValue()) {
                            $errors['error'] = $this->get('translator')->trans('trans.game_account.not_enough_points');
                        } else {

                            $em->flush();
                            return new JsonResponse(array('success' => true), Response::HTTP_OK);
                        }
                    }
                }
            }

            $errorsList = $form->getErrors(true);
            foreach ($errorsList as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }
            return new JsonResponse($errors, Response::HTTP_OK);
        }

        return $this->render('AppBundle:Panel/Modal:move_points.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/reset/char", name="panel_reset_character")
     * @param Request $request
     * @param bool $modal
     * @return JsonResponse|Response
     */
    public function resetCharacterAction(Request $request, $modal = false)
    {
        if (!$request->isXmlHttpRequest() && !$modal) $this->redirectToRoute('panel_index');

        $editCharacter = new EditCharacter();
        $form = $this->createForm(ResetCharacter::class, $editCharacter);

        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            $errors = array();
            if ($form->isSubmitted() && $form->isValid()) {
                /** @var Character $char */
                $char = $this->getUser()->getCharacter($editCharacter->getAccountId(), $editCharacter->getCharacterId());
                $em = $this->getDoctrine()->getManager();

                if ($char === NULL) {
                    $errors['error'] = $this->get('translator')->trans('trans.game_account.missing');
                } else if ($em->getRepository('AppBundle:Character')->isAccountOnline($editCharacter->getAccountId())) {
                    $errors['error'] = $this->get('translator')->trans('trans.game_account.need_offline');
                } else {
                    if (strcmp ($editCharacter->getType(), "position") == 0) {
                        $char->resetPosition();
                    } else {
                        $char->resetStyle();
                    }
                    $em->flush();
                    return new JsonResponse(array('success' => true), Response::HTTP_OK);
                }
            }
            $errorsList = $form->getErrors(true);
            foreach ($errorsList as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }
            return new JsonResponse($errors, Response::HTTP_OK);
        }

        return $this->render('AppBundle:Panel/Modal:reset_character.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
