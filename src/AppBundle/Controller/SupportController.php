<?php

namespace AppBundle\Controller;

use AppBundle\Entity\GameAccount;
use AppBundle\Entity\User;
use Freshdesk\Exceptions\ValidationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class SupportController extends Controller
{

    /**
     * @Route("/support", name="support_page")
     * @Security("has_role('ROLE_USER')")
     */
    public function supportAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $api = $this->get('freshdesk');

        $saved = false;
        $choices = array();

        /**
         * @var GameAccount $gameAccount
         */
        foreach($user->getGameAccounts() as $gameAccount) {
            $choiceKey = $gameAccount->getUserid();
            $choices[$choiceKey] = $gameAccount->getAccountId();
        }

        $form = $this->createFormBuilder()
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'trans.support.type_account' => 'Account',
                    'trans.support.type_botting' => 'Botting',
                    'trans.support.type_bugreport' => 'Bug Report',
                    'trans.support.type_donations' => 'Donations',
                    'trans.support.type_griefing' => 'Griefing Report',
                    'trans.support.type_question' => 'Question',
                    'trans.support.type_quest' => 'Quest Issue',
                    'trans.support.type_other' => 'Other Issue'
                ],
                'label' => 'trans.support.type',
            ])
            ->add('account', ChoiceType::class, [
                'choices' => $choices,
                'expanded' => false,
                'multiple' => false,
                'placeholder' => 'trans.form.select_placeholder',
                'label' => 'trans.support.account',
                'required' => false,
            ])
            ->add('subject', TextType::class, [
                'attr' => array('maxlength' => 50),
                'label' => 'trans.support.subject',

            ])
            ->add('description', TextAreaType::class, [
                'attr' => array('rows' => 10,'maxlength' => 2000),
                'label' => 'trans.support.description',
            ])
            ->getForm();
        $clean_form = clone $form;
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $gameAccount = $user->getGameAccount($data['account']);

            $allowed_types = ['Question', 'Bug Report', 'Griefing Report', 'Account', 'Botting','Donations','Quest Issue','Other Issue'];
            $type = (!in_array($data['type'],$allowed_types)) ? $type = '' : $data['type'];

            $custom_fields = array();
            if ($gameAccount) $custom_fields["game_account"] = $gameAccount->getUserid() . "";
            $custom_fields["master_id"] = $user->getMasterId() . "";

            $ticket = [
                "name" => $user->getEmail(),
                "email" => $user->getEmail(),
                "subject" => $data['subject'],
                "description" => $data['description'],
                "type" => $type,
                "priority" => 1,
                "status" => 2,
                "custom_fields" => $custom_fields
            ];

            $api->tickets->create($ticket);
            $saved = true;
            $form = clone $clean_form;
        }

        $tickets = array();
        $tickets['data'] = array();
        try {
            $dump = $api->tickets->all(['email' => $user->getEmail()]);
            foreach ($dump as $t) {
                $priority = "";
                if (isset($t['priority'])) {
                    switch($t['priority']) {
                        case 1:  $priority = "Low"; break;
                        case 2:  $priority = "Medium"; break;
                        case 3:  $priority = "High"; break;
                        case 4:  $priority = "Urgent"; break;
                    }
                }

                $status = "Pending";
                if (isset($t['status'])) {
                    switch($t['status']) {
                        case 2:  $status = "Open"; break;
                        case 3:  $status = "Pending"; break;
                        case 4:  $status = "Resolved"; break;
                        case 5:  $status = "Closed"; break;
                    }
                }

                $tmp = [
                    'id' => (isset($t['id']) ? $t['id'] : ""),
                    'subject' => (isset($t['subject']) ? $t['subject'] : ""),
                    'type' => (isset($t['type']) ? $t['type'] : ""),
                    'priority' => $priority,
                    'status' => $status,
                    'created' => (isset($t['created_at']) ? $created = (new \DateTime($t['created_at']))->format('Y-m-d H:i:s') : ""),
                    'updated' => (isset($t['created_at']) ? $updated = (new \DateTime($t['updated_at']))->format('Y-m-d H:i:s') : "")
                ];

                $tickets['data'][] = $tmp;
            }
        } catch (ValidationException $e) {
        }

        return $this->render('AppBundle:Pages:support.html.twig', [
            'form' => $form->createView(),
            'saved' => $saved,
            'history' => json_encode($tickets)
        ]);
    }
}
