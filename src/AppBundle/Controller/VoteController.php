<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\UserPoints;
use AppBundle\Entity\VoteTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
/**
 * Class VoteController
 * @package AppBundle\Controller
 */
class VoteController extends Controller
{
    /**
     * @Route("/vote", name="voting_page")
     * @Route("/vote/")
     * @Route("/vote/{vote_id}", requirements={"vote_id":"\d+"})
     */
    public function voteAction(Request $request, $vote_id = -1)
    {
        // Retrieve vote site info from database
        $em = $this->getDoctrine()->getManager();
        $vote_info = $em->getRepository('AppBundle:VoteSite')->findAll();
        $delay = 0;

        // Vote site was called
        if ($vote_id > -1) {
            $voteSite = $em->getRepository('AppBundle:VoteSite')->find($vote_id);

            if ($voteSite !== NULL) {
                // User is logged in
                if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
                    /** @var User $user */
                    $user = $this->getUser();
                    $voteTime = $em->getRepository('AppBundle:VoteTime')->getVoteTime($user->getMasterId(),$voteSite->getId());
                    $delayCheck = $em->getRepository('AppBundle:VoteTime')->checkDelay($voteSite->getId(), $voteSite->getDelay(), $user->getMasterId(), $request->getClientIp());

                    if ($delayCheck !== NULL){
                        $now = new \DateTime();
                        // Delay is 0 or less when voting allowed
                        $delay = $voteSite->getDelay() - round(($now->getTimestamp() - $delayCheck->getTime()->getTimestamp())/60, 0, PHP_ROUND_HALF_DOWN);
                    }

                    // Player can vote
                    if ($delay <= 0) {
                        if ($user->getUserPoints() === NULL) {
                            $userPoints = new UserPoints();
                            $userPoints->updateVotePoints($voteSite->getPoints());
                            $user->setUserPoints($userPoints);
                            $em->persist($userPoints);
                        } else {
                            $user->getUserPoints()->updateVotePoints($voteSite->getPoints());
                        }
                        if ($voteTime === NULL){
                            $voteTime = new VoteTime($user->getMasterId(), $voteSite->getId(), $request->getClientIp());
                            $em->persist($voteTime);
                        } else {
                            $voteTime->setTime();
                        }
                        $em->flush();
                        return $this->redirect($voteSite->getUrl());
                    }
                } else {
                    return $this->redirect($voteSite->getUrl());
                }
            }
        }

        $a = ['1','2','3','4','5'];
        shuffle($a);
        return $this->render('AppBundle:Pages:vote.html.twig', array('vote' => $vote_info, 'delay' => $delay, 'litems' => $a));
    }
}
