<?php

namespace AppBundle\Controller;

use AppBundle\Entity\WoestatsChar;
use AppBundle\Entity\WoestatsGuild;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class WoEStatsController
 * Handles majority of player game account interaction such as moving Zeny or Cash Points.
 * @package AppBundle\Controller
 * @Route("/woestats")
 */
class WoEStatsController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Pages:woe_stats.html.twig');
    }

    /**
     * @Route("/guildlist/{type}/{war}", requirements={"type": "live|woe","war": "pre|trans"})
     * @param $type
     * @param $war
     * @return Response
     */
    public function guildlistAction($type, $war)
    {
        $flag = $this->trans($war);
        /** @var EntityManager $em */
        $em = $this->getWoeStatsManager($type);

        $WoeGuilds = $em->getRepository('AppBundle:WoestatsGuild')->getAllGuildStats($flag);
        $emblems = array();
        if (!empty($WoeGuilds)) {
            /**
             * @var  $key
             * @var WoestatsGuild $guild
             */
            foreach ($WoeGuilds as $guild) {
                $guild->setHoldTime(0);
                if ($guild->getCastles() == '') // No castles held
                    $guild->setCastleList(new \SplFixedArray());
                else { // Split castles into an array
                    $castles_array = preg_split('#;#', $guild->getCastles(), -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

                    $castles = new \SplFixedArray(count($castles_array));
                    $castle_count = count($castles_array);
                    for ($i = 0; $i < $castle_count; ++$i) {
                        $castle_tmp = preg_split('#:#', $castles_array[$i], -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                        $castles[$i]['id'] = $castle_tmp[0];
                        $castles[$i]['time'] = $castle_tmp[1];
                        $guild->setHoldTime($guild->getHoldTime() + $castle_tmp[1]);
                    }
                    $guild->setCastleList($castles);
                }
            }
            // Sort guilds by how long they held castles
            usort($WoeGuilds, array($this, "sortHoldTime"));
            foreach ($WoeGuilds as $key => $guild) {
                if ($guild->getEmblemData() !== NULL && $guild->getEmblemData() != '') {
                    $dataTmp = @gzuncompress(pack('H*', stream_get_contents($guild->getEmblemData())));
                    $emblems[$key] = $this->imagecreatefrombmpstring($dataTmp);
                }
            }
        }

        return $this->render('AppBundle:Pages:woe_guildlist.html.twig',
            array('type' => $type, 'war' => $war, 'woeguilds' => $WoeGuilds, 'emblems' => $emblems)
        );
    }

    /**
     * @Route("/playerlist/{type}/{war}", requirements={"type": "live|woe","war": "pre|trans"})
     * @param $war
     * @return Response
     */
    public function playerlistAction($type, $war)
    {
        $flag = $this->trans($war);
        /** @var EntityManager $em */
        $em = $this->getWoeStatsManager($type);
        $players = $em->getRepository('AppBundle:WoestatsChar')->getAllPlayers($flag);

        $playerlist = array();
        /** @var WoestatsChar $player */
        foreach ($players as $player) {
            $playerlist['data'][] = $player->jsonSerialize();
        }

        return $this->render('AppBundle:Pages:woe_playerlist.html.twig',
            array('type'=>$type, 'war' => $war, 'playerlist' => json_encode($playerlist))
        );
    }

    /**
     * @Route("/guildstats/{type}/{war}/{uid}", requirements={"type": "live|woe","war": "pre|trans","uid": "\d+"})
     * @param $war
     * @param $uid
     * @return Response
     */
    public function guildmembersAction($type,$war,$uid)
    {
        $flag = $this->trans($war);
        /** @var EntityManager $em */
        $em = $this->getWoeStatsManager($type);
        $guild = $em->getRepository('AppBundle:WoestatsGuild')->getGuildStatsById($flag,$uid);

        $emblem = null;
        $members['data'] = array();
        if (!empty($guild)) {
            if ($guild->getEmblemData() !== NULL && $guild->getEmblemData() != '') {
                $dataTmp = @gzuncompress(pack('H*', stream_get_contents($guild->getEmblemData())));
                $emblem = $this->imagecreatefrombmpstring($dataTmp);
            }
            $members['data'] = $guild->jsonWoeChars();
        }

        return $this->render('AppBundle:Pages:woe_guildmembers.html.twig',
            array('type' => $type,
                    'war' => $war,
                    'uid' => $uid,
                    'gname' => $guild->getName(),
                    'members' => json_encode($members),
                    'emblem' => $emblem)
        );
    }

    /**
     * @Route("/charstats/{type}/{war}/{uid}", requirements={"type": "live|woe","war": "pre|trans","uid": "\d+"})
     * @param $war
     * @param $uid
     * @return Response
     */
    public function charstatsAction($type,$war,$uid)
    {
        $flag = $this->trans($war);
        /** @var EntityManager $em */
        $em = $this->getWoeStatsManager($type);
        $player = $em->getRepository('AppBundle:WoestatsChar')->getCharStatsById($flag,$uid);

        $emblem = null;
        if (!empty($player)) {
            if ($player->getWoeguild()->getEmblemData() !== NULL && $player->getWoeguild()->getEmblemData() != '') {
                $dataTmp = @gzuncompress(pack('H*',stream_get_contents($player->getWoeguild()->getEmblemData())));
                $emblem = $this->imagecreatefrombmpstring($dataTmp);
            }

            $player->setSkillsTotal(0);
            if($player->getSkills() == '')
                $player->setSkillsList(new \SplFixedArray());
            else {
                $skill_tmplist = '';
                $skills_array = preg_split('#;#', $player->getSkills(), -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                $skills_list = array();
                $skills_count = count($skills_array);
                for($i = 0; $i < $skills_count; ++$i) {
                    $skill_tmp = preg_split('#:#', $skills_array[$i], -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                    $skills_list[$i]['id'] = $skill_tmp[0];
                    $skills_list[$i]['amount'] = $skill_tmp[1];
                    $skills_list[$i]['name'] = $em->getRepository('AppBundle:SkillDb')->getSkillName($skill_tmp[0]);
                    $player->setSkillsTotal($player->getSkillsTotal() + $skill_tmp[1]);
                    if($i == (count($skills_array)-1)) // Last skill, don't add a comma
                        $skill_tmplist .= $skill_tmp[0];
                    else $skill_tmplist .= $skill_tmp[0].', ';
                }

                usort($skills_list, array($this, 'sortByName'));
                $player->setSkillsList($skills_list);
            }

            $player->setItemsTotal(0);
            if($player->getItems() == '')
                $player->setItemsList(new \SplFixedArray());
            else {
                $item_tmplist = '';
                $items_array = preg_split('#;#', $player->getItems(), -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                $items_list = array();
                $items_count = count($items_array);
                for($i = 0; $i < $items_count; ++$i) {
                    $item_tmp = preg_split('#:#', $items_array[$i], -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                    $items_list[$i]['id'] = $item_tmp[0];
                    $items_list[$i]['amount'] = $item_tmp[1];
                    $items_list[$i]['name'] = $em->getRepository('AppBundle:ItemDb')->getItemName($item_tmp[0]);
                    $player->setItemsTotal($player->getItemsTotal() + $item_tmp[1]);
                    if($i == (count($items_array)-1)) // Last item, don't add a comma
                        $item_tmplist .= $item_tmp[0];
                    else $item_tmplist .= $item_tmp[0].', ';
                }

                usort($items_list, array($this, 'sortByName'));
                $player->setItemsList($items_list);
            }

        }

        return $this->render('AppBundle:Pages:woe_charstats.html.twig',
            array('type' => $type,'war' => $war, 'uid' => $uid, 'player' => $player, 'emblem' => $emblem)
        );
    }

    /**
     * @param $war
     * @return int
     */
    private function trans($war)
    {
        if($war == "trans"){ return 1;}
        if($war == "pre"){ return 2;}
        return 999;
    }

    /**
     * @param $type
     * @return EntityManager
     */
    private function getWoeStatsManager($type) {
        if ($type == "woe")
            return $this->getDoctrine()->getManager('woe');
        else
            return $this->getDoctrine()->getManager();
    }

    // Copied directly from FluxCP, added imagepng to get the image as a data string
    /**
     * @param $im
     * @return bool|string
     */
    private function imagecreatefrombmpstring($im)
    {
        $type = $offset = $width = $height = $bits = null;
        $header = unpack("vtype/Vsize/v2reserved/Voffset", substr($im, 0, 14));
        $info = unpack("Vsize/Vwidth/Vheight/vplanes/vbits/Vcompression/Vimagesize/Vxres/Vyres/Vncolor/Vimportant", substr($im, 14, 40));
        extract($info);
        extract($header);
        if ($type != 0x4D42)
            return false;
        $palette_size = $offset - 54;
        $imres = imagecreatetruecolor($width, $height);
        imagealphablending($imres, false);
        imagesavealpha($imres, true);
        $pal = array();
        if ($palette_size) {
            $palette = substr($im, 54, $palette_size);
            $j = 0;
            $n = 0;
            while ($j < $palette_size) {
                $b = ord($palette{$j++});
                $g = ord($palette{$j++});
                $r = ord($palette{$j++});
                $a = ord($palette{$j++});
                if (($r & 0xf8 == 0xf8) && ($g == 0) && ($b & 0xf8 == 0xf8))
                    $a = 127; // alpha = 255 on 0xFF00FF
                $pal[$n++] = imagecolorallocatealpha($imres, $r, $g, $b, $a);
            }
        }
        $scan_line_size = (($bits * $width) + 7) >> 3;
        $scan_line_align = ($scan_line_size & 0x03) ? 4 - ($scan_line_size & 0x03) : 0;
        for ($i = 0, $l = $height - 1; $i < $height; $i++, $l--) {
            $scan_line = substr($im, $offset + (($scan_line_size + $scan_line_align) * $l), $scan_line_size);
            if ($bits == 24) {
                $j = 0;
                $n = 0;
                while ($j < $scan_line_size) {
                    $b = ord($scan_line{$j++});
                    $g = ord($scan_line{$j++});
                    $r = ord($scan_line{$j++});
                    $a = 0;
                    if (($r & 0xf8 == 0xf8) && ($g == 0) && ($b & 0xf8 == 0xf8))
                        $a = 127; // alpha = 255 on 0xFF00FF
                    $col = imagecolorallocatealpha($imres, $r, $g, $b, $a);
                    imagesetpixel($imres, $n++, $i, $col);
                }
            } else if ($bits == 8) {
                $j = 0;
                while ($j < $scan_line_size) {
                    $col = $pal[ord($scan_line{$j++})];
                    imagesetpixel($imres, $j - 1, $i, $col);
                }
            } else if ($bits == 4) {
                $j = 0;
                $n = 0;
                while ($j < $scan_line_size) {
                    $byte = ord($scan_line{$j++});
                    $p1 = $byte >> 4;
                    $p2 = $byte & 0x0F;
                    imagesetpixel($imres, $n++, $i, $pal[$p1]);
                    imagesetpixel($imres, $n++, $i, $pal[$p2]);
                }
            } else if ($bits == 1) {
                $j = 0;
                $n = 0;
                while ($j < $scan_line_size) {
                    $byte = ord($scan_line{$j++});
                    $p1 = (int)(($byte & 0x80) != 0);
                    $p2 = (int)(($byte & 0x40) != 0);
                    $p3 = (int)(($byte & 0x20) != 0);
                    $p4 = (int)(($byte & 0x10) != 0);
                    $p5 = (int)(($byte & 0x08) != 0);
                    $p6 = (int)(($byte & 0x04) != 0);
                    $p7 = (int)(($byte & 0x02) != 0);
                    $p8 = (int)(($byte & 0x01) != 0);
                    imagesetpixel($imres, $n++, $i, $pal[$p1]);
                    imagesetpixel($imres, $n++, $i, $pal[$p2]);
                    imagesetpixel($imres, $n++, $i, $pal[$p3]);
                    imagesetpixel($imres, $n++, $i, $pal[$p4]);
                    imagesetpixel($imres, $n++, $i, $pal[$p5]);
                    imagesetpixel($imres, $n++, $i, $pal[$p6]);
                    imagesetpixel($imres, $n++, $i, $pal[$p7]);
                    imagesetpixel($imres, $n++, $i, $pal[$p8]);
                }
            }
        }
        ob_start();
        imagepng($imres);
        $imres = ob_get_contents();
        ob_end_clean();
        return base64_encode($imres);
    }

    /**
     * @param $a WoestatsGuild
     * @param $b WoestatsGuild
     * @return mixed
     */
    private function sortHoldTime($a, $b)
    {
        if ($b->getHoldTime() == $a->getHoldTime()) // Hold times are the same, use kills to sort instead
            return ($b->getKills() - $a->getKills());
        return ($b->getHoldTime() - $a->getHoldTime());
    }

    /**
     * @param $a array
     * @param $b array
     * @return mixed
     */
    private function sortByName($a, $b)
    {
        return strcasecmp($a['name'],$b['name']);
    }
}
