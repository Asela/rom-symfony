<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class APIController
 * @package AppBundle\Controller
 */
class APIController extends Controller
{

    /**
     * @Route("/api/vendors")
     */
    public function apiVendorsAction()
    {
        $vendors['data'] = $this->get('app.cache_manager')->getVendingList();
        return new Response(addslashes(json_encode($vendors)));
    }

    /**
     * @Route("/api/buyers")
     */
    public function apiBuyingstoreAction()
    {
        $buyers['data'] = $this->get('app.cache_manager')->getBuyingStoreList();
        return new Response(addslashes(json_encode($buyers)));
    }

}
