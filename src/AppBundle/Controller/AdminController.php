<?php

namespace AppBundle\Controller;

use AppBundle\Controller\ENUM\Gamestate;
use AppBundle\Entity\AccRegNum;
use AppBundle\Entity\Character;
use AppBundle\Entity\GameAccount;
use AppBundle\Entity\PendingCashpoints;
use AppBundle\Entity\User;
use AppBundle\Entity\WebActionLog;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\DataTransformer\NumberToLocalizedStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AdminController
 * @package AppBundle\Controller
 * @Security("has_role('ROLE_STAFF')")
 * @Route("/admin")
 */
class AdminController extends Controller
{

    /**
     * @Route("/", name="admin_index")
     */
    public function adminPanelAction()
    {
        // These are cached for 5 minutes
        $online_accounts = $this->get('app.cache_manager')->getOnlineCount();
        $daily_active = $this->get('app.cache_manager')->getDailyActive();
        $weekly_active = $this->get('app.cache_manager')->getWeeklyActive();

        // Rest are cached every 30 minutes
        $total_master_accounts = $this->get('app.cache_manager')->getTotalMasterAccounts();
        $total_game_accounts = $this->get('app.cache_manager')->getTotalGameAccounts();
        $avg_game_per_master = 0;
        if ($total_master_accounts > 0)
            $avg_game_per_master = $total_game_accounts / $total_master_accounts;

        $registrations_day = $this->get('app.cache_manager')->getDailyRegistrations();
        $registrations_week = $this->get('app.cache_manager')->getWeeklyRegistrations();
        $active_zeny = $this->get('app.cache_manager')->getActiveZeny();
        $zeny_earned = $this->get('app.cache_manager')->getZenyEarned();
        $active_points = $this->get('app.cache_manager')->getActivePoints();
        $points_spent = $this->get('app.cache_manager')->getPointsSpent();
        $total_banned = $this->get('app.cache_manager')->getTotalBans();
        $weekly_bans = $this->get('app.cache_manager')->getWeeklyBans();
        $demographics['data'] = $this->get('app.cache_manager')->getDemographics();

        return $this->render('AppBundle:Admin:index.html.twig', [
            'current_players' => number_format($online_accounts),
            'registrations_day' => number_format($registrations_day),
            'registrations_week' => number_format($registrations_week),
            'daily_active_users' => number_format($daily_active),
            'biweekly_active_users' => number_format($weekly_active),
            'total_master_accounts' => number_format($total_master_accounts),
            'total_game_accounts' => number_format($total_game_accounts),
            'avg_game_per_master' => number_format($avg_game_per_master),
            'active_zeny' => number_format($active_zeny),
            'zeny_earned' => number_format($zeny_earned),
            'active_points' => number_format($active_points),
            'points_spent' => number_format($points_spent),
            'total_banned' => $total_banned,
            'weekly_bans' => $weekly_bans,
            'demographic_list' => json_encode($demographics)
            ]);
    }

    /**
     * @Route("/search", name="admin_search")
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $search = $request->request->get('search');

        $characters['data'] = array();
        $gameAccounts['data'] = array();
        $users['data'] = array();
        if (isset($search)) {
            $characters['data'] = $em->getRepository('AppBundle:Character')->searchCharacters($search);
            $gameAccounts['data'] = $em->getRepository('AppBundle:GameAccount')->searchGameAccounts($search);
            $users['data'] = $em->getRepository('AppBundle:User')->searchUsers($search);
        }

        return $this->render('AppBundle:Admin:search.html.twig', [
            'search' => $search,
            'user_list' => json_encode($users),
            'gameaccount_list' => json_encode($gameAccounts),
            'character_list' => json_encode($characters)
            ]);
    }

    /**
     * @Route("/staff", name="admin_staff_list")
     */
    public function staffAction()
    {
        $em = $this->getDoctrine()->getManager();
        $staff_list['data'] = $em->getRepository('AppBundle:User')->getStaffAccounts();

        $conn = $this->get('database_connection');
        $userPunishLogs['data'] = $conn->fetchAll("CALL getLatestUserPunishment()");
        $userWebActionLogs['data'] = $conn->fetchAll("CALL getLatestUserWebActionLogs()");

        return $this->render('AppBundle:Admin:staff.html.twig', [
            'staff_list' => json_encode($staff_list),
            'punish_logs' => addslashes(json_encode($userPunishLogs)),
            'web_logs' => addslashes(json_encode($userWebActionLogs)),
            ]);
    }

    /**
     * @Route("/user/{id}", name="admin_manage_user", requirements={"id": "\d+"})
     * @var integer $id
     * @return Response
     */
    public function adminManageUserAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);

        if ($user === NULL) {
            throw $this->createNotFoundException('User not found');
        }

        $isStaff = in_array("ROLE_STAFF", $user->getRoles());
        $allowRemove = !in_array("ROLE_ADMIN", $user->getRoles()) && !in_array("ROLE_SUPER_ADMIN", $user->getRoles());

        $conn = $this->get('database_connection');
        $userPunishLogs['data'] = $conn->fetchAll("CALL getUserPunishment(".$user->getMasterId().")");
        $userWebBanLogs['data'] = $conn->fetchAll("CALL getUserWebActionLogs(".$user->getMasterId().")");

        $receivedPoints = $em->getRepository('AppBundle:PendingCashpoints')->getMasterReceivedPoints($user->getMasterId());
        $pendingPoints = $em->getRepository('AppBundle:PendingCashpoints')->getMasterPendingPoints($user->getMasterId());

        $data = $user->jsonGameAccounts(true);

        /** @var GameAccount $gameAccount */
        foreach ($data as $gaKey => $gameAccount) {
            $data[$gaKey]['pending_points'] = $em->getRepository('AppBundle:PendingCashpoints')->getPendingPoints($gameAccount['account_id']);
            /** @var Character $char */
            foreach ($gameAccount['chars'] as $chKey => $char) {
                $g = $em->getRepository('AppBundle:Guild')->find($char['guild']);

                $data[$gaKey]['chars'][$chKey]['guild'] = '';
                if ($g !== NULL)
                    $data[$gaKey]['chars'][$chKey]['guild'] = $g->getName();
            }
        }

        $gameAccounts['data'] = $data;

        return $this->render('AppBundle:Admin:manage_user.html.twig', [
            'master_account' => $user,
            'master_banned' => $user->getState() == Gamestate::MASTER_BAN,
            'gameaccounts' => json_encode($gameAccounts),
            'punish_logs' => addslashes(json_encode($userPunishLogs)),
            'web_logs' => addslashes(json_encode($userWebBanLogs)),
            'isStaff' => $isStaff,
            'allowRemove' => $allowRemove,
            'unconfirmed' => $user->getState() == Gamestate::EMAIL_UNCONFIRMED,
            'received_points' => number_format($receivedPoints),
            'pending_points' => number_format($pendingPoints)
        ]);
    }


    /**
     * @Route("/ban/user", name="admin_ban_user")
     * @param Request $request
     * @param bool $modal
     * @return JsonResponse|Response
     */
    public function banUserAction(Request $request, $modal = false)
    {
        if (!$request->isXmlHttpRequest() && !$modal) $this->redirectToRoute('admin_index');

        $form = $this->get('form.factory')->createNamedBuilder('ban_user_form')
            ->add('masterId', HiddenType::class, array(
                'error_bubbling' => true,
            ))
            ->add('reason', TextareaType::class, array(
                'error_bubbling' => true,
                'label' => 'trans.admin.ban_reason'
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            $errors = array();
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                if(is_numeric($data['masterId'])) {
                    $em = $this->getDoctrine()->getManager();
                    /** @var User $user */
                    $user = $em->getRepository('AppBundle:User')->find($data['masterId']);
                    if ($user === NULL) {
                        $errors['error'] = $this->get('translator')->trans('trans.admin.missing_account');
                    } else {
                        $this->get('app.ban_support')->banUser($this->getUser()->getMasterId(),$user,$data['reason']);
                        return new JsonResponse(array('success' => true), Response::HTTP_OK);
                    }
                } else {
                    $errors['error'] = $this->get('translator')->trans('trans.admin.missing_account');
                }
            }
            $errorsList = $form->getErrors(true);
            foreach ($errorsList as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }
            return new JsonResponse($errors, Response::HTTP_OK);
        }

        return $this->render('AppBundle:Admin/Modal:ban_user.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/unban/user", name="admin_unban_user")
     * @param Request $request
     * @param bool $modal
     * @return JsonResponse|Response
     */
    public function unbanUserAction(Request $request, $modal = false)
    {
        if (!$request->isXmlHttpRequest() && !$modal) $this->redirectToRoute('admin_index');

        $form = $this->get('form.factory')->createNamedBuilder('unban_user_form')
            ->add('masterId', HiddenType::class, array(
                'error_bubbling' => true,
            ))
            ->add('reason', TextareaType::class, array(
                'error_bubbling' => true,
                'label' => 'trans.admin.unban_reason'
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            $errors = array();
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                if(is_numeric($data['masterId'])) {
                    $em = $this->getDoctrine()->getManager();
                    /** @var User $user */
                    $user = $em->getRepository('AppBundle:User')->find($data['masterId']);
                    if ($user === NULL) {
                        $errors['error'] = $this->get('translator')->trans('trans.admin.missing_account');
                    } else {
                        $this->get('app.ban_support')->unbanUser($this->getUser()->getMasterId(),$user,$data['reason']);
                        return new JsonResponse(array('success' => true), Response::HTTP_OK);
                    }
                } else {
                    $errors['error'] = $this->get('translator')->trans('trans.admin.missing_account');
                }
            }
            $errorsList = $form->getErrors(true);
            foreach ($errorsList as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }
            return new JsonResponse($errors, Response::HTTP_OK);
        }

        return $this->render('AppBundle:Admin/Modal:unban_user.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/ban/gameaccount", name="admin_ban_gameaccount")
     * @param Request $request
     * @param bool $modal
     * @return JsonResponse|Response
     */
    public function banGameAccountAction(Request $request, $modal = false)
    {
        if (!$request->isXmlHttpRequest() && !$modal) $this->redirectToRoute('admin_index');

        $form = $this->get('form.factory')->createNamedBuilder('ban_gameaccount_form')
            ->add('accountId', HiddenType::class, array(
                'error_bubbling' => true,
            ))
            ->add('reason', TextareaType::class, array(
                'error_bubbling' => true,
                'label' => 'trans.admin.ban_reason'
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            $errors = array();
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                if(is_numeric($data['accountId'])) {
                    $em = $this->getDoctrine()->getManager();
                    /** @var GameAccount $gameAccount */
                    $gameAccount = $em->getRepository('AppBundle:GameAccount')->find($data['accountId']);
                    if ($gameAccount === NULL) {
                        $errors['error'] = $this->get('translator')->trans('trans.admin.missing_account');
                    } else {
                        $this->get('app.ban_support')->banGameAccount($this->getUser()->getMasterId(),$gameAccount,$data['reason']);
                        return new JsonResponse(array('success' => true), Response::HTTP_OK);
                    }
                } else {
                    $errors['error'] = $this->get('translator')->trans('trans.admin.missing_account');
                }
            }
            $errorsList = $form->getErrors(true);
            foreach ($errorsList as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }
            return new JsonResponse($errors, Response::HTTP_OK);
        }

        return $this->render('AppBundle:Admin/Modal:ban_gameaccount.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/unban/gameaccount", name="admin_unban_gameaccount")
     * @param Request $request
     * @param bool $modal
     * @return JsonResponse|Response
     */
    public function unbanGameAccountAction(Request $request, $modal = false)
    {
        if (!$request->isXmlHttpRequest() && !$modal) $this->redirectToRoute('admin_index');

        $form = $this->get('form.factory')->createNamedBuilder('unban_gameaccount_form')
            ->add('accountId', HiddenType::class, array(
                'error_bubbling' => true,
            ))
            ->add('reason', TextareaType::class, array(
                'error_bubbling' => true,
                'label' => 'trans.admin.unban_reason'
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            $errors = array();
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                if(is_numeric($data['accountId'])) {
                    $em = $this->getDoctrine()->getManager();
                    /** @var GameAccount $gameAccount */
                    $gameAccount = $em->getRepository('AppBundle:GameAccount')->find($data['accountId']);
                    if ($gameAccount === NULL) {
                        $errors['error'] = $this->get('translator')->trans('trans.admin.missing_account');
                    } else {
                        $this->get('app.ban_support')->unbanGameAccount($this->getUser()->getMasterId(),$gameAccount,$data['reason']);
                        return new JsonResponse(array('success' => true), Response::HTTP_OK);
                    }
                } else {
                    $errors['error'] = $this->get('translator')->trans('trans.admin.missing_account');
                }
            }
            $errorsList = $form->getErrors(true);
            foreach ($errorsList as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }
            return new JsonResponse($errors, Response::HTTP_OK);
        }

        return $this->render('AppBundle:Admin/Modal:unban_gameaccount.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/mvps", name="admin_mvps")
     */
    public function mvpsAction()
    {
        $mvp_cards['data'] = $this->get('app.cache_manager')->getMVPCards();
        $mvps_killed['data'] = $this->get('app.cache_manager')->getMVPSKilled();
        $mvp_killers['data'] = $this->get('app.cache_manager')->getTopMVPKillers();

        return $this->render('AppBundle:Admin:mvps.html.twig', [
            'mvp_cards' => json_encode($mvp_cards),
            'mvps_killed' => json_encode($mvps_killed),
            'mvp_killers' => json_encode($mvp_killers)
        ]);
    }

    /**
     * @Route("/economy", name="admin_economy")
     */
    public function economyAction()
    {
        $top_zeny['data'] = $this->get('app.cache_manager')->getTopZenyHolders();
        $top_points['data'] = $this->get('app.cache_manager')->getTopPointsHolders();

        return $this->render('AppBundle:Admin:economy.html.twig', [
            'top_zeny' => json_encode($top_zeny),
            'top_points' => json_encode($top_points),
        ]);
    }

    /**
     * @Route("/donations", name="admin_donations")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function donationsAction()
    {
        $conn = $this->get('database_connection');
        $complete['data'] = $conn->fetchAll("CALL getDonationsCompleteLog()");
        $failed['data'] = $conn->fetchAll("CALL getDonationsFailLog()");

        return $this->render('AppBundle:Admin:donations.html.twig', [
            'complete' => json_encode($complete),
            'failed' => json_encode($failed),
        ]);
    }

    /**
     * @Route("/points/user", name="admin_update_points")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @param bool $modal
     * @return JsonResponse|Response
     */
    public function updatePointsAction(Request $request, $modal = false)
    {
        if (!$request->isXmlHttpRequest() && !$modal) $this->redirectToRoute('admin_index');

        $form = $this->get('form.factory')->createNamedBuilder('update_points_form')
            ->add('accountId', HiddenType::class, array(
                'error_bubbling' => true,
            ))
            ->add('amount', NumberType::class, array(
                'label' => 'trans.admin.eco.amount',
                'scale' => 0,
                'rounding_mode' => NumberToLocalizedStringTransformer::ROUND_FLOOR,
                'error_bubbling' => true
            ))
            ->add('reason', TextareaType::class, array(
                'error_bubbling' => true,
                'label' => 'trans.admin.update_points_reason'
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            $errors = array();
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                if (!is_numeric($data['amount'])) {
                    $errors['error'] = $this->get('translator')->trans('trans.admin.invalid_amount');
                } else if(is_numeric($data['accountId'])) {
                    $em = $this->getDoctrine()->getManager();
                    /** @var GameAccount $gameAccount */
                    $gameAccount = $em->getRepository('AppBundle:GameAccount')->find($data['accountId']);
                    if ($gameAccount === NULL) {
                        $errors['error'] = $this->get('translator')->trans('trans.admin.missing_account');
                    } else {
                        if ($gameAccount->getPoints() === NULL) {
                            $points = new AccRegNum('#CASHPOINTS',0);
                            $gameAccount->addAccRegNum($points);
                            $em->persist($points);
                        }
                        if ( ($gameAccount->getPoints()->getValue() + $data['amount']) < 0) {
                            $errors['error'] = $this->get('translator')->trans('trans.admin.update_points_negative');
                        } else {
                            $isOnline = $em->getRepository('AppBundle:Character')->isAccountOnline($gameAccount->getAccountId());

                            $this->get('app.mailer')->sendAdminPointUpdate($gameAccount, $data['amount'], $data['reason']);
                            if ($isOnline && $data['amount'] < 0) {
                                $errors['error'] = $this->get('translator')->trans('trans.admin.user_online');
                            } else {
                                if ($data['amount'] < 0) {
                                    $reason = 'Removed '.abs($data['amount']).' Cash Points';
                                } else {
                                    $reason = 'Added '.$data['amount'].' Cash Points';
                                }
                                $em->persist(new WebActionLog($this->getUser()->getMasterId(), $gameAccount->getMasterId(), $gameAccount->getAccountId(), $reason, $data['reason']));
                                if ($isOnline) {
                                    $pendingPoints = new PendingCashpoints($gameAccount->getMasterId(),$gameAccount->getAccountId(), $data['amount'], false);
                                } else {
                                    $gameAccount->getPoints()->updateValue($data['amount']);
                                    $pendingPoints = new PendingCashpoints($gameAccount->getMasterId(),$gameAccount->getAccountId(), $data['amount'], true);
                                }
                                $woeCashPoints = new PendingCashpoints($gameAccount->getMasterId(),$gameAccount->getAccountId(), $data['amount'], false);
                                $wm = $this->get('doctrine')->getManager('woe');

                                $em->persist($pendingPoints);
                                $wm->persist($woeCashPoints);

                                $em->flush();
                                $wm->flush();
                                return new JsonResponse(array('success' => true), Response::HTTP_OK);
                            }
                        }
                    }
                } else {
                    $errors['error'] = $this->get('translator')->trans('trans.admin.missing_account');
                }
            }
            $errorsList = $form->getErrors(true);
            foreach ($errorsList as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }
            return new JsonResponse($errors, Response::HTTP_OK);
        }

        return $this->render('AppBundle:Admin/Modal:update_points.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/unconfirmed", name="admin_unconfirmed_list")
     */
    public function unconfirmedUsersAction()
    {
        $conn = $this->get('database_connection');
        $users['data'] = $conn->fetchAll("CALL getUsersByState('".Gamestate::EMAIL_UNCONFIRMED."')");

        return $this->render('AppBundle:Admin:unconfirmed_accounts.html.twig', [
            'user_list' => json_encode($users)
        ]);
    }

    /**
     * @Route("/reward/user", name="admin_reward_gameaccount")
     * @param Request $request
     * @param bool $modal
     * @return JsonResponse|Response
     */
    public function rewardGameAccountAction(Request $request, $modal = false)
    {
        if (!$request->isXmlHttpRequest() && !$modal) $this->redirectToRoute('admin_index');

        $form = $this->get('form.factory')->createNamedBuilder('reward_gameaccount_form')
            ->add('accountId', HiddenType::class, array(
                'error_bubbling' => true,
            ))
            ->add('item', ChoiceType::class, array(
                'choices' => array(
                    'Event Coupon' => '#GM_40004',
                    'Mysterious Dyestuffs' => '#GM_6220',
                    'Mystery Egg' => '#GM_40096',
                    'Ancient Silver Coin' => '#GM_40029',
                    'Midgard Coin' => '#GM_40030',
                    'Costume Ticket' => '#GM_40003',
                    'Costume Creative Bonnet [Art Contest Reward ONLY]' => "#GM_34555"
                )
            ))
            ->add('reason', TextareaType::class, array(
                'error_bubbling' => true,
                'label' => 'trans.admin.give_item_reason'
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            $errors = array();
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();

                $allowed_types = ['#GM_40004','#GM_6220','#GM_40096','#GM_40029','#GM_40030','#GM_40003','#GM_34555'];
                if (!in_array($data['item'],$allowed_types)) {
                    $errors['error'] = $this->get('translator')->trans('trans.admin.invalid_item');
                } else if(is_numeric($data['accountId'])) {
                    $em = $this->getDoctrine()->getManager();
                    /** @var GameAccount $gameAccount */
                    $gameAccount = $em->getRepository('AppBundle:GameAccount')->find($data['accountId']);
                    if ($gameAccount === NULL) {
                        $errors['error'] = $this->get('translator')->trans('trans.admin.missing_account');
                    } else {
                        $reward = $gameAccount->getAccRegNum()->get($data['item']);
                        if ($reward === NULL) {
                            $reward = new AccRegNum($data['item'],0);
                            $gameAccount->addAccRegNum($reward);
                            $em->persist($reward);
                        }

                        $reward->updateValue(1);

                        $reason = '';
                        switch($data['item']) {
                            case '#GM_40004': $reason = "Awarded 1 Event Coupon"; break;
                            case '#GM_6220': $reason = "Awarded 1 Mysterious Dyestuff"; break;
                            case '#GM_40096': $reason = "Awarded 1 Mystery Egg"; break;
                            case '#GM_40029': $reason = "Awarded 1 Ancient Silver Coin"; break;
                            case '#GM_40030': $reason = "Awarded 1 Midgard Coin"; break;
                            case '#GM_40003': $reason = "Awarded 1 Costume Ticket"; break;
                            case '#GM_34555': $reason = "Awarded 1 Costume Creative Bonnet (Art Contest Prize)"; break;
                            default: $reason = "REWARD FAILURE. ABORT.";
                        }

                        $em->persist(new WebActionLog($this->getUser()->getMasterId(), $gameAccount->getMasterId(), $gameAccount->getAccountId(), $reason, $data['reason']));
                        $em->flush();
                        return new JsonResponse(array('success' => true), Response::HTTP_OK);
                    }
                } else {
                    $errors['error'] = $this->get('translator')->trans('trans.admin.missing_account');
                }
            }
            $errorsList = $form->getErrors(true);
            foreach ($errorsList as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }
            return new JsonResponse($errors, Response::HTTP_OK);
        }

        return $this->render('AppBundle:Admin/Modal:reward_gameaccount.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/move/gameaccount", name="admin_move_gameaccount")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @param bool $modal
     * @return JsonResponse|Response
     */
    public function moveGameAccountAction(Request $request, $modal = false)
    {
        if (!$request->isXmlHttpRequest() && !$modal) $this->redirectToRoute('admin_index');

        $form = $this->get('form.factory')->createNamedBuilder('move_gameaccount_form')
            ->add('accountId', HiddenType::class, array(
                'error_bubbling' => true,
            ))
            ->add('newMasterId', NumberType::class, array(
                'scale' => 0,
                'error_bubbling' => true,
                'label' => 'trans.admin.new_master_id'
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            $errors = array();
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                if(is_numeric($data['accountId']) && is_numeric($data['newMasterId'])) {
                    $em = $this->getDoctrine()->getManager();
                    /** @var GameAccount $gameAccount */
                    $gameAccount = $em->getRepository('AppBundle:GameAccount')->find($data['accountId']);
                    /** @var User $newMaster */
                    $newMaster = $em->getRepository('AppBundle:User')->find($data['newMasterId']);

                    if ($gameAccount === NULL || $newMaster === NULL) {
                        $errors['error'] = $this->get('translator')->trans('trans.admin.missing_account');
                    } else {
                        $isOnline = $em->getRepository('AppBundle:Character')->isAccountOnline($gameAccount->getAccountId());
                        if ($isOnline) {
                            $errors['error'] = $this->get('translator')->trans('trans.admin.move_online');
                        } else {
                            $oldMasterId = $gameAccount->getMasterId();
                            $newMaster->addGameAccount($gameAccount);

                            $type = "Moved Game Account";
                            $reason = "Account ID " . $gameAccount->getAccountId() . " to Master ID " . $newMaster->getMasterId();
                            $em->persist(new WebActionLog($this->getUser()->getMasterId(), $oldMasterId, $gameAccount->getAccountId(), $type, $reason));

                            $reason = "Account ID " . $gameAccount->getAccountId() . " from Master ID " . $oldMasterId;
                            $em->persist(new WebActionLog($this->getUser()->getMasterId(), $gameAccount->getMasterId(), $gameAccount->getAccountId(), $type, $reason));

                            $em->flush();
                            return new JsonResponse(array('success' => true), Response::HTTP_OK);
                        }
                    }
                } else {
                    $errors['error'] = $this->get('translator')->trans('trans.admin.missing_account');
                }
            }
            $errorsList = $form->getErrors(true);
            foreach ($errorsList as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }
            return new JsonResponse($errors, Response::HTTP_OK);
        }

        return $this->render('AppBundle:Admin/Modal:move_gameaccount.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
