<?php

namespace AppBundle\Controller;

use AppBundle\Controller\ENUM\Gamestate;
use AppBundle\Entity\GameAccount;
use AppBundle\Entity\PasswordRecovery;
use AppBundle\Entity\Referral;
use AppBundle\Entity\User;
use AppBundle\Entity\WebActionLog;
use AppBundle\Entity\EmailConfirmation;
use AppBundle\Form\AdminEditUserForm;
use AppBundle\Form\Model\AdminEditUserAccount;
use AppBundle\Form\Model\BaseUser;
use AppBundle\Form\Model\EditUserAccount;
use AppBundle\Form\RecoverPasswordForm;
use AppBundle\Form\UserRegistrationForm;
use AppBundle\Form\UserEditForm;
use AppBundle\Form\EmailForm;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SecurityController
 * @package AppBundle\Controller
 *
 */
class SecurityController extends Controller
{
    /**
     * @Route("/login", name="security_login")
     */
    public function loginAction(Request $request, $modal = false)
    {
        $user = $this->getUser();
        if ($user instanceof User) {
            return $this->redirectToRoute('app_pages_index');
        }

		$authenticationUtils = $this->get('security.authentication_utils');
        // get the login error if there is one
        $errors = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse(['error' => ($errors ? $errors->getMessageKey() : '')], Response::HTTP_OK);
        }

        if ($modal) {
            $template = 'AppBundle:Security:login_modal.html.twig';
        } else {
            $template = 'AppBundle:Security:login.html.twig';
        }
        return $this->render($template, [
            'last_username' => $lastUsername,
            'error'  => $errors,
        ]);
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logoutAction()
    {
        throw new \Exception('this should not be reached!');
    }

    /**
     * @param Request $request
     * @param Form $form
     * @param User $user
     * @return bool
     */
    private function registerUser(Request $request, Form $form, User $user) {

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->setRoles(['ROLE_USER']);
            $user->setRegistrationIp($request->getClientIp());
            $user->setRegistrationTime();
            $user->setLastIp($request->getClientIp());
            $user->setLastLogin();
			$referralCode = $user->getReferralCode(); // Check if this user registered via referral
			$user->setReferralCode(''); // Set blank referral code before the account is saved

            /** @var GameAccount $gameaccount */
            foreach($user->getGameAccounts() as $gameaccount) {
                $gameaccount->setUserPass(md5($user->getPlainPassword()));
                $gameaccount->setEmail($user->getEmail());
                $gameaccount->setBirthdate($user->getBirthdate());
            }

            $em = $this->getDoctrine()->getManager();

            $emailConf = new EmailConfirmation($user);

            $em->persist($user);
            $em->persist($emailConf);
            $em->flush();

			// Now that the account has a Master ID, we can add referral data to the db
            /** @var User $referrer */
			if($referralCode && ($referrer = $em->getRepository('AppBundle:User')->findOneBy(['referralCode' => $referralCode]))) { // Check that the code actually exists
                $em->persist(new Referral($user->getMasterId(), $referrer->getMasterId()));
			}
			// Hash Master ID into user's referral code
            $user->setReferralCode(str_replace(array('+','/'), array('-','_'), openssl_encrypt($user->getMasterId(),
                $this->getParameter('app.referral_method'),
                $this->getParameter('app.referral_password'), 0,
                $this->getParameter('app.referral_iv'))));
            $em->flush();
	
            $this->get('app.mailer')->sendConfirmationMail($user->getEmail(), $emailConf->getCode());

            $this->get('security.authentication.guard_handler')->authenticateUserAndHandleSuccess($user,$request,$this->get('app.security.login_form_authenticator'),'main');

            return true;
        }

        return false;
    }

    /**
     * @Route("/register", name="security_register")
	 * @Route("/register/{referralCode}", name="security_register")
     * @param Request $request
     * @param bool $modal
     * @return null|JsonResponse|Response
     */
    public function registerAction(Request $request, $modal = false, $referralCode = NULL)
    {
        /** @var User $user*/
        $user = new User();
        $user->setState(Gamestate::EMAIL_UNCONFIRMED);
        $gameAccount = new GameAccount();
        $gameAccount->setState(Gamestate::EMAIL_UNCONFIRMED);
        $user->addGameAccount($gameAccount);

        $form = $this->createForm(UserRegistrationForm::class, $user, ['error_bubbling' => $request->isXmlHttpRequest()]);
		if($referralCode) $form->get('referralCode')->setData($referralCode);
        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            if ($this->registerUser($request, $form, $user)) {
                return new JsonResponse(array('success' => true), Response::HTTP_OK);
            }

            $errorsList = $form->getErrors(true);
            $errors = array();
            foreach ($errorsList as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }

            return new JsonResponse($errors, Response::HTTP_OK);
        } else {
            if ($this->getUser() instanceof User) {
                return $this->redirectToRoute('app_pages_index');
            }

            if ($this->registerUser($request,$form, $user)) {
                return $this->redirectToRoute('app_pages_index');
            }

            if ($modal) {
                return $this->render('AppBundle:Security:register_modal.html.twig', [
                    'form' => $form->createView()
                ]);
            } else {
                return $this->render('AppBundle:Security:register.html.twig', [
                    'form' => $form->createView()
                ]);
            }
        }
    }


    /**
     * @Route("/confirm/{key}", name="security_confirm_email", requirements={"key": "\w{32}"})
     * @param string $key
     * @return Response
     */
    public function confirmEmailAction($key)
    {
        $em = $this->getDoctrine()->getManager();

        $emailConf = $em->getRepository('AppBundle:EmailConfirmation')->findOneBy(["code" => $key]);

        $confirmed = false;

        if ($emailConf !== NULL) {
            // This works under assumption that webConf date is in the past
            $diff = $emailConf->getDate()->diff(new \DateTime())->days;

            // Less than 2 days from confirmation sent
            if ($diff < 2) {
                // Note: This updates the state on both User and it's GameAccounts
                // If the state of any of those is NOT email_unconfirmed, this does nothing
                $emailConf->getMasterAccount()->confirmEmail();
                $em->remove($emailConf);
                $em->flush();
                $confirmed = true;
            }
        }

        return $this->render('AppBundle:Security:confirm_email.html.twig', [
            'confirmed'  => $confirmed
        ]);
    }

    /**
     * @Route("/confirm/resend", name="security_resend_email_confirm")
     * @param Request $request
     * @return Response
     */
    public function resendEmailConfirmationAction(Request $request)
    {
        $form = $this->createForm(EmailForm::class);
        $form->handleRequest($request);

        $sent = false;
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('AppBundle:User')->findOneBy(['email' => $data['email']]);
            if ($user !== NULL) {
                if ($user->getState() == Gamestate::EMAIL_UNCONFIRMED) {
                    $emailConf = $em->getRepository('AppBundle:EmailConfirmation')->find($user->getMasterId());
                    if ($emailConf !== NULL) {
                        $emailConf->update();
                    } else {
                        $emailConf = new EmailConfirmation($user);
                        $em->persist($emailConf);
                    }
                    $this->get('app.mailer')->sendConfirmationMail($user->getEmail(), $emailConf->getCode());
                    $em->flush();
                }
            }

            $sent = true;
        }

        return $this->render('AppBundle:Security:resend_confirmation.html.twig', [
            'form'  => $form->createView(),
            'sent'  => $sent
        ]);
    }


    /**
     * @Route("/recover", name="security_password_recovery")
     * @param Request $request
     * @return Response
     */
    public function passwordRecoveryAction(Request $request)
    {
        $entity = new BaseUser();
        $form = $this->createForm(EmailForm::class,$entity);
        $form->handleRequest($request);

        $sent = false;
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('AppBundle:User')->findOneBy(['email' => $entity->getEmail()]);
            if ($user !== NULL) {
                $passwordRecovery = $em->getRepository('AppBundle:PasswordRecovery')->find($user->getMasterId());
                if ($passwordRecovery !== NULL) {
                    $passwordRecovery->update();
                } else {
                    $passwordRecovery = new PasswordRecovery($user);
                    $em->persist($passwordRecovery);
                }
                $this->get('app.mailer')->sendPasswordReset($user->getEmail(), $passwordRecovery->getCode());
                $em->flush();
            }

            $sent = true;
        }

        return $this->render('AppBundle:Security:recover_password.html.twig', [
            'form'  => $form->createView(),
            'sent'  => $sent
        ]);
    }

    /**
     * @Route("/recovery/{key}", name="security_password_reset", requirements={"key": "\w{32}"})
     * @param Request $request
     * @param string $key
     * @return Response
     */
    public function passwordResetAction(Request $request, $key)
    {
        $entity = new BaseUser();
        $form = $this->createForm(RecoverPasswordForm::class, $entity);
        $form->handleRequest($request);

        // Statuses
        $recovered = false;
        $allow_recovery = false;
        $error = false;

        $em = $this->getDoctrine()->getManager();

        $passwordRecovery = $em->getRepository('AppBundle:PasswordRecovery')->findOneBy(["code" => $key]);

        if ($passwordRecovery !== NULL) {
            // This works under assumption that webConf date is in the past
            $diff = $passwordRecovery->getDate()->diff(new \DateTime())->days;
            // Less than 2 days from confirmation sent
            if ($diff < 2) {
                $allow_recovery = true;
                if ($form->isSubmitted() && $form->isValid()) {
                    $user = $em->getRepository('AppBundle:User')->find($passwordRecovery->getMasterId());
                    if ($user !== NULL) {
                        $user->setPlainPassword($entity->getNewPassword());
                        $em->remove($passwordRecovery);
                        $em->flush();
                        $recovered = true;
                    } else {
                        $error = true;
                    }
                }
            }
        }

        return $this->render('AppBundle:Security:reset_password.html.twig', [
            'form'  => $form->createView(),
            'recovered' => $recovered,
            'allow_recovery' => $allow_recovery,
            'error' => $error
        ]);
    }

    /**
     * @Route("/panel/edit", name="security_edit")
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function editAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var User $user*/
        $user = $this->getUser();

        $entity = new EditUserAccount();
        $entity->setEmail($user->getEmail());
        $entity->setAllowEmail($user->isAllowEmail());
        $entity->setCountry($user->getCountry());

        $form = $this->createForm(UserEditForm::class, $entity);
        $form->handleRequest($request);

        $saved = false;
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setEmail($entity->getEmail());
            $user->setAllowEmail($entity->isAllowEmail());
            $user->setCountry($entity->getCountry());
            if ($entity->getNewPassword()) {
                $user->setPlainPassword($entity->getNewPassword());
            }

            /** @var GameAccount $gameaccount */
            foreach($user->getGameAccounts() as $gameaccount) {
                $gameaccount->setEmail($user->getEmail());
            }
            $em->flush();

            $saved = true;
        }

        $em->refresh($user);

        return $this->render('AppBundle:Security:edit_user.html.twig', [
            'form' => $form->createView(),
            'saved' => $saved
        ]);
    }

    /**
     * @Route("/admin/user/edit/{id}", name="security_admin_edit", requirements={"id": "\d+"})
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @param $id
     * @param bool $modal
     * @return JsonResponse|Response
     */
    public function adminEditAction(Request $request,$id, $modal = false)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);

        if ($user === NULL) {
            throw $this->createNotFoundException('User not found');
        }
        if (!$request->isXmlHttpRequest() && !$modal) $this->redirectToRoute('admin_index');

        $entity = new AdminEditUserAccount();
        $entity->setEmail($user->getEmail());
        $entity->setAllowEmail($user->isAllowEmail());
        $entity->setBirthdate($user->getBirthdate());
        $entity->setCountry($user->getCountry());

        $form = $this->createForm(AdminEditUserForm::class, $entity);
        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            $errors = array();
            if ($form->isSubmitted() && $form->isValid()) {
                $user->setEmail($entity->getEmail());
                $user->setAllowEmail($entity->isAllowEmail());
                $user->setCountry($entity->getCountry());
                $user->setBirthdate($entity->getBirthdate());
                if ($entity->getNewPassword()) {
                    $user->setPlainPassword($entity->getNewPassword());
                }

                /** @var GameAccount $gameaccount */
                foreach ($user->getGameAccounts() as $gameaccount) {
                    $gameaccount->setEmail($user->getEmail());
                    $gameaccount->setBirthdate($user->getBirthdate());
                }
                $em->flush();

                return new JsonResponse(array('success' => true), Response::HTTP_OK);
            }
            $errorsList = $form->getErrors(true);
            foreach ($errorsList as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }
            return new JsonResponse($errors, Response::HTTP_OK);
        }

        $em->refresh($user);

        return $this->render('AppBundle:Security:edit_user_modal.html.twig', [
            'form' => $form->createView(),
            'master_id' => $user->getMasterId()
        ]);
    }

    /**
     * @Route("/admin/staff/edit", name="security_edit_staff")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @param bool $modal
     * @return JsonResponse|Response
     */
    public function editStaffAction(Request $request, $modal = false)
    {
        if (!$request->isXmlHttpRequest() && !$modal) $this->redirectToRoute('admin_staff_list');

        $form = $this->get('form.factory')->createNamedBuilder('edit_staff_form')
            ->add('masterId', HiddenType::class, array(
                'error_bubbling' => true,
            ))
            ->add('type', HiddenType::class, array(
                'error_bubbling' => true,
            ))
            ->getForm();
        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            $errors = array();
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                if (is_numeric($data['masterId'])) {
                    $em = $this->getDoctrine()->getManager();
                    $user = $em->getRepository('AppBundle:User')->find($data['masterId']);
                    if ($user === NULL) {
                        $errors['error'] = $this->get('translator')->trans('trans.admin.missing_account');
                    } else {
                        if(strcmp($data['type'],'R') == 0) {
                            $user->setRoles(['ROLE_USER']);
                            $em->persist(new WebActionLog($this->getUser()->getMasterId(),$user->getMasterId(),0,"Removed from Staff",""));
                        } else if (strcmp($data['type'],'A') == 0) {
                            $user->setRoles(['ROLE_STAFF']);
                            $em->persist(new WebActionLog($this->getUser()->getMasterId(),$user->getMasterId(),0,"Added to Staff",""));
                        }
                        $em->flush();
                        return new JsonResponse(array('success' => true), Response::HTTP_OK);
                    }
                } else {
                    $errors['error'] = $this->get('translator')->trans('trans.admin.missing_account');
                }
            }
            $errorsList = $form->getErrors(true);
            foreach ($errorsList as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }
            return new JsonResponse($errors, Response::HTTP_OK);
        }

        return $this->render('AppBundle:Admin/Modal:edit_staff.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/confirm/user", name="security_admin_confirm_user")
     * @Security("is_granted('ROLE_STAFF')")
     * @param Request $request
     * @param bool $modal
     * @return JsonResponse|Response
     */
    public function adminConfirmUserAction(Request $request, $modal = false)
    {
        if (!$request->isXmlHttpRequest() && !$modal) $this->redirectToRoute('admin_index');

        $form = $this->get('form.factory')->createNamedBuilder('confirm_user_form')
            ->add('masterId', HiddenType::class, array(
                'error_bubbling' => true,
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            $errors = array();
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                if(is_numeric($data['masterId'])) {
                    $em = $this->getDoctrine()->getManager();
                    $user = $em->getRepository('AppBundle:User')->find($data['masterId']);
                    if ($user === NULL) {
                        $errors['error'] = $this->get('translator')->trans('trans.admin.missing_account');
                    } else {
                        $user->confirmEmail();
                        $em->flush();
                        return new JsonResponse(array('success' => true), Response::HTTP_OK);
                    }
                } else {
                    $errors['error'] = $this->get('translator')->trans('trans.admin.missing_account');
                }
            }
            $errorsList = $form->getErrors(true);
            foreach ($errorsList as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }
            return new JsonResponse($errors, Response::HTTP_OK);
        }

        return $this->render('AppBundle:Admin/Modal:confirm_user.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
