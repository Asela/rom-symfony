<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FeedIo\Reader\Result as FeedIoResult;


/**
 * Class PagesController
 * @package AppBundle\Controller
 */
class PagesController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Pages:index.html.twig');
    }
    /**
     * @Route("/status")
     */
    public function statusAction()
    {
        $isalive = 0;
        if(@fsockopen("live.riseofmidgard.com",32690)) {
            $isalive = 1;
        }

        // Cached at 5 minute intervals
        $online_accounts = $this->get('app.cache_manager')->getOnlineCount();
        $daily_active = $this->get('app.cache_manager')->getDailyActive();
        $total_count = $this->get('app.cache_manager')->getTotalMasterAccounts();

        return $this->render('AppBundle:Pages:status.html.twig',[
                'player_count' => $online_accounts,
                'daily_active' => $daily_active,
                'total_count' => $total_count,
                'isalive' => $isalive
            ]
        );
    }
    
    /**
     * @Route("/terms")
     */
    public function termsAction()
    {
        return $this->render('AppBundle:Pages:terms.html.twig');
    }

     /**
     * @Route("/privacy")
     */
    public function privacyAction()
    {
        return $this->render('AppBundle:Pages:privacy.html.twig');
    }

    /**
     * @Route("/gmlogs")
     */
    public function gmlogsAction()
    {
        $logs['data'] = $this->get('app.cache_manager')->getGMLogs();
        return $this->render('AppBundle:Pages:gmlogs.html.twig', array(
            'logs' => addslashes(json_encode($logs)),
        ));
    }

    /**
     * @Route("/vendors")
     */
    public function vendorsAction()
    {
        $vendors['data'] = $this->get('app.cache_manager')->getVendingList();
        $active_zeny = $this->get('app.cache_manager')->getActiveZeny();

        return $this->render('AppBundle:Pages:vendorslist.html.twig',[
                'vendors' => addslashes(json_encode($vendors)),
                'active_zeny' => number_format($active_zeny)
            ]
        );
    }

	/**
     * @Route("/buyers")
     */
    public function buyingstoreAction()
    {
        $buyers['data'] = $this->get('app.cache_manager')->getBuyingStoreList();
        $active_zeny = $this->get('app.cache_manager')->getActiveZeny();

        return $this->render('AppBundle:Pages:buyingstorelist.html.twig', [
                'buyers' => addslashes(json_encode($buyers)),
                'active_zeny' => number_format($active_zeny)
            ]
        );
    }
    /**
     * @Route("/information")
     */
    public function informationAction()
    {
        return $this->render('AppBundle:Pages:information.html.twig');
    }

    /**
     * @Route("/about")
     */
    public function aboutAction()
    {
        return $this->render('AppBundle:Pages:about.html.twig');
    }

    /**
     * @Route("/cashshop")
     */
    public function cashshopAction()
    {
        $account_cp_bonus = 0;

        if ($this->isGranted("ROLE_USER")) {
            $user = $this->getUser();
            $em = $this->getDoctrine()->getManager();
            $cp_bonus = $em->getRepository('AppBundle:CpBonus')->getUserBonus($user->getMasterId());
            if ($cp_bonus) $account_cp_bonus = $cp_bonus->getAmount();
            else $account_cp_bonus = 0;
        }
        return $this->render('AppBundle:Pages:cashshop.html.twig', array('discount10' => ($account_cp_bonus==10), 'discount20' => ($account_cp_bonus==20)));
    }

    /**
     * @Route("/news")
     */
    public function newsAction()
    {
        // create a simple FeedIo instance
        $feedIo = \FeedIo\Factory::create()->getFeedIo();
        $news_announces = $this->processNewsFeed($feedIo->read("https://riseofmidgard.com/forum/feed.php?mode=announcements"));
        $news_patches = $this->processNewsFeed($feedIo->read("https://riseofmidgard.com/forum/feed.php?mode=patches"));

        return $this->render('AppBundle:Pages:news.html.twig',['announcements'=>$news_announces,'patches'=>$news_patches]);
    }

    /**
     * Processes news feeds into usable arrays
     * @param FeedIoResult $result
     * @return array
     */
    private function processNewsFeed(FeedIoResult $result)
    {
        $items = array();
        /** @var \FeedIo\Feed\Item\ $item */
        foreach( $result->getFeed() as $item ) {
            $entry = array();
            $entry['title'] = $item->getTitle();
            $entry['link'] = $item->getLink();
            $entry['published'] = \DateTime::createFromFormat(\DateTime::ATOM, $item->getValue('published'))->format('Y-m-d H:i:s');
            $entry['updated'] = $item->getLastModified()->format('Y-m-d H:i:s');
            $entry['content'] = $item->getDescription();
            $entry['author'] = $item->getValue('author');

            $items[] = $entry;
        }
        return $items;
    }
}
