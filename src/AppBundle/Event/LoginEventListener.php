<?php

namespace AppBundle\Event;


use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class LoginEventListener
{

    /**
     * @var EntityManager
     */
    protected $_em;

    /**
     * @var RequestStack
     */
    private $_stack;

    public function __construct(EntityManager $em, RequestStack $stack)
    {
        $this->_em = $em;
        $this->_stack = $stack;
    }

    /**
     * Catches the login of a user and does something with it
     *
     * @param InteractiveLoginEvent $event
     * @return void
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $token = $event->getAuthenticationToken();
        if ($token && $token->getUser() instanceof User)
        {
            $token->getUser()->setLastLogin();
            $token->getUser()->setLastIp($this->_stack->getCurrentRequest()->getClientIp());
            $this->_em->flush();
        }
    }
}
