<?php

namespace AppBundle\Form;

use AppBundle\Entity\Character;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CharSubForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => array('maxlength' => 30),
                'label' => 'trans.character.name',
                'error_bubbling' => $options['error_bubbling']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Character::class,
        ));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'error_bubbling' => true
        );
    }

}
