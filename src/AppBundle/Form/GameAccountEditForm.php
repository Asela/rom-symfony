<?php

namespace AppBundle\Form;

use AppBundle\Form\Model\EditGameAccount;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GameAccountEditForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('accountId', HiddenType::class, [
                'data' => 0
            ])
            ->add('newPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_name' => 'new_password',
                'second_name' => 'confirm_new_password',
                'first_options'  => array('label' => 'trans.game_account.new_password'),
                'second_options' => array('label' => 'trans.game_account.confirm_new_password'),
                'error_bubbling' => true,
                'invalid_message' => 'trans.form.password_match'
            ])
            ->add('currentPassword', PasswordType::class, [
                'label' => 'trans.form.master_password',
                'error_bubbling' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EditGameAccount::class
        ]);
    }
}
