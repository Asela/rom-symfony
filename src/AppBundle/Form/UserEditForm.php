<?php

namespace AppBundle\Form;

use AppBundle\Form\Model\EditUserAccount;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserEditForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'trans.form.email',
                'attr' => array('maxlength' => 39)
            ])
            ->add('newPassword', RepeatedType::class, [
                'attr' => ['maxlength' => 23],
                'type' => PasswordType::class,
                'first_name' => 'password',
                'second_name' => 'confirm_password',
                'first_options'  => array('label' => 'trans.form.new_password'),
                'second_options' => array('label' => 'trans.form.confirm_password'),
                'required' => false,
                'invalid_message' => 'validtrans.password_match'
            ])
            ->add('country', CountryType::class, [
                'label' => 'trans.form.country'
            ])
            ->add('allowEmail', CheckboxType::class, [
                'label' => 'trans.form.email_allowed',
                'required' => false
            ])
            ->add('oldPassword', PasswordType::class, [
                'label' => 'trans.form.current_password',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EditUserAccount::class,
            'validation_groups' => array('Default')
        ]);
    }
}
