<?php

namespace AppBundle\Form;

use AppBundle\Form\Model\BaseUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmailForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'trans.form.email',
                'attr' => array('maxlength' => 39),
                'error_bubbling' => true
            ])
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return array(
            'data_class' => BaseUser::class,
            'validation_groups' => array('email')
        );
    }

    public function getName()
    {
        return 'email_form';
    }
}
