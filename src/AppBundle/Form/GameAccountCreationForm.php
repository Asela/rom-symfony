<?php

namespace AppBundle\Form;

use AppBundle\Entity\GameAccount;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GameAccountCreationForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userid', TextType::class, [
                'attr' => array('maxlength' => 23),
                'label' => 'trans.game_account.userid',
                'error_bubbling' => true
            ])
            ->add('plainPassword', RepeatedType::class, [
                'attr' => ['maxlength' => 23],
                'type' => PasswordType::class,
                'first_name' => 'password',
                'second_name' => 'confirm_password',
                'first_options'  => array('label' => 'trans.game_account.password'),
                'second_options' => array('label' => 'trans.game_account.confirm_password'),
                'error_bubbling' => true
            ])
            ->add('sex', ChoiceType::class, [
                'choices' => array(
                    'trans.form.male' => 'M',
                    'trans.form.female' => 'F',
                ),
                'expanded' => true,
                'label' => 'trans.character.gender',
                'error_bubbling' => true
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GameAccount::class,
            'validation_groups' => array('Default', 'new_gameaccount_registration')
        ]);
    }
}
