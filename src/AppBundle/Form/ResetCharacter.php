<?php

namespace AppBundle\Form;

use AppBundle\Form\Model\EditCharacter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ResetCharacter extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

         $builder
            ->add('accountId', HiddenType::class, array(
                'error_bubbling' => true,
            ))
            ->add('characterId', HiddenType::class, array(
                'error_bubbling' => true,
            ))
             ->add('type', HiddenType::class, array(
                 'error_bubbling' => true,
             ))
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EditCharacter::class
        ]);
    }
}
