<?php

namespace AppBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class AdminEditUserAccount
{
    /**
     * @Assert\NotBlank()
     * @Assert\Email(checkMX = true, message = "The email '{{ value }}' is not a valid email.")
     * @Assert\Length(
     *      min = 5,
     *      max = 39,
     *      minMessage = "Your email must be at least {{ limit }} characters long",
     *      maxMessage = "Your email cannot be longer than {{ limit }} characters"
     * )
     * @var string
     */
    private $email;

    /**
     * @Assert\Length(max=4096)
     * @Assert\Regex(
     *  pattern="/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{7,}/",
     *  message="validtrans.password"
     * )
     * @var string
     */
    private $newPassword;

    /**
     * @var date
     * @Assert\Date(message = "validtrans.user.invalid_birthdate")
     */
    private $birthdate = '0000-00-00';

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Country()
     */
    private $country = '';

    /**
     * @var boolean
     *
     * @Assert\Type(
     *     type="bool",
     *     message="The value {{ value }} is not valid."
     * )
     */
    private $allowEmail;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @param string $newPassword
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
    }

    /**
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return boolean
     */
    public function isAllowEmail()
    {
        return $this->allowEmail;
    }

    /**
     * @param boolean $allowEmail
     */
    public function setAllowEmail($allowEmail)
    {
        $this->allowEmail = $allowEmail;
    }
}
