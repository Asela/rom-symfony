<?php

namespace AppBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class EditCharacter
{
    /**
     * @var integer
     *
     * @Assert\Type(
     *     type="numeric",
     *     message="Invalid account data."
     * )
     */
    protected $accountId;

    /**
     * @var integer
     *
     * @Assert\Type(
     *     type="numeric",
     *     message="Invalid character data."
     * )
     */
    protected $characterId;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    protected $type;

    /**
     * @return int
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param int $accountId
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
    }

    /**
     * @return int
     */
    public function getCharacterId()
    {
        return $this->characterId;
    }

    /**
     * @param int $characterId
     */
    public function setCharacterId($characterId)
    {
        $this->characterId = $characterId;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }


}
