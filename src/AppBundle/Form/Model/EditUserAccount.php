<?php

namespace AppBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;

class EditUserAccount
{
    /**
     * @Assert\NotBlank()
     * @Assert\Email(checkMX = true, message = "The email '{{ value }}' is not a valid email.")
     * @Assert\Length(
     *      min = 5,
     *      max = 39,
     *      minMessage = "Your email must be at least {{ limit }} characters long",
     *      maxMessage = "Your email cannot be longer than {{ limit }} characters"
     * )
     * @var string
     */
    private $email;

    /**
     * @SecurityAssert\UserPassword(
     *     message = "Current password is incorrect"
     * )
     * @var string
     */
    private $oldPassword;

    /**
     * @Assert\Length(max=23,maxMessage = "validtrans.password")
     * @Assert\Regex(
     *  pattern="/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{7,23}/",
     *  message="validtrans.password"
     * )
     * @var string
     */
    private $newPassword;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Country()
     */
    private $country = '';
    
    /**
     * @var boolean
     *
     * @Assert\Type(
     *     type="bool",
     *     message="The value {{ value }} is not valid."
     * )
     */
    private $allowEmail;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * @param string $oldPassword
     */
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;
    }

    /**
     * @return string
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @param string $newPassword
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return boolean
     */
    public function isAllowEmail()
    {
        return $this->allowEmail;
    }

    /**
     * @param boolean $allowEmail
     */
    public function setAllowEmail($allowEmail)
    {
        $this->allowEmail = $allowEmail;
    }
}
