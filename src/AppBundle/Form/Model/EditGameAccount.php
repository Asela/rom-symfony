<?php

namespace AppBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;

class EditGameAccount
{
    /**
     * @var integer
     *
     * @Assert\Type(
     *     type="numeric",
     *     message="Invalid account data."
     * )
     */
    private $accountId;

    /**
     *
     * @SecurityAssert\UserPassword(
     *     message = "Current password is incorrect"
     * )
     */
    private $currentPassword;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=23,maxMessage = "validtrans.password")
     * @Assert\Regex(
     *  pattern="/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{7,23}/",
     *  message="validtrans.password"
     * )
     * @var string
     */
    private $newPassword;

    /**
     * @return int
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param int $accountId
     */
    public function setAccountId($accountId)
    {

        $this->accountId = $accountId;
    }

    public function getCurrentPassword()
    {
        return $this->currentPassword;
    }

    public function setCurrentPassword($currentPassword)
    {
        $this->currentPassword = $currentPassword;
    }

    /**
     * @return string
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @param string $newPassword
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
    }
}
