<?php

namespace AppBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class BaseUser
{
    /**
     * @Assert\NotBlank(groups={"email"})
     * @Assert\Email(checkMX = true, message = "The email '{{ value }}' is not a valid email.", groups={"email"})
     * @Assert\Length(
     *      min = 5,
     *      max = 39,
     *      minMessage = "Your email must be at least {{ limit }} characters long",
     *      maxMessage = "Your email cannot be longer than {{ limit }} characters",
     *      groups={"email"}
     * )
     * @var string
     */
    private $email;

    /**
     * @Assert\NotBlank(groups={"new_password"})
     * @Assert\Length(max=23,maxMessage = "validtrans.password")
     * @Assert\Regex(
     *  pattern="/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{7,23}/",
     *  message="validtrans.password",
     *  groups={"new_password"}
     * )
     * @var string
     */
    private $newPassword;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @param string $newPassword
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
    }


}
