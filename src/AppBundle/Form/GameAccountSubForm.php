<?php
namespace AppBundle\Form;
use AppBundle\Entity\GameAccount;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GameAccountSubForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userid', TextType::class, [
                'attr' => array('maxlength' => 23),
                'label' => 'trans.game_account.userid',
                'error_bubbling' => $options['error_bubbling']
            ])
            ->add('sex', ChoiceType::class, [
                'choices' => array(
                    'trans.form.male' => 'M',
                    'trans.form.female' => 'F',
                ),
                'expanded' => true,
                'label' => 'trans.character.gender',
                'error_bubbling' => $options['error_bubbling']
            ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => GameAccount::class
        ));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'error_bubbling' => true
        );
    }
}
