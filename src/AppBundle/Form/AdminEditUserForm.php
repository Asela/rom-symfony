<?php

namespace AppBundle\Form;

use AppBundle\Form\Model\AdminEditUserAccount;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminEditUserForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'trans.form.email',
                'attr' => array('maxlength' => 39)
            ])
            ->add('newPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_name' => 'password',
                'second_name' => 'confirm_password',
                'first_options'  => array('label' => 'trans.form.new_password'),
                'second_options' => array('label' => 'trans.form.confirm_password'),
                'required' => false,
                'invalid_message' => 'validtrans.password_match'
            ])
            ->add('birthdate',BirthdayType::class, [
                'label' => 'trans.form.birthdate',
                'widget' => 'choice',
                'format' => 'yyyy-MMMM-dd',
                'years' => range(date('Y')-7, date('Y')-90),
                'error_bubbling' => $options['error_bubbling']
            ])
            ->add('country', CountryType::class, [
                'label' => 'trans.form.country'
            ])
            ->add('allowEmail', CheckboxType::class, [
                'label' => 'trans.form.email_allowed',
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AdminEditUserAccount::class,
            'validation_groups' => array('Default')
        ]);
    }
}
