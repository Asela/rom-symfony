<?php

namespace AppBundle\Form;

use AppBundle\Form\Model\BaseUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecoverPasswordForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('newPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_name' => 'password',
                'second_name' => 'confirm_password',
                'first_options'  => array('label' => 'trans.form.new_password'),
                'second_options' => array('label' => 'trans.form.confirm_password'),
                'error_bubbling' => true
            ])
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return array(
            'data_class' => BaseUser::class,
            'validation_groups' => array('new_password')
        );
    }

    public function getName()
    {
        return 'password_form';
    }

}
