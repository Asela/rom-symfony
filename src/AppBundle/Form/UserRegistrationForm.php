<?php

namespace AppBundle\Form;
use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;

class UserRegistrationForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'trans.form.email',
                'attr' => array('maxlength' => 39),
                'error_bubbling' => $options['error_bubbling']
            ])
            ->add('plainPassword', RepeatedType::class, [
                'attr' => ['maxlength' => 23],
                'type' => PasswordType::class,
                'first_name' => 'password',
                'second_name' => 'confirm_password',
                'first_options'  => array('label' => 'trans.form.password'),
                'second_options' => array('label' => 'trans.form.confirm_password'),
                'error_bubbling' => $options['error_bubbling'],
                'invalid_message' => 'validtrans.password_match'
            ])
            ->add('gameAccounts', CollectionType::class, [
                'entry_type' => GameAccountSubForm::class,
                'constraints' => array(new Valid()),
                'error_bubbling' => $options['error_bubbling'],
                'entry_options' => ['error_bubbling' => $options['error_bubbling']]
            ])
            ->add('birthdate',BirthdayType::class, [
                'label' => 'trans.form.birthdate',
                'widget' => 'choice',
                'data' => new \DateTime(),
                'format' => 'yyyy-MMMM-dd',
                'years' => range(date('Y')-7, date('Y')-90),
                'error_bubbling' => $options['error_bubbling']
            ])
            ->add('allowEmail', CheckboxType::class, [
                'label' => 'trans.form.email_allowed',
                'data' => true,
                'required' => false,
                'error_bubbling' => $options['error_bubbling']
            ])
            ->add('country', CountryType::class, [
                'label' => 'trans.form.country',
                'placeholder'=>'trans.form.select_placeholder',
                'error_bubbling' => $options['error_bubbling']
            ])
            ->add('referralCode', HiddenType::class, [
                'required' => false,
                'error_bubbling' => $options['error_bubbling']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'validation_groups' => array('Default', 'registration')
        ]);
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'error_bubbling' => true
        );
    }
}
