<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
/**
 * CharRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CharRepository extends EntityRepository
{
    /**
     * @param integer $accountId
     * @return bool
     */
    public function isAccountOnline($accountId) {
        $qb = $this->createQueryBuilder('c')
            ->select('c.accountId')
            ->where('c.online = 1')
            ->andWhere('c.accountId = :id')
            ->setParameter('id', $accountId)
			->setMaxResults(1)
            ->getQuery();
        return $qb->getOneOrNullResult() !== NULL;
    }

    public function countOnlineAccounts()
    {
        $qb = $this->createQueryBuilder('c')
            ->select('count(c.charId)')
            ->where('c.online = 1')
            ->getQuery();
        return $qb->getSingleScalarResult();
    }

    public function searchCharacters($search) {
        $qb = $this->createQueryBuilder('c')
            ->select("c.charId", "c.accountId", "c.masterId", "c.name")
            ->where("c.charId LIKE :search")
            ->orWhere("c.name LIKE :search")
            ->setParameter('search', '%'.$search.'%')
            ->getQuery();
        return $qb->getArrayResult();
    }

    public function hasCharOverLevel($masterId, $level) {
        $qb = $this->createQueryBuilder('c')
            ->select('c.accountId')
            ->where('c.masterId = :masterId')
            ->andWhere('c.baseLevel >= :level')
            ->setParameter('masterId', $masterId)
            ->setParameter('level', $level)
            ->setMaxResults(1)
            ->getQuery();
        return $qb->getOneOrNullResult();
    }
}
