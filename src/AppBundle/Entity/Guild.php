<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Guild
 *
 * @ORM\Table(name="guild", uniqueConstraints={@ORM\UniqueConstraint(name="guild_id", columns={"guild_id"})}, indexes={@ORM\Index(name="char_id", columns={"char_id"})})
 * @ORM\Entity(readOnly=true)
 */
class Guild
{
    /**
     * @var integer
     *
     * @ORM\Column(name="guild_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $guildId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=24, nullable=false)
     */
    private $name = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="char_id", type="integer")
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $charId;

    /**
     * @var string
     *
     * @ORM\Column(name="master", type="string", length=24, nullable=false)
     */
    private $master = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="guild_lv", type="boolean", nullable=false)
     */
    private $guildLv = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="connect_member", type="boolean", nullable=false)
     */
    private $connectMember = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="max_member", type="boolean", nullable=false)
     */
    private $maxMember = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="average_lv", type="smallint", nullable=false)
     */
    private $averageLv = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="exp", type="bigint", nullable=false)
     */
    private $exp = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="next_exp", type="integer", nullable=false)
     */
    private $nextExp = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="skill_point", type="boolean", nullable=false)
     */
    private $skillPoint = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="mes1", type="string", length=60, nullable=false)
     */
    private $mes1 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="mes2", type="string", length=120, nullable=false)
     */
    private $mes2 = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="emblem_len", type="integer", nullable=false)
     */
    private $emblemLen = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="emblem_id", type="integer", nullable=false)
     */
    private $emblemId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="emblem_data", type="blob", length=65535, nullable=true)
     */
    private $emblemData;

    /**
     * @var string
     *
     * @ORM\Column(name="storage_password", type="string", length=32, nullable=false)
     */
    private $storagePassword = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="storage_position", type="integer", nullable=false)
     */
    private $storagePosition = '20';

    /**
     * @var integer
     *
     * @ORM\Column(name="bound_position", type="integer", nullable=false)
     */
    private $boundPosition = '0';

    /**
     * @return int
     */
    public function getGuildId()
    {
        return $this->guildId;
    }

    /**
     * @param int $guildId
     */
    public function setGuildId($guildId)
    {
        $this->guildId = $guildId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getCharId()
    {
        return $this->charId;
    }

    /**
     * @param int $charId
     */
    public function setCharId($charId)
    {
        $this->charId = $charId;
    }

    /**
     * @return string
     */
    public function getMaster()
    {
        return $this->master;
    }

    /**
     * @param string $master
     */
    public function setMaster($master)
    {
        $this->master = $master;
    }

    /**
     * @return boolean
     */
    public function isGuildLv()
    {
        return $this->guildLv;
    }

    /**
     * @param boolean $guildLv
     */
    public function setGuildLv($guildLv)
    {
        $this->guildLv = $guildLv;
    }

    /**
     * @return boolean
     */
    public function isConnectMember()
    {
        return $this->connectMember;
    }

    /**
     * @param boolean $connectMember
     */
    public function setConnectMember($connectMember)
    {
        $this->connectMember = $connectMember;
    }

    /**
     * @return boolean
     */
    public function isMaxMember()
    {
        return $this->maxMember;
    }

    /**
     * @param boolean $maxMember
     */
    public function setMaxMember($maxMember)
    {
        $this->maxMember = $maxMember;
    }

    /**
     * @return int
     */
    public function getAverageLv()
    {
        return $this->averageLv;
    }

    /**
     * @param int $averageLv
     */
    public function setAverageLv($averageLv)
    {
        $this->averageLv = $averageLv;
    }

    /**
     * @return int
     */
    public function getExp()
    {
        return $this->exp;
    }

    /**
     * @param int $exp
     */
    public function setExp($exp)
    {
        $this->exp = $exp;
    }

    /**
     * @return int
     */
    public function getNextExp()
    {
        return $this->nextExp;
    }

    /**
     * @param int $nextExp
     */
    public function setNextExp($nextExp)
    {
        $this->nextExp = $nextExp;
    }

    /**
     * @return boolean
     */
    public function isSkillPoint()
    {
        return $this->skillPoint;
    }

    /**
     * @param boolean $skillPoint
     */
    public function setSkillPoint($skillPoint)
    {
        $this->skillPoint = $skillPoint;
    }

    /**
     * @return string
     */
    public function getMes1()
    {
        return $this->mes1;
    }

    /**
     * @param string $mes1
     */
    public function setMes1($mes1)
    {
        $this->mes1 = $mes1;
    }

    /**
     * @return string
     */
    public function getMes2()
    {
        return $this->mes2;
    }

    /**
     * @param string $mes2
     */
    public function setMes2($mes2)
    {
        $this->mes2 = $mes2;
    }

    /**
     * @return int
     */
    public function getEmblemLen()
    {
        return $this->emblemLen;
    }

    /**
     * @param int $emblemLen
     */
    public function setEmblemLen($emblemLen)
    {
        $this->emblemLen = $emblemLen;
    }

    /**
     * @return int
     */
    public function getEmblemId()
    {
        return $this->emblemId;
    }

    /**
     * @param int $emblemId
     */
    public function setEmblemId($emblemId)
    {
        $this->emblemId = $emblemId;
    }

    /**
     * @return string
     */
    public function getEmblemData()
    {
        return $this->emblemData;
    }

    /**
     * @param string $emblemData
     */
    public function setEmblemData($emblemData)
    {
        $this->emblemData = $emblemData;
    }

    /**
     * @return string
     */
    public function getStoragePassword()
    {
        return $this->storagePassword;
    }

    /**
     * @param string $storagePassword
     */
    public function setStoragePassword($storagePassword)
    {
        $this->storagePassword = $storagePassword;
    }

    /**
     * @return int
     */
    public function getStoragePosition()
    {
        return $this->storagePosition;
    }

    /**
     * @param int $storagePosition
     */
    public function setStoragePosition($storagePosition)
    {
        $this->storagePosition = $storagePosition;
    }

    /**
     * @return int
     */
    public function getBoundPosition()
    {
        return $this->boundPosition;
    }

    /**
     * @param int $boundPosition
     */
    public function setBoundPosition($boundPosition)
    {
        $this->boundPosition = $boundPosition;
    }


}

