<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserPoints
 *
 * @ORM\Table(name="master_account_points", indexes={@ORM\Index(name="master_id", columns={"master_id"})})
 * @ORM\Entity
 */
class UserPoints
{
    /**
     * @var integer
     *
     * @ORM\Column(name="master_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $masterId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="vote_points", type="integer", nullable=false)
     */
    private $votePoints = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="referral_points", type="integer", nullable=false)
     */
    private $referralPoints = 0;

    /**
     * @var User
     * @ORM\OneToOne(targetEntity="\AppBundle\Entity\User", inversedBy="userPoints")
     * @ORM\JoinColumn(name="master_id", referencedColumnName="master_id")
     */
    private $user;

    /**
     * UserPoints constructor.
     */
    public function __construct()
    {
        return $this;
    }


    /**
     * @return int
     */
    public function getMasterId()
    {
        return $this->masterId;
    }

    /**
     * @param int $masterId
     */
    public function setMasterId($masterId)
    {
        $this->masterId = $masterId;
    }

    /**
     * @return int
     */
    public function getVotePoints()
    {
        return $this->votePoints;
    }

    /**
     * @param int $votePoints
     */
    public function setVotePoints($votePoints)
    {
        $this->votePoints = $votePoints;
    }

    /**
     * @param int $votePoints
     */
    public function updateVotePoints($votePoints)
    {
        $this->votePoints += $votePoints;
    }

    /**
     * @return int
     */
    public function getReferralPoints()
    {
        return $this->referralPoints;
    }

    /**
     * @param int $referralPoints
     */
    public function setReferralPoints($referralPoints)
    {
        $this->referralPoints = $referralPoints;
    }

    /**
     * @param int $referralPoints
     */
    public function updateReferralPoints($referralPoints)
    {
        $this->referralPoints += $referralPoints;
    }


    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

}
