<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WebActionLog
 *
 * @ORM\Table(name="web_action_log", indexes={@ORM\Index(name="src_master_id", columns={"src_master_id"}), @ORM\Index(name="target_master_id", columns={"target_master_id"}), @ORM\Index(name="target_account_id", columns={"target_account_id"})})
 * @ORM\Entity
 */
class WebActionLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="src_master_id", type="integer", nullable=false)
     */
    private $srcMasterId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="target_master_id", type="integer", nullable=false)
     */
    private $targetMasterId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="target_account_id", type="integer", nullable=false)
     */
    private $targetAccountId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=false)
     */
    private $type = '';

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="string", length=255, nullable=false)
     */
    private $reason = '';

    /**
     * Punishmentlog constructor.
     * @param int $srcMasterId
     * @param int $targetMasterId
     * @param int $targetAccountId
     * @param string $type
     * @param string $reason
     */
    public function __construct($srcMasterId, $targetMasterId, $targetAccountId, $type, $reason)
    {
        $this->date  = new \DateTime();
        $this->srcMasterId = $srcMasterId;
        $this->targetMasterId = $targetMasterId;
        $this->targetAccountId = $targetAccountId;
        $this->type = $type;
        $this->reason = $reason;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setBanDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getSrcMasterId()
    {
        return $this->srcMasterId;
    }

    /**
     * @param int $srcMasterId
     */
    public function setSrcMasterId($srcMasterId)
    {
        $this->srcMasterId = $srcMasterId;
    }

    /**
     * @return int
     */
    public function getTargetMasterId()
    {
        return $this->targetMasterId;
    }

    /**
     * @param int $targetMasterId
     */
    public function setTargetMasterId($targetMasterId)
    {
        $this->targetMasterId = $targetMasterId;
    }

    /**
     * @return int
     */
    public function getTargetAccountId()
    {
        return $this->targetAccountId;
    }

    /**
     * @param int $targetAccountId
     */
    public function setTargetAccountId($targetAccountId)
    {
        $this->targetAccountId = $targetAccountId;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }
}

