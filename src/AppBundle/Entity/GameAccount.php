<?php

namespace AppBundle\Entity;

use AppBundle\Controller\ENUM\Gamestate;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Login
 *
 * Fields User interacts with directly: userPass, sex
 *
 * @ORM\Table(name="login", indexes={@ORM\Index(name="account_id", columns={"account_id"}), @ORM\Index(name="name", columns={"userid"}), @ORM\Index(name="master_id", columns={"master_id"}), @ORM\Index(name="email", columns={"email"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\GameAccountRepository")
 * @UniqueEntity(fields="userid", message="validtrans.gameaccount.already_exists")
 */
class GameAccount implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="account_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $accountId;

    /**
     * @var string
     * @Assert\Length(min=3, max=23, minMessage="validtrans.gameaccount.too_short", maxMessage="validtrans.gameaccount.too_long")
     * @Assert\Regex(
     *     pattern="/^[a-zA-z0-9]+$/",
     *     match=true,
     *     message="validtrans.gameaccount.invalid_userid"
     * )
     * @ORM\Column(name="userid", type="string", length=23, nullable=false, unique=true)
     */
    private $userid = '';

    /**
     * @var string
     *
     * @ORM\Column(name="user_pass", type="string", length=32, nullable=false)
     */
    private $userPass = '';

    /**
     * A non-persisted field that's used to create the encoded password.
     * @Assert\NotBlank(groups={"new_gameaccount_registration"})
     * @Assert\Length(max=23,maxMessage = "validtrans.password")
     * @Assert\Regex(
     *  pattern="/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{7,23}/",
     *  message="validtrans.password"
     * )
     * @var string
     */
    private $plainPassword;

    /**
     * @var string
     * @Assert\NotBlank(message = "validtrans.gameaccount.gender_blank")
     * @Assert\Choice(choices = {"M","F"}, message = "validtrans.gameaccount.gender_choice")
     * @ORM\Column(name="sex", type="string", nullable=false)
     */
    private $sex = 'M';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=39, nullable=false)
     */
    private $email = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="group_id", type="integer", nullable=false)
     */
    private $groupId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="integer", nullable=false)
     */
    private $state = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="unban_time", type="integer", nullable=false)
     */
    private $unbanTime = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="logincount", type="integer", nullable=false)
     */
    private $logincount = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="lastlogin", type="string", nullable=false)
     */
    private $lastlogin = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="last_ip", type="string", length=100, nullable=false)
     */
    private $lastIp = '';

    /**
     * @var date
     *
     * @ORM\Column(name="birthdate", type="date", nullable=false)
     */
    private $birthdate = '0000-00-00';

    /**
     * @var string
     *
     * @ORM\Column(name="last_mac", type="string", length=18, nullable=false)
     */
    private $lastMac = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="master_id", type="integer")
     */
    private $masterId = '0';

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="gameAccounts")
     * @ORM\JoinColumn(name="master_id", referencedColumnName="master_id")
     */
    private $masterAccount;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\Character", mappedBy="gameAccount", indexBy="charId",  cascade={"persist"}, fetch="EAGER")
     */
    private $characters;

    /**
     * @var Zeny
     * @ORM\OneToOne(targetEntity="\AppBundle\Entity\Zeny", mappedBy="gameAccount", cascade={"persist"})
     */
    private $zeny;

    /**
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\AccRegNum", mappedBy="gameAccount", indexBy="key", cascade={"persist"})
     */
    private $accRegNum;

    public function __construct()
    {
        $this->characters = new ArrayCollection();
        $this->accRegNum = new ArrayCollection();
        $this->addAccRegNum(new AccRegNum('#CASHPOINTS', 0));
        $this->setZeny(new Zeny());
    }

    /**
     * @return int
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param int $accountId
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
    }

    /**
     * @return string
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param string $userid
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;
    }

    /**
     * @return string
     */
    public function getUserPass()
    {
        return $this->userPass;
    }

    /**
     * @param string $userPass
     */
    public function setUserPass($userPass)
    {
        $this->userPass = $userPass;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
        // forces the object to look "dirty" to Doctrine. Avoids
        // Doctrine *not* saving this entity, if only plainPassword changes
        $this->userPass = null;
    }

    /**
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param string $sex
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param int $groupId
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param int $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return int
     */
    public function getUnbanTime()
    {
        return $this->unbanTime;
    }

    /**
     * @param int $unbanTime
     */
    public function setUnbanTime($unbanTime)
    {
        $this->unbanTime = $unbanTime;
    }

    /**
     * @return int
     */
    public function getLogincount()
    {
        return $this->logincount;
    }

    /**
     * @param int $logincount
     */
    public function setLogincount($logincount)
    {
        $this->logincount = $logincount;
    }

    /**
     * @return string
     */
    public function getLastlogin()
    {
        return $this->lastlogin;
    }

    /**
     * @param \DateTime $lastlogin
     */
    public function setLastlogin($lastlogin)
    {
        $this->lastlogin = $lastlogin;
    }

    /**
     * @return string
     */
    public function getLastIp()
    {
        return $this->lastIp;
    }

    /**
     * @param string $lastIp
     */
    public function setLastIp($lastIp)
    {
        $this->lastIp = $lastIp;
    }

    /**
     * @return date
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * @param date $birthdate
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
    }

    /**
     * @return string
     */
    public function getLastMac()
    {
        return $this->lastMac;
    }

    /**
     * @param string $lastMac
     */
    public function setLastMac($lastMac)
    {
        $this->lastMac = $lastMac;
    }

    /**
     * @return int
     */
    public function getMasterId()
    {
        return $this->masterAccount->getMasterId();
    }

    /**
     * @return mixed
     */
    public function getMasterAccount()
    {
        return $this->masterAccount;
    }

    /**
     * @param mixed $masterAccount
     */
    public function setMasterAccount($masterAccount)
    {
        $this->masterAccount = $masterAccount;
        /** @var Character $character */
        foreach($this->characters as $character) {
            $character->setMasterAccount($masterAccount);
        }
    }

    /**
     * @return ArrayCollection
     */
    public function getCharacters()
    {
        return $this->characters;
    }

    /**
     * @param ArrayCollection $characters
     */
    public function setCharacters($characters)
    {
        $this->characters = $characters;
    }

    /**
     * @param Character $character
     */
    public function addCharacter(Character $character)
    {
        $character->setGameAccount($this);
        $this->characters->add($character);
    }

    /**
     * @param integer $characterId
     * @return Character
     */
    public function getCharacter($characterId) {
        $character = $this->characters->get($characterId);
        return $character;
    }

    /**
     * @return Zeny
     */
    public function getZeny()
    {
        return $this->zeny;
    }

    /**
     * @param Zeny $zeny
     */
    public function setZeny($zeny)
    {
        $zeny->setGameAccount($this);
        $this->zeny = $zeny;
    }

    /**
     * @return ArrayCollection
     */
    public function getAccRegNum()
    {
        return $this->accRegNum;
    }

    /**
     * @param ArrayCollection $accRegNum
     */
    public function setAccRegNum($accRegNum)
    {
        $this->accRegNum = $accRegNum;
    }

    /**
     * @param AccRegNum $character
     */
    public function addAccRegNum(AccRegNum $accRegNum)
    {
        $accRegNum->setGameAccount($this);
        $this->accRegNum->set($accRegNum->getKey(), $accRegNum);
    }

    /**
     * @return string
     */
    public function getZenyFormatted()
    {
        $zeny = $this->zeny;
        return ($zeny === NULL) ? "0" : number_format($this->zeny->getAmount());
    }

    /**
     * @return string
     */
    public function getBankFormatted()
    {
        /** @var AccRegNum $bank */
        $bank = $this->getAccRegNum()->get('#BANKVAULT');
        return ($bank === NULL) ? "0" : number_format($bank->getValue());
    }

    /**
     * @return AccRegNum|null
     */
    public function getPoints() {
        $points = $this->getAccRegNum()->get('#CASHPOINTS');
        return $points;
    }

    /**
     * @return string
     */
    public function getPointsFormatted()
    {
        /** @var AccRegNum $points */
        $points = $this->getAccRegNum()->get('#CASHPOINTS');
        return ($points === NULL) ? "0" : number_format($points->getValue());
    }

    public function jsonSerialize($full = false)
    {
        $chars = array();
        /** @var Character $character */
        foreach($this->characters as $character) {
            $chars[] = $character->jsonSerialize($full);
        }

        $ret = ['account_id' => $this->accountId,
                'userid' => $this->userid,
                'cashpoints' => $this->getPointsFormatted(),
                'zeny' => $this->getZenyFormatted(),
                'sex' => $this->sex,
                'last_login' => $this->lastlogin,
                'last_ip' => $this->lastIp,
                'chars' => $chars
                ];

        if ($full) {
            $banned = false;
            $locked = false;
            switch ($this->state) {
                case Gamestate::OK:
                case Gamestate::CHANGE_PWD:
                case Gamestate::EMAIL_UNCONFIRMED:
                    break;
                case Gamestate::SELF_LOCK:
                    $locked = true;
                    break;
                default:
                    $banned = true;
                    break;
            }
            $ret['banned'] = $banned;
            $ret['locked'] = $locked;
        }
        return $ret;
    }
}
