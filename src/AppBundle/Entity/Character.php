<?php

namespace AppBundle\Entity;

use AppBundle\Controller\ENUM\ClassParser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Char
 *
 * Fields User interacts with directly: name
 *
 * @ORM\Table(name="`char`", indexes={@ORM\Index(name="char_id", columns={"char_id"}),@ORM\Index(name="account_id", columns={"account_id"}),@ORM\Index(name="name", columns={"name"}), @ORM\Index(name="party_id", columns={"party_id"}), @ORM\Index(name="guild_id", columns={"guild_id"}), @ORM\Index(name="online", columns={"online"}), @ORM\Index(name="master_id", columns={"master_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CharRepository")
 * @UniqueEntity(fields="name", message="validtrans.character.already_exists")
 */
class Character implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="char_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $charId;

    /**
     * @var integer
     *
     * @ORM\Column(name="account_id", type="integer", nullable=false)
     */
    private $accountId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="char_num", type="smallint", nullable=false)
     */
    private $charNum = '0';

    /**
     * @var string
     * @Assert\Length(min=3, max=23,minMessage="validtrans.character.too_short", maxMessage="validtrans.character.too_long")
     * @Assert\Regex(
     *     pattern="/^[a-zA-z0-9 ]+$/",
     *     match=true,
     *     message="validtrans.character.invalid_name"
     * )
     * @Assert\Regex(
     *     pattern="/GM |DEV |SGM|ADMIN|SERVER/i",
     *     match=false,
     *     message="validtrans.character.blocked_name"
     * )
     * @ORM\Column(name="`name`", type="string", length=30, nullable=false, unique=true)
     */
    private $name = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="class", type="smallint", nullable=false)
     */
    private $class = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="base_level", type="smallint", nullable=false)
     */
    private $baseLevel = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="job_level", type="smallint", nullable=false)
     */
    private $jobLevel = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="guild_id", type="integer", nullable=false)
     */
    private $guildId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="hair", type="smallint", nullable=false)
     */
    private $hair = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="hair_color", type="smallint", nullable=false)
     */
    private $hairColor = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="clothes_color", type="smallint", nullable=false)
     */
    private $clothesColor = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="body", type="smallint", nullable=false)
     */
    private $body = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="last_map", type="string", length=11, nullable=false)
     */
    private $lastMap = 'rom_air';

    /**
     * @var integer
     *
     * @ORM\Column(name="last_x", type="smallint", nullable=false)
     */
    private $lastX = '236';

    /**
     * @var integer
     *
     * @ORM\Column(name="last_y", type="smallint", nullable=false)
     */
    private $lastY = '53';

    /**
     * @var string
     *
     * @ORM\Column(name="save_map", type="string", length=11, nullable=false)
     */
    private $saveMap = 'rom_air';

    /**
     * @var integer
     *
     * @ORM\Column(name="save_x", type="smallint", nullable=false)
     */
    private $saveX = '236';

    /**
     * @var integer
     *
     * @ORM\Column(name="save_y", type="smallint", nullable=false)
     */
    private $saveY = '53';

    /**
     * @var boolean
     *
     * @ORM\Column(name="online", type="boolean", nullable=false)
     */
    private $online = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="unban_time", type="integer", nullable=false)
     */
    private $unbanTime = '0';

    /**
     * @var string
     * @ORM\Column(name="sex", type="string", nullable=false)
     */
    private $sex = 'U';

    /**
     * @var integer
     *
     * @ORM\Column(name="master_id", type="integer", nullable=false)
     */
    private $masterId = '0';

    /**
     * @var GameAccount
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\GameAccount", inversedBy="characters")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="account_id")
     */
    private $gameAccount;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="master_id", referencedColumnName="master_id")
     */
    private $masterAccount;

    /**
     * @return int
     */
    public function getCharId()
    {
        return $this->charId;
    }

    /**
     * @param int $charId
     */
    public function setCharId($charId)
    {
        $this->charId = $charId;
    }

    /**
     * @return int
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param int $accountId
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
    }

    /**
     * @return string
     */
    public function getCharNum()
    {
        return $this->charNum;
    }

    /**
     * @param string $charNum
     */
    public function setCharNum($charNum)
    {
        $this->charNum = $charNum;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param int $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * @return int
     */
    public function getBaseLevel()
    {
        return $this->baseLevel;
    }

    /**
     * @param int $baseLevel
     */
    public function setBaseLevel($baseLevel)
    {
        $this->baseLevel = $baseLevel;
    }

    /**
     * @return int
     */
    public function getJobLevel()
    {
        return $this->jobLevel;
    }

    /**
     * @param int $jobLevel
     */
    public function setJobLevel($jobLevel)
    {
        $this->jobLevel = $jobLevel;
    }

    /**
     * @return int
     */
    public function getGuildId()
    {
        return $this->guildId;
    }

    /**
     * @param int $guildId
     */
    public function setGuildId($guildId)
    {
        $this->guildId = $guildId;
    }

    /**
     * @return int
     */
    public function getHair()
    {
        return $this->hair;
    }

    /**
     * @param int $hair
     */
    public function setHair($hair)
    {
        $this->hair = $hair;
    }

    /**
     * @return int
     */
    public function getHairColor()
    {
        return $this->hairColor;
    }

    /**
     * @param int $hairColor
     */
    public function setHairColor($hairColor)
    {
        $this->hairColor = $hairColor;
    }

    /**
     * @return int
     */
    public function getClothesColor()
    {
        return $this->clothesColor;
    }

    /**
     * @param int $clothesColor
     */
    public function setClothesColor($clothesColor)
    {
        $this->clothesColor = $clothesColor;
    }

    /**
     * @return int
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param int $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getLastMap()
    {
        return $this->lastMap;
    }

    /**
     * @param string $lastMap
     */
    public function setLastMap($lastMap)
    {
        $this->lastMap = $lastMap;
    }

    /**
     * @return int
     */
    public function getLastX()
    {
        return $this->lastX;
    }

    /**
     * @param int $lastX
     */
    public function setLastX($lastX)
    {
        $this->lastX = $lastX;
    }

    /**
     * @return int
     */
    public function getLastY()
    {
        return $this->lastY;
    }

    /**
     * @param int $lastY
     */
    public function setLastY($lastY)
    {
        $this->lastY = $lastY;
    }

    /**
     * @return string
     */
    public function getSaveMap()
    {
        return $this->saveMap;
    }

    /**
     * @param string $saveMap
     */
    public function setSaveMap($saveMap)
    {
        $this->saveMap = $saveMap;
    }

    /**
     * @return int
     */
    public function getSaveX()
    {
        return $this->saveX;
    }

    /**
     * @param int $saveX
     */
    public function setSaveX($saveX)
    {
        $this->saveX = $saveX;
    }

    /**
     * @return int
     */
    public function getSaveY()
    {
        return $this->saveY;
    }

    /**
     * @param int $saveY
     */
    public function setSaveY($saveY)
    {
        $this->saveY = $saveY;
    }

    /**
     * @return boolean
     */
    public function isOnline()
    {
        return $this->online;
    }

    /**
     * @param boolean $online
     */
    public function setOnline($online)
    {
        $this->online = $online;
    }

    /**
     * @return int
     */
    public function getUnbanTime()
    {
        return $this->unbanTime;
    }

    /**
     * @param int $unbanTime
     */
    public function setUnbanTime($unbanTime)
    {
        $this->unbanTime = $unbanTime;
    }

    /**
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param string $sex
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    }

    /**
     * @return int
     */
    public function getMasterId()
    {
        return $this->masterId;
    }

    /**
     * @param int $masterId
     */
    public function setMasterId($masterId)
    {
        $this->masterId = $masterId;
    }

    /**
     * @return mixed
     */
    public function getGameAccount()
    {
        return $this->gameAccount;
    }

    /**
     * @param mixed $gameAccount
     */
    public function setGameAccount($gameAccount)
    {
        $this->gameAccount = $gameAccount;
    }

    /**
     * @return mixed
     */
    public function getMasterAccount()
    {
        return $this->masterAccount;
    }

    /**
     * @param mixed $masterAccount
     */
    public function setMasterAccount($masterAccount)
    {
        $this->masterAccount = $masterAccount;
    }

    /**
     * @return Guild
     */
    public function getGuild()
    {
        // rAthena defaults no guild to 0, which doctrine isn't a fan off
        if ($this->guildId == 0) return new Guild();
        return $this->guild;
    }

    /**
     * @param Guild $guild
     */
    public function setGuild($guild)
    {
        $this->guild = $guild;
    }

    /**
     * Reset character style to default
     */
    public function resetStyle()
    {
        $this->hair = 1;
        $this->hairColor = 0;
        $this->clothesColor = 0;
        $this->body = 0;
    }

    /**
     * Reset character position to save point
     */
    public function resetPosition()
    {
        $this->lastX = $this->saveX;
        $this->lastY = $this->saveY;
        $this->lastMap = $this->saveMap;
    }

    public function jsonSerialize($full = false)
    {
        $ret = [
            'char_id' => $this->charId,
            'name' => $this->name,
            'class' => ClassParser::getClassName($this->class),
            'sex' => $this->sex,
            'base_level' => $this->baseLevel,
            'job_level' => $this->jobLevel,
            'online' => $this->online,
            'location' => ($this->lastMap . " " . $this->lastX . ", " . $this->lastY),
            'savepoint' => ($this->saveMap . " " . $this->saveX . ", " . $this->saveY),
            'guild' => $this->guildId
        ];

        return $ret;
    }
}
