<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccRegNum
 *
 * @ORM\Table(name="acc_reg_num", indexes={@ORM\Index(name="account_id", columns={"account_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AccRegNumRepository")
 */
class AccRegNum
{
    /**
     * @var integer
     *
     * @ORM\Column(name="account_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $accountId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="`key`", type="string", length=32)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $key = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="`index`", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $index = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="value", type="integer", nullable=false)
     */
    private $value = '0';

    /**
     * @var GameAccount
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\GameAccount", inversedBy="accRegNum")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="account_id")
     */
    private $gameAccount;


    /**
     * AccRegNum constructor.
     * @param $key string
     * @param $value integer
     */
    public function __construct($key, $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param int $accountId
     * @return AccRegNum
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     * @return AccRegNum
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return int
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param int $index
     * @return AccRegNum
     */
    public function setIndex($index)
    {
        $this->index = $index;
        return $this;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     * @return AccRegNum
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @param int $value
     */
    public function updateValue($value) {
        $this->value += $value;
    }

    /**
     * @return mixed
     */
    public function getGameAccount()
    {
        return $this->gameAccount;
    }

    /**
     * @param mixed $gameAccount
     */
    public function setGameAccount($gameAccount)
    {
        $this->gameAccount = $gameAccount;
    }

}
