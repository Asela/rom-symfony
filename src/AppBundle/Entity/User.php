<?php

namespace AppBundle\Entity;

use AppBundle\Controller\ENUM\Gamestate;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * User
 *
 * Fields User interacts with directly: email, password
 *
 * @ORM\Table(name="master_account", indexes={@ORM\Index(name="master_id", columns={"master_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 * @UniqueEntity(fields={"email"}, message="validtrans.user.already_exists")
 */
class User implements UserInterface
{
      /**
     * @var integer
     *
     * @ORM\Column(name="master_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $masterId;

    /**
     * @Assert\NotBlank()
     * @Assert\Email(checkMX = true, message = "validtrans.user.invalid_email")
     * @Assert\Length(
     *      min = 5,
     *      max = 39,
     *      minMessage = "validtrans.user.email_short",
     *      maxMessage = "validtrans.user.email_long"
     * )
     * @ORM\Column(name="email", type="string", length=39, nullable=false, unique=true)
     */
    private $email = '';
    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=60, nullable=false)
     */
    private $password = '';
    /**
     * @ORM\Column(name="roles", type="json_array", nullable=false)
     */
    private $roles = [];
    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="integer", nullable=false)
     */
    private $state = '0';
    /**
     * @var date
     * @Assert\Date(message = "validtrans.user.invalid_birthdate")
     * @ORM\Column(name="birthdate", type="date", nullable=false)
     */
    private $birthdate = '0000-00-00';
    /**
     * @var boolean
     *
     * @ORM\Column(name="allow_email", type="boolean")
     */
    private $allowEmail = '0';
    /**
     * @var decimal
     *
     * @ORM\Column(name="money_spent", type="decimal", precision=8, scale=2)
     */
    private $moneySpent = 0;
    /**
     * @var string
     *
     * @ORM\Column(name="referral_code", type="string", nullable=false)
     */
    private $referralCode = '';
    /**
     * @var string
     * @Assert\Country()
     * @ORM\Column(name="country", type="string", length=2, nullable=false)
     */
    private $country = '';

    /**
     * @var datetime
     * @ORM\Column(name="registration_time", type="datetime", nullable=false)
     */
    private $registrationTime = '0000-00-00 00:00:00';
    /**
     * @var string
     * @ORM\Column(name="registration_ip", type="string", nullable=false)
     */
    private $registrationIp = '';
    /**
     * @var datetime
     * @ORM\Column(name="last_login", type="datetime", nullable=false)
     */
    private $lastLogin = '0000-00-00 00:00:00';
    /**
     * @var string
     * @ORM\Column(name="last_ip", type="string", nullable=false)
     */
    private $lastIp = '';

    /**
     * A non-persisted field that's used to create the encoded password.
     * @Assert\NotBlank(groups={"Registration"})
     * @Assert\Length(max=23,maxMessage = "validtrans.password")
     * @Assert\Regex(
     *  pattern="/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{7,23}/",
     *  message="validtrans.password"
     * )
     * @var string
     */
    private $plainPassword;

    /**
     * @var ArrayCollection
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\GameAccount", mappedBy="masterAccount", indexBy="accountId", cascade={"persist"})
     */
    private $gameAccounts;

    /**
     * @var UserPoints
     * @ORM\OneToOne(targetEntity="\AppBundle\Entity\UserPoints", mappedBy="user", cascade={"persist"})
     */
    private $userPoints;


    public function __construct()
    {
        $this->gameAccounts = new ArrayCollection();
        $this->setUserPoints(new UserPoints(0,0));
    }

    // needed by the security system
    public function getUsername()
    {
        return $this->email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getRoles()
    {
        $roles = $this->roles;
        if (!in_array('ROLE_USER', $roles)) {
            $roles[] = 'ROLE_USER';
        }
        return $roles;
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getSalt()
    {
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
        // forces the object to look "dirty" to Doctrine. Avoids
        // Doctrine *not* saving this entity, if only plainPassword changes
        $this->password = null;
    }

    /**
     * @return int
     */
    public function getMasterId()
    {
        return $this->masterId;
    }

    /**
     * @param int $masterId
     */
    public function setMasterId($masterId)
    {
        $this->masterId = $masterId;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param int $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return date
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * @param date $birthdate
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
    }

    /**
     * @return boolean
     */
    public function isAllowEmail()
    {
        return $this->allowEmail;
    }

    /**
     * @param boolean $allowEmail
     */
    public function setAllowEmail($allowEmail)
    {
        $this->allowEmail = $allowEmail;
    }

    /**
     * @return decimal
     */
    public function getMoneySpent()
    {
        return $this->moneySpent;
    }

    /**
     * @param decimal $moneySpent
     */
    public function setMoneySpent($moneySpent)
    {
        $this->moneySpent = $moneySpent;
    }

    /**
     * @param decimal $moneySpent
     */
    public function updateMoneySpent($moneySpent)
    {
        $this->moneySpent += $moneySpent;
    }

    /**
     * @return string
     */
    public function getReferralCode()
    {
        return $this->referralCode;
    }

    /**
     * @param string $referralCode
     */
    public function setReferralCode($referralCode)
    {
        $this->referralCode = $referralCode;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return datetime
     */
    public function getRegistrationTime()
    {
        return $this->registrationTime;
    }

    /**
     * @param datetime $registrationTime
     * @ORM\PrePersist
     */
    public function setRegistrationTime()
    {
        $this->registrationTime = new \DateTime();
    }

    /**
     * @return string
     */
    public function getRegistrationIp()
    {
        return $this->registrationIp;
    }

    /**
     * @param string $registrationIp
     */
    public function setRegistrationIp($registrationIp)
    {
        $this->registrationIp = $registrationIp;
    }

    /**
     * @return datetime
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * @param datetime $lastLogin
     * @ORM\PrePersist
     */
    public function setLastLogin()
    {
        $this->lastLogin = new \DateTime();
    }

    /**
     * @return string
     */
    public function getLastIp()
    {
        return $this->lastIp;
    }

    /**
     * @param string $lastIp
     */
    public function setLastIp($lastIp)
    {
        $this->lastIp = $lastIp;
    }

    /**
     * @return ArrayCollection
     */
    public function getGameAccounts()
    {
        return $this->gameAccounts;
    }

    /**
     * @param ArrayCollection $gameAccounts
     */
    public function setGameAccounts($gameAccounts)
    {
        $this->gameAccounts = $gameAccounts;
    }

    /**
     * @param GameAccount $gameAccount
     */
    public function addGameAccount(GameAccount $gameAccount)
    {
        $gameAccount->setMasterAccount($this);
        $gameAccount->setEmail($this->email);
        $gameAccount->setBirthdate($this->birthdate);
        $this->gameAccounts->add($gameAccount);
    }

    /**
     * @param integer $accountId
     * @return GameAccount
     */
    public function getGameAccount($accountId) {
        $gameAccount = $this->gameAccounts->get($accountId);
        return $gameAccount;
    }

    /**
     * @return int
     */
    public function getFirstGameAccountId() {
        /** @var GameAccount $gameAccount */
        $gameAccount = $this->gameAccounts->first();
        return ($gameAccount === NULL ? 0 : $gameAccount->getAccountId());
    }

    /**
     * @return UserPoints
     */
    public function getUserPoints()
    {
        return $this->userPoints;
    }

    /**
     * @param UserPoints $userPoints
     */
    public function setUserPoints($userPoints)
    {
        $userPoints->setUser($this);
        $this->userPoints = $userPoints;
    }

    /**
     * @param integer $accountId
     * @param integer $characterId
     * @return Character
     */
    public function getCharacter($accountId, $characterId) {
        $gameAccount = $this->gameAccounts->get($accountId);
        return ($gameAccount === NULL ? NULL : $gameAccount->getCharacter($characterId));
    }


    /**
     * @return array
     */
    public function jsonGameAccounts($full = false) {
        $gameAccounts = array();
        /** @var GameAccount $gameAccount */
        foreach($this->gameAccounts as $gameAccount) {
            $gameAccounts[] = $gameAccount->jsonSerialize($full);
        }

        return $gameAccounts;
    }

    /**
     * Sets User state and child GameAccounts states to OK
     * if they are in the EMAIL_UNCONFIRMED state.
     * This prevents email verification from unbanning a user.
     */
    public function confirmEmail() {
        if ($this->state == Gamestate::EMAIL_UNCONFIRMED) $this->state = Gamestate::OK;
        /** @var GameAccount $gameAccount */
        foreach($this->gameAccounts as $gameAccount) {
            if ($gameAccount->getState() == Gamestate::EMAIL_UNCONFIRMED)
              $gameAccount->setState(Gamestate::OK);
        }
    }

    /**
     * @return null|string
     */
    public function getCountryName() {
        return Intl::getRegionBundle()->getCountryName($this->getCountry());
    }

    /**
     * @return string
     */
    public function getVotePointsFormatted()
    {
        $userPoints = $this->userPoints;
        return ($userPoints === NULL) ? "0" : number_format($this->userPoints->getVotePoints());
    }

    /**
     * @return string
     */
    public function getReferralPointsFormatted()
    {
        $userPoints = $this->userPoints;
        return ($userPoints === NULL) ? "0" : number_format($this->userPoints->getReferralPoints());
    }
}
