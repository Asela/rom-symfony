<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccRegNum
 *
 * @ORM\Table(name="pending_cashpoints", indexes={@ORM\Index(name="master_id", columns={"master_id"}),@ORM\Index(name="account_id", columns={"account_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PendingCashpointsRepository",readOnly=true)
 */
class PendingCashpoints
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="master_id", type="integer", nullable=true)
     */
    private $masterId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="account_id", type="integer", nullable=true)
     */
    private $accountId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="amount", type="integer", nullable=false)
     */
    private $amount = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="received", type="integer", nullable=false)
     */
    private $received = '0';

    /**
     * PendingCashpoints constructor.
     * @param int $masterId
     * @param int $accountId
     * @param int $amount
     * @param bool $received
     */
    public function __construct($masterId, $accountId, $amount, $received)
    {
        $this->masterId = $masterId;
        $this->accountId = $accountId;
        $this->amount = $amount;
        $this->received = $received;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getMasterId()
    {
        return $this->masterId;
    }

    /**
     * @param int $masterId
     */
    public function setMasterId($masterId)
    {
        $this->masterId = $masterId;
    }

    /**
     * @return int
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param int $accountId
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return boolean
     */
    public function isReceived()
    {
        return $this->received;
    }

    /**
     * @param boolean $received
     */
    public function setReceived($received)
    {
        $this->received = $received;
    }
}
