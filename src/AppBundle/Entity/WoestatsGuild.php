<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * WoestatsGuild
 *
 * @ORM\Table(name="woestats_guild")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\WoestatsGuildRepository", readOnly=true)
 */
class WoestatsGuild
{
    /**
     * @var integer
     *
     * @ORM\Column(name="guild_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @Groups({"basic"})
     */
    private $guildId = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="agit_flag", type="boolean")
     * @ORM\GeneratedValue(strategy="NONE")
     * @Groups({"basic"})
     */
    private $agitFlag = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=24, nullable=false)
     * @Groups({"basic"})
     */
    private $name = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="kills", type="smallint", nullable=false)
     * @Groups({"basic"})
     */
    private $kills = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="deaths", type="smallint", nullable=false)
     * @Groups({"basic"})
     */
    private $deaths = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="emperiums", type="smallint", nullable=false)
     * @Groups({"basic"})
     */
    private $emperiums = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="castles", type="string", length=100, nullable=false)
     *
     */
    private $castles = '';

    /**
     * @var string
     *
     * @ORM\Column(name="emblem_data", type="binary", length=65535, nullable=true)
     * @Groups({"basic"})
     */
    private $emblemData;

    /**
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\WoestatsChar", mappedBy="woeguild")
     */
    private $woechars;

    /**
     * @var integer
     * @Groups({"basic"})
     */
    private $holdTime = '0';

    /**
     * @var array
     * @Groups({"basic"})
     */
    private $castleList;

    /**
     * @return int
     */
    public function getGuildId()
    {
        return $this->guildId;
    }

    /**
     * @param int $guildId
     */
    public function setGuildId($guildId)
    {
        $this->guildId = $guildId;
    }

    /**
     * @return boolean
     */
    public function isAgitFlag()
    {
        return $this->agitFlag;
    }

    /**
     * @param boolean $agitFlag
     */
    public function setAgitFlag($agitFlag)
    {
        $this->agitFlag = $agitFlag;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getKills()
    {
        return $this->kills;
    }

    /**
     * @param int $kills
     */
    public function setKills($kills)
    {
        $this->kills = $kills;
    }

    /**
     * @return int
     */
    public function getDeaths()
    {
        return $this->deaths;
    }

    /**
     * @param int $deaths
     */
    public function setDeaths($deaths)
    {
        $this->deaths = $deaths;
    }

    /**
     * @return int
     */
    public function getEmperiums()
    {
        return $this->emperiums;
    }

    /**
     * @param int $emperiums
     */
    public function setEmperiums($emperiums)
    {
        $this->emperiums = $emperiums;
    }

    /**
     * @return string
     */
    public function getCastles()
    {
        return $this->castles;
    }

    /**
     * @param string $castles
     */
    public function setCastles($castles)
    {
        $this->castles = $castles;
    }

    /**
     * @return string
     */
    public function getEmblemData()
    {
        return $this->emblemData;
    }

    /**
     * @param string $emblemData
     */
    public function setEmblemData($emblemData)
    {
        $this->emblemData = $emblemData;
    }

    /**
     * @return mixed
     */
    public function getWoechars()
    {
        return $this->woechars;
    }

    /**
     * @param mixed $woechars
     */
    public function setWoechars($woechars)
    {
        $this->woechars = $woechars;
    }

    /**
     * @return int
     */
    public function getHoldTime()
    {
        return $this->holdTime;
    }

    /**
     * @param int $holdTime
     */
    public function setHoldTime($holdTime)
    {
        $this->holdTime = $holdTime;
    }

    /**
     * @return array
     */
    public function getCastleList()
    {
        return $this->castleList;
    }

    /**
     * @param array $castleList
     */
    public function setCastleList($castleList)
    {
        $this->castleList = $castleList;
    }

    public function jsonWoeChars()
    {
        $chars = array();
        /** @var Character $character */
        foreach ($this->woechars as $character) {
            $chars[] = $character->jsonSerialize();
        }

        return $chars;
    }
}
