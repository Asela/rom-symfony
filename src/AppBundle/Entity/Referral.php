<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Referral
 *
 * @ORM\Table(name="referrals", indexes={@ORM\Index(name="master_id", columns={"master_id"})})
 * @ORM\Entity
 */
class Referral
{
    /**
     * @var integer
     *
     * @ORM\Column(name="master_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $masterId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="referrer", type="integer", nullable=false)
     */
    private $referrer = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="received", type="integer", nullable=false)
     */
    private $received = '0';

    /**
     * Referral constructor.
     * @param int $masterId
     * @param int $referralCode
     */
    public function __construct($masterId, $referrer)
    {
        $this->masterId = $masterId;
        $this->referrer = $referrer;
    }

    /**
     * @return int
     */
    public function getMasterId()
    {
        return $this->masterId;
    }

    /**
     * @param int $masterId
     */
    public function setMasterId($masterId)
    {
        $this->masterId = $masterId;
    }

    /**
     * @return int
     */
    public function getReferrer()
    {
        return $this->referrer;
    }

    /**
     * @param int $referrer
     */
    public function setReferrer($referrer)
    {
        $this->referrer = $referrer;
    }

    /**
     * @return boolean
     */
    public function isReceived()
    {
        return $this->received;
    }

    /**
     * @param boolean $received
     */
    public function setReceived($received)
    {
        $this->received = $received;
    }
}
