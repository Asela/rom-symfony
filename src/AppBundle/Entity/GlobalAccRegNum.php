<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlobalAccRegNum
 *
 * @ORM\Table(name="global_acc_reg_num", indexes={@ORM\Index(name="account_id", columns={"account_id"})})
 * @ORM\Entity
 */
class GlobalAccRegNum
{
    /**
     * @var integer
     *
     * @ORM\Column(name="account_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $accountId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="`key`", type="string", length=32)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $key = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="`index`", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $index = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="`value`", type="integer", nullable=false)
     */
    private $value = '0';

    /**
     * GlobalAccRegNum constructor.
     * @param int $accountId
     * @param string $key
     * @param int $value
     */
    public function __construct($accountId, $key, $value)
    {
        $this->accountId = $accountId;
        $this->key = $key;
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param int $accountId
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return int
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param int $index
     */
    public function setIndex($index)
    {
        $this->index = $index;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @param int $value
     */
    public function updateValue($value)
    {
        $this->value += $value;
    }
}
