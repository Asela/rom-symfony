<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IpnLog
 *
 * @ORM\Table(name="ipn_log", indexes={@ORM\Index(name="txn_id", columns={"txn_id"})})
 * @ORM\Entity
 */
class IpnLog
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="txn_id", type="string", length=20, nullable=true)
     */
    private $txnId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="complete", type="boolean")
     */
    private $complete = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=255, nullable=true)
     */
    private $message;

    /**
     * @var integer
     *
     * @ORM\Column(name="paypal_log_id", type="integer", nullable=true)
     */
    private $paypalLogId;


    /**
     * @var PaypalLog
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\PaypalLog")
     * @ORM\JoinColumn(name="paypal_log_id", referencedColumnName="id")
     */
    private $paypalLog;


    /**
     * PaypalLog constructor.
     */
    public function __construct($txn_id, $complete, $message, $paypalLog)
    {
        $this->date = new \DateTime();
        $this->txnId = $txn_id;
        $this->complete = $complete;
        $this->message = $message;
        $this->paypalLog = $paypalLog;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     *
     */
    public function setDate()
    {
        $this->date = new \DateTime();
    }

    /**
     * @return string
     */
    public function getTxnId()
    {
        return $this->txnId;
    }

    /**
     * @param string $txnId
     */
    public function setTxnId($txnId)
    {
        $this->txnId = $txnId;
    }

    /**
     * @return boolean
     */
    public function isComplete()
    {
        return $this->complete;
    }

    /**
     * @param boolean $complete
     */
    public function setComplete($complete)
    {
        $this->complete = $complete;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getPaypalLogId()
    {
        return $this->paypalLogId;
    }

    /**
     * @param int $paypalLogId
     */
    public function setPaypalLogId($paypalLogId)
    {
        $this->paypalLogId = $paypalLogId;
    }

    /**
     * @return PaypalLog
     */
    public function getPaypalLog()
    {
        return $this->paypalLog;
    }

    /**
     * @param PaypalLog $paypalLog
     */
    public function setPaypalLog($paypalLog)
    {
        $this->paypalLog = $paypalLog;
    }


}

