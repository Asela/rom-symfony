<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Punishmentlog
 *
 * @ORM\Table(name="punishmentlog", indexes={@ORM\Index(name="src_account_id", columns={"src_account_id"}), @ORM\Index(name="src_char_id", columns={"src_char_id"})})
 * @ORM\Entity
 */
class Punishmentlog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="punishment_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $punishmentId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="punishment_date", type="datetime", nullable=false)
     */
    private $punishmentDate = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="src_account_id", type="integer", nullable=false)
     */
    private $srcAccountId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="src_char_id", type="integer", nullable=false)
     */
    private $srcCharId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="src_char_name", type="string", length=25, nullable=false)
     */
    private $srcCharName = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="target_master_id", type="integer", nullable=false)
     */
    private $targetMasterId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="target_account_id", type="integer", nullable=false)
     */
    private $targetAccountId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="target_char_id", type="integer", nullable=false)
     */
    private $targetCharId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="target_char_name", type="string", length=25, nullable=false)
     */
    private $targetCharName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="command", type="string", length=255, nullable=false)
     */
    private $command = '';

    /**
     * @var string
     *
     * @ORM\Column(name="full_command", type="string", length=255, nullable=false)
     */
    private $fullCommand = '';

    /**
     * @return int
     */
    public function getPunishmentId()
    {
        return $this->punishmentId;
    }

    /**
     * @param int $punishmentId
     */
    public function setPunishmentId($punishmentId)
    {
        $this->punishmentId = $punishmentId;
    }

    /**
     * @return \DateTime
     */
    public function getPunishmentDate()
    {
        return $this->punishmentDate;
    }

    /**
     * @param \DateTime $punishmentDate
     */
    public function setPunishmentDate($punishmentDate)
    {
        $this->punishmentDate = $punishmentDate;
    }

    /**
     * @return int
     */
    public function getSrcAccountId()
    {
        return $this->srcAccountId;
    }

    /**
     * @param int $srcAccountId
     */
    public function setSrcAccountId($srcAccountId)
    {
        $this->srcAccountId = $srcAccountId;
    }

    /**
     * @return int
     */
    public function getSrcCharId()
    {
        return $this->srcCharId;
    }

    /**
     * @param int $srcCharId
     */
    public function setSrcCharId($srcCharId)
    {
        $this->srcCharId = $srcCharId;
    }

    /**
     * @return string
     */
    public function getSrcCharName()
    {
        return $this->srcCharName;
    }

    /**
     * @param string $srcCharName
     */
    public function setSrcCharName($srcCharName)
    {
        $this->srcCharName = $srcCharName;
    }

    /**
     * @return int
     */
    public function getTargetMasterId()
    {
        return $this->targetMasterId;
    }

    /**
     * @param int $targetMasterId
     */
    public function setTargetMasterId($targetMasterId)
    {
        $this->targetMasterId = $targetMasterId;
    }

    /**
     * @return int
     */
    public function getTargetAccountId()
    {
        return $this->targetAccountId;
    }

    /**
     * @param int $targetAccountId
     */
    public function setTargetAccountId($targetAccountId)
    {
        $this->targetAccountId = $targetAccountId;
    }

    /**
     * @return int
     */
    public function getTargetCharId()
    {
        return $this->targetCharId;
    }

    /**
     * @param int $targetCharId
     */
    public function setTargetCharId($targetCharId)
    {
        $this->targetCharId = $targetCharId;
    }

    /**
     * @return string
     */
    public function getTargetCharName()
    {
        return $this->targetCharName;
    }

    /**
     * @param string $targetCharName
     */
    public function setTargetCharName($targetCharName)
    {
        $this->targetCharName = $targetCharName;
    }

    /**
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * @param string $command
     */
    public function setCommand($command)
    {
        $this->command = $command;
    }

    /**
     * @return string
     */
    public function getFullCommand()
    {
        return $this->fullCommand;
    }

    /**
     * @param string $fullCommand
     */
    public function setFullCommand($fullCommand)
    {
        $this->fullCommand = $fullCommand;
    }
}

