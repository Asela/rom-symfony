<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaypalLog
 *
 * @ORM\Table(name="paypal_log", indexes={@ORM\Index(name="account_id", columns={"account_id"}), @ORM\Index(name="parent_txn_id", columns={"parent_txn_id"}), @ORM\Index(name="txn_id", columns={"txn_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PaypalLogRepository")
 */
class PaypalLog
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="account_id", type="integer", nullable=true)
     */
    private $accountId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="credits", type="integer", nullable=true)
     */
    private $credits = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_email", type="string", length=60, nullable=true)
     */
    private $receiverEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="item_name", type="string", length=100, nullable=true)
     */
    private $itemName;

    /**
     * @var string
     *
     * @ORM\Column(name="item_number", type="string", length=10, nullable=true)
     */
    private $itemNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="quantity", type="string", length=6, nullable=true)
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_status", type="string", length=20, nullable=true)
     */
    private $paymentStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="pending_reason", type="string", length=20, nullable=true)
     */
    private $pendingReason;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_date", type="string", length=40, nullable=true)
     */
    private $paymentDate;

    /**
     * @var string
     *
     * @ORM\Column(name="mc_gross", type="string", length=20, nullable=true)
     */
    private $mcGross;

    /**
     * @var string
     *
     * @ORM\Column(name="mc_fee", type="string", length=20, nullable=true)
     */
    private $mcFee;

    /**
     * @var string
     *
     * @ORM\Column(name="tax", type="string", length=20, nullable=true)
     */
    private $tax;

    /**
     * @var string
     *
     * @ORM\Column(name="mc_currency", type="string", length=3, nullable=true)
     */
    private $mcCurrency;

    /**
     * @var string
     *
     * @ORM\Column(name="parent_txn_id", type="string", length=20, nullable=true)
     */
    private $parentTxnId;

    /**
     * @var string
     *
     * @ORM\Column(name="txn_id", type="string", length=20, nullable=true)
     */
    private $txnId;

    /**
     * @var string
     *
     * @ORM\Column(name="txn_type", type="string", length=20, nullable=true)
     */
    private $txnType;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=30, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=40, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="address_street", type="string", length=50, nullable=true)
     */
    private $addressStreet;

    /**
     * @var string
     *
     * @ORM\Column(name="address_city", type="string", length=30, nullable=true)
     */
    private $addressCity;

    /**
     * @var string
     *
     * @ORM\Column(name="address_state", type="string", length=30, nullable=true)
     */
    private $addressState;

    /**
     * @var string
     *
     * @ORM\Column(name="address_zip", type="string", length=20, nullable=true)
     */
    private $addressZip;

    /**
     * @var string
     *
     * @ORM\Column(name="address_country", type="string", length=30, nullable=true)
     */
    private $addressCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="address_status", type="string", length=11, nullable=true)
     */
    private $addressStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="payer_email", type="string", length=60, nullable=true)
     */
    private $payerEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="payer_status", type="string", length=10, nullable=true)
     */
    private $payerStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_type", type="string", length=10, nullable=true)
     */
    private $paymentType;

    /**
     * @var string
     *
     * @ORM\Column(name="notify_version", type="string", length=10, nullable=true)
     */
    private $notifyVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="verify_sign", type="string", length=255, nullable=true)
     */
    private $verifySign;

    /**
     * @var string
     *
     * @ORM\Column(name="referrer_id", type="string", length=10, nullable=true)
     */
    private $referrerId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="process_date", type="datetime", nullable=true)
     */
    private $processDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hold_until", type="datetime", nullable=true)
     */
    private $holdUntil;

    /**
     * PaypalLog constructor.
     */
    public function __construct($ipn_info, $credits = 0)
    {
        $this->accountId = (isset($ipn_info['custom'])) ? $ipn_info['custom'] : null;
        $this->credits = $credits;
        $this->receiverEmail = (isset($ipn_info['receiver_email'])) ? $ipn_info['receiver_email'] : null;;
        $this->itemName = (isset($ipn_info['item_name'])) ? $ipn_info['item_name'] : null;;
		$this->itemNumber = (isset($ipn_info['item_number'])) ? $ipn_info['item_number'] : null;;
		$this->quantity = (isset($ipn_info['quantity'])) ? $ipn_info['quantity'] : null;
		$this->paymentStatus = (isset($ipn_info['payment_status'])) ? $ipn_info['payment_status'] : null;
		$this->pendingReason = (isset($ipn_info['pending_reason'])) ? $ipn_info['pending_reason'] : null;
		$this->paymentDate = (isset($ipn_info['payment_date'])) ? $ipn_info['payment_date'] : null;
		$this->mcGross = (isset($ipn_info['mc_gross'])) ? $ipn_info['mc_gross'] : null;
		$this->mcFee = (isset($ipn_info['mc_fee'])) ? $ipn_info['mc_fee'] : null;
		$this->tax = (isset($ipn_info['tax'])) ? $ipn_info['tax'] : null;
		$this->mcCurrency = (isset($ipn_info['mc_currency'])) ? $ipn_info['mc_currency'] : null;
		$this->parentTxnId = (isset($ipn_info['parent_txn_id'])) ? $ipn_info['parent_txn_id'] : null;
		$this->txnId = (isset($ipn_info['txn_id'])) ? $ipn_info['txn_id'] : null;
		$this->txnType = (isset($ipn_info['txn_type'])) ? $ipn_info['txn_type'] : null;
		$this->firstName = (isset($ipn_info['first_name'])) ? $ipn_info['first_name'] : null;
		$this->lastName = (isset($ipn_info['last_name'])) ? $ipn_info['last_name'] : null;
		$this->addressStreet = (isset($ipn_info['address_street'])) ? $ipn_info['address_street'] : null;
		$this->addressCity = (isset($ipn_info['address_city'])) ? $ipn_info['address_city'] : null;
		$this->addressState = (isset($ipn_info['address_state'])) ? $ipn_info['address_state'] : null;
		$this->addressZip = (isset($ipn_info['address_zip'])) ? $ipn_info['address_zip'] : null;
		$this->addressCountry = (isset($ipn_info['address_country'])) ? $ipn_info['address_country'] : null;
		$this->addressStatus = (isset($ipn_info['address_status'])) ? $ipn_info['address_status'] : null;
		$this->payerEmail = (isset($ipn_info['payer_email'])) ? $ipn_info['payer_email'] : null;
		$this->payerStatus = (isset($ipn_info['payer_status'])) ? $ipn_info['payer_status'] : null;
		$this->paymentType = (isset($ipn_info['payment_type'])) ? $ipn_info['payment_type'] : null;
		$this->notifyVersion = (isset($ipn_info['notify_version'])) ? $ipn_info['notify_version'] : null;
		$this->verifySign = (isset($ipn_info['verify_sign'])) ? $ipn_info['verify_sign'] : null;
		$this->referrerId = (isset($ipn_info['referrer_id'])) ? $ipn_info['referrer_id'] : null;
	    $this->processDate = new \DateTime();
		$this->holdUntil = (isset($ipn_info['hold_until'])) ? $ipn_info['hold_until'] : null;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param int $accountId
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
    }

    /**
     * @return int
     */
    public function getCredits()
    {
        return $this->credits;
    }

    /**
     * @param int $credits
     */
    public function setCredits($credits)
    {
        $this->credits = $credits;
    }

    /**
     * @return string
     */
    public function getReceiverEmail()
    {
        return $this->receiverEmail;
    }

    /**
     * @param string $receiverEmail
     */
    public function setReceiverEmail($receiverEmail)
    {
        $this->receiverEmail = $receiverEmail;
    }

    /**
     * @return string
     */
    public function getItemName()
    {
        return $this->itemName;
    }

    /**
     * @param string $itemName
     */
    public function setItemName($itemName)
    {
        $this->itemName = $itemName;
    }

    /**
     * @return string
     */
    public function getItemNumber()
    {
        return $this->itemNumber;
    }

    /**
     * @param string $itemNumber
     */
    public function setItemNumber($itemNumber)
    {
        $this->itemNumber = $itemNumber;
    }

    /**
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param string $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return string
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    /**
     * @param string $paymentStatus
     */
    public function setPaymentStatus($paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;
    }

    /**
     * @return string
     */
    public function getPendingReason()
    {
        return $this->pendingReason;
    }

    /**
     * @param string $pendingReason
     */
    public function setPendingReason($pendingReason)
    {
        $this->pendingReason = $pendingReason;
    }

    /**
     * @return string
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    /**
     * @param string $paymentDate
     */
    public function setPaymentDate($paymentDate)
    {
        $this->paymentDate = $paymentDate;
    }

    /**
     * @return string
     */
    public function getMcGross()
    {
        return $this->mcGross;
    }

    /**
     * @param string $mcGross
     */
    public function setMcGross($mcGross)
    {
        $this->mcGross = $mcGross;
    }

    /**
     * @return string
     */
    public function getMcFee()
    {
        return $this->mcFee;
    }

    /**
     * @param string $mcFee
     */
    public function setMcFee($mcFee)
    {
        $this->mcFee = $mcFee;
    }

    /**
     * @return string
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param string $tax
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
    }

    /**
     * @return string
     */
    public function getMcCurrency()
    {
        return $this->mcCurrency;
    }

    /**
     * @param string $mcCurrency
     */
    public function setMcCurrency($mcCurrency)
    {
        $this->mcCurrency = $mcCurrency;
    }

    /**
     * @return string
     */
    public function getParentTxnId()
    {
        return $this->parentTxnId;
    }

    /**
     * @param string $parentTxnId
     */
    public function setParentTxnId($parentTxnId)
    {
        $this->parentTxnId = $parentTxnId;
    }

    /**
     * @return string
     */
    public function getTxnId()
    {
        return $this->txnId;
    }

    /**
     * @param string $txnId
     */
    public function setTxnId($txnId)
    {
        $this->txnId = $txnId;
    }

    /**
     * @return string
     */
    public function getTxnType()
    {
        return $this->txnType;
    }

    /**
     * @param string $txnType
     */
    public function setTxnType($txnType)
    {
        $this->txnType = $txnType;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getAddressStreet()
    {
        return $this->addressStreet;
    }

    /**
     * @param string $addressStreet
     */
    public function setAddressStreet($addressStreet)
    {
        $this->addressStreet = $addressStreet;
    }

    /**
     * @return string
     */
    public function getAddressCity()
    {
        return $this->addressCity;
    }

    /**
     * @param string $addressCity
     */
    public function setAddressCity($addressCity)
    {
        $this->addressCity = $addressCity;
    }

    /**
     * @return string
     */
    public function getAddressState()
    {
        return $this->addressState;
    }

    /**
     * @param string $addressState
     */
    public function setAddressState($addressState)
    {
        $this->addressState = $addressState;
    }

    /**
     * @return string
     */
    public function getAddressZip()
    {
        return $this->addressZip;
    }

    /**
     * @param string $addressZip
     */
    public function setAddressZip($addressZip)
    {
        $this->addressZip = $addressZip;
    }

    /**
     * @return string
     */
    public function getAddressCountry()
    {
        return $this->addressCountry;
    }

    /**
     * @param string $addressCountry
     */
    public function setAddressCountry($addressCountry)
    {
        $this->addressCountry = $addressCountry;
    }

    /**
     * @return string
     */
    public function getAddressStatus()
    {
        return $this->addressStatus;
    }

    /**
     * @param string $addressStatus
     */
    public function setAddressStatus($addressStatus)
    {
        $this->addressStatus = $addressStatus;
    }

    /**
     * @return string
     */
    public function getPayerEmail()
    {
        return $this->payerEmail;
    }

    /**
     * @param string $payerEmail
     */
    public function setPayerEmail($payerEmail)
    {
        $this->payerEmail = $payerEmail;
    }

    /**
     * @return string
     */
    public function getPayerStatus()
    {
        return $this->payerStatus;
    }

    /**
     * @param string $payerStatus
     */
    public function setPayerStatus($payerStatus)
    {
        $this->payerStatus = $payerStatus;
    }

    /**
     * @return string
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * @param string $paymentType
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;
    }

    /**
     * @return string
     */
    public function getNotifyVersion()
    {
        return $this->notifyVersion;
    }

    /**
     * @param string $notifyVersion
     */
    public function setNotifyVersion($notifyVersion)
    {
        $this->notifyVersion = $notifyVersion;
    }

    /**
     * @return string
     */
    public function getVerifySign()
    {
        return $this->verifySign;
    }

    /**
     * @param string $verifySign
     */
    public function setVerifySign($verifySign)
    {
        $this->verifySign = $verifySign;
    }

    /**
     * @return string
     */
    public function getReferrerId()
    {
        return $this->referrerId;
    }

    /**
     * @param string $referrerId
     */
    public function setReferrerId($referrerId)
    {
        $this->referrerId = $referrerId;
    }

    /**
     * @return \DateTime
     */
    public function getProcessDate()
    {
        return $this->processDate;
    }

    /**
     * @param \DateTime $processDate
     */
    public function setProcessDate($processDate)
    {
        $this->processDate = $processDate;
    }

    /**
     * @return \DateTime
     */
    public function getHoldUntil()
    {
        return $this->holdUntil;
    }

    /**
     * @param \DateTime $holdUntil
     */
    public function setHoldUntil($holdUntil)
    {
        $this->holdUntil = $holdUntil;
    }
}

