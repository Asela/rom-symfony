<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ZenyRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ZenyRepository extends EntityRepository
{
    public function getTotalZeny()
    {
        $qb = $this->createQueryBuilder('z')
            ->select('sum(z.amount) as total_zeny')
            ->getQuery();
        return ($qb->getSingleScalarResult() === NULL ? 0 : $qb->getSingleScalarResult());
    }

    /**
     * @param array $active_accounts
     * @return integer
     */
    public function countActiveZeny($active_accounts)
    {
        $qb = $this->createQueryBuilder('z')
            ->select('sum(z.amount) as active_zeny')
            ->where('z.accountId in (:ids)')
            ->setParameter('ids', $active_accounts)
            ->getQuery();
        return ($qb->getSingleScalarResult() === NULL ? 0 : $qb->getSingleScalarResult());
    }
}
	
