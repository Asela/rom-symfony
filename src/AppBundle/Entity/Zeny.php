<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Zeny
 *
 * @ORM\Table(name="zeny")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ZenyRepository")
 */
class Zeny
{
    /**
     * @var integer
     *
     * @ORM\Column(name="account_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $accountId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="amount", type="integer", nullable=false)
     */
    private $amount = '0';

    /**
     * @var GameAccount
     * @ORM\OneToOne(targetEntity="\AppBundle\Entity\GameAccount", inversedBy="zeny")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="account_id")
     */
    private $gameAccount;

    /**
     * @return int
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param int $accountId
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
 * @param int $amount
 */
    public function updateAmount($amount) {
        $this->amount += $amount;
    }

    /**
     * @return mixed
     */
    public function getGameAccount()
    {
        return $this->gameAccount;
    }

    /**
     * @param mixed $gameAccount
     */
    public function setGameAccount($gameAccount)
    {
        $this->gameAccount = $gameAccount;
    }

}
