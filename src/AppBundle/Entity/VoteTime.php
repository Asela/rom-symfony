<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VoteTime
 *
 * @ORM\Table(name="web_vote_times", indexes={@ORM\Index(name="master_id", columns={"master_id"}),@ORM\Index(name="site_id", columns={"site_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\VoteTimeRepository")
 */
class VoteTime
{
    /**
     * @var integer
     *
     * @ORM\Column(name="master_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $masterId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="site_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $siteId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=100, nullable=false)
     */
    private $ip = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime", nullable=false)
     */
    private $time = '0000-00-00 00:00:00';

    /**
     * VoteTime constructor.
     * @param int $masterId
     * @param int $siteId
     * @param string $ip
     */
    public function __construct($masterId, $siteId, $ip)
    {
        $this->masterId = $masterId;
        $this->siteId = $siteId;
        $this->ip = $ip;
        $this->time = new \DateTime();
    }

    /**
     * @return int
     */
    public function getMasterId()
    {
        return $this->masterId;
    }

    /**
     * @param int $masterId
     */
    public function setMasterId($masterId)
    {
        $this->masterId = $masterId;
    }

    /**
     * @return int
     */
    public function getSiteId()
    {
        return $this->siteId;
    }

    /**
     * @param int $siteId
     */
    public function setSiteId($siteId)
    {
        $this->siteId = $siteId;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Autoset time
     */
    public function setTime()
    {
        $this->time = new \DateTime();
    }

}
