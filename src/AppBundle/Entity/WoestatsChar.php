<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WoestatsChar
 *
 * @ORM\Table(name="woestats_char")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\WoestatsCharRepository", readOnly=true)
 */
class WoestatsChar implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="char_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $charId = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="agit_flag", type="boolean")
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $agitFlag = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="class", type="string", length=30, nullable=false)
     */
    private $class = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="base_level", type="smallint", nullable=false)
     */
    private $baseLevel = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="job_level", type="smallint", nullable=false)
     */
    private $jobLevel = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="guild_id", type="integer", nullable=false)
     */
    private $guildId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="kills", type="smallint", nullable=false)
     */
    private $kills = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="deaths", type="smallint", nullable=false)
     */
    private $deaths = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="emperiums", type="integer", nullable=false)
     */
    private $emperiums = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="barricades", type="integer", nullable=false)
     */
    private $barricades = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="damage_dealt", type="integer", nullable=false)
     */
    private $damageDealt = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="damage_taken", type="integer", nullable=false)
     */
    private $damageTaken = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="skills", type="string", length=255, nullable=false)
     */
    private $skills = '';

    /**
     * @var string
     *
     * @ORM\Column(name="items", type="string", length=255, nullable=false)
     */
    private $items = '';

    /**
     * @var WoestatsGuild
     * @ORM\ManyToOne(targetEntity="WoestatsGuild", inversedBy="woechars")
     * @ORM\JoinColumn(name="guild_id", referencedColumnName="guild_id")
     */
    private $woeguild;

    /**
     * @var array
     */
    private $skillsList;

    /**
     * @var integer
     */
    private $skillsTotal;

    /**
     * @var array
     */
    private $itemsList;

    /**
     * @var integer
     */
    private $itemsTotal;

    /**
     * @return int
     */
    public function getCharId()
    {
        return $this->charId;
    }

    /**
     * @param int $charId
     */
    public function setCharId($charId)
    {
        $this->charId = $charId;
    }

    /**
     * @return boolean
     */
    public function isAgitFlag()
    {
        return $this->agitFlag;
    }

    /**
     * @param boolean $agitFlag
     */
    public function setAgitFlag($agitFlag)
    {
        $this->agitFlag = $agitFlag;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * @return int
     */
    public function getBaseLevel()
    {
        return $this->baseLevel;
    }

    /**
     * @param int $baseLevel
     */
    public function setBaseLevel($baseLevel)
    {
        $this->baseLevel = $baseLevel;
    }

    /**
     * @return int
     */
    public function getJobLevel()
    {
        return $this->jobLevel;
    }

    /**
     * @param int $jobLevel
     */
    public function setJobLevel($jobLevel)
    {
        $this->jobLevel = $jobLevel;
    }

    /**
     * @return int
     */
    public function getGuildId()
    {
        return $this->guildId;
    }

    /**
     * @param int $guildId
     */
    public function setGuildId($guildId)
    {
        $this->guildId = $guildId;
    }

    /**
     * @return int
     */
    public function getKills()
    {
        return $this->kills;
    }

    /**
     * @param int $kills
     */
    public function setKills($kills)
    {
        $this->kills = $kills;
    }

    /**
     * @return int
     */
    public function getDeaths()
    {
        return $this->deaths;
    }

    /**
     * @param int $deaths
     */
    public function setDeaths($deaths)
    {
        $this->deaths = $deaths;
    }

    /**
     * @return int
     */
    public function getEmperiums()
    {
        return $this->emperiums;
    }

    /**
     * @param int $emperiums
     */
    public function setEmperiums($emperiums)
    {
        $this->emperiums = $emperiums;
    }

    /**
     * @return int
     */
    public function getBarricades()
    {
        return $this->barricades;
    }

    /**
     * @param int $barricades
     */
    public function setBarricades($barricades)
    {
        $this->barricades = $barricades;
    }

    /**
     * @return int
     */
    public function getDamageDealt()
    {
        return $this->damageDealt;
    }

    /**
     * @param int $damageDealt
     */
    public function setDamageDealt($damageDealt)
    {
        $this->damageDealt = $damageDealt;
    }

    /**
     * @return int
     */
    public function getDamageTaken()
    {
        return $this->damageTaken;
    }

    /**
     * @param int $damageTaken
     */
    public function setDamageTaken($damageTaken)
    {
        $this->damageTaken = $damageTaken;
    }

    /**
     * @return string
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @param string $skills
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;
    }

    /**
     * @return string
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param string $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @return WoestatsGuild
     */
    public function getWoeguild()
    {
        if ($this->guildId == 0) return null;
        return $this->woeguild;
    }

    /**
     * @param WoestatsGuild $woeguild
     */
    public function setWoeguild($woeguild)
    {
        $this->woeguild = $woeguild;
    }

    /**
     * @return array
     */
    public function getSkillsList()
    {
        return $this->skillsList;
    }

    /**
     * @param array $skillsList
     */
    public function setSkillsList($skillsList)
    {
        $this->skillsList = $skillsList;
    }

    /**
     * @return int
     */
    public function getSkillsTotal()
    {
        return $this->skillsTotal;
    }

    /**
     * @param int $skillsTotal
     */
    public function setSkillsTotal($skillsTotal)
    {
        $this->skillsTotal = $skillsTotal;
    }

    /**
     * @return array
     */
    public function getItemsList()
    {
        return $this->itemsList;
    }

    /**
     * @param array $itemsList
     */
    public function setItemsList($itemsList)
    {
        $this->itemsList = $itemsList;
    }

    /**
     * @return int
     */
    public function getItemsTotal()
    {
        return $this->itemsTotal;
    }

    /**
     * @param int $itemsTotal
     */
    public function setItemsTotal($itemsTotal)
    {
        $this->itemsTotal = $itemsTotal;
    }


    /**
     * @return string
     */
    public function getGuildname()
    {
        if ($this->getWoeguild() !== NULL)
            return $this->getWoeguild()->getName();
        else
            return "";
    }

    public function jsonSerialize()
    {
       $ret = ['char_id' => $this->charId,
           'char_name' => $this->name,
           'guildname' => $this->getGuildname(),
           'class' => $this->class,
           'kills' => number_format($this->kills),
           'deaths' => number_format($this->deaths),
           'damage_dealt' => number_format($this->damageDealt),
           'damage_taken' => number_format($this->damageTaken),
           'objectives' => number_format($this->barricades+$this->emperiums)
        ];
        return $ret;
    }
}
