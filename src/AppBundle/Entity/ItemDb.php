<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItemDb
 *
 * @ORM\Table(name="item_db")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ItemDbRepository", readOnly=true)
 */
class ItemDb
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="smallint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="name_english", type="string", length=50, nullable=false)
     */
    private $nameEnglish = '';

    /**
     * @var string
     *
     * @ORM\Column(name="name_japanese", type="string", length=50, nullable=false)
     */
    private $nameJapanese = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint", nullable=false)
     */
    private $type = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="price_buy", type="integer", nullable=true)
     */
    private $priceBuy;

    /**
     * @var integer
     *
     * @ORM\Column(name="price_sell", type="integer", nullable=true)
     */
    private $priceSell;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="smallint", nullable=false)
     */
    private $weight = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="attack", type="smallint", nullable=true)
     */
    private $attack;

    /**
     * @var integer
     *
     * @ORM\Column(name="defence", type="smallint", nullable=true)
     */
    private $defence;

    /**
     * @var integer
     *
     * @ORM\Column(name="range", type="integer", nullable=true)
     */
    private $range;

    /**
     * @var integer
     *
     * @ORM\Column(name="slots", type="smallint", nullable=true)
     */
    private $slots;

    /**
     * @var integer
     *
     * @ORM\Column(name="equip_jobs", type="integer", nullable=true)
     */
    private $equipJobs;

    /**
     * @var integer
     *
     * @ORM\Column(name="equip_upper", type="smallint", nullable=true)
     */
    private $equipUpper;

    /**
     * @var integer
     *
     * @ORM\Column(name="equip_genders", type="smallint", nullable=true)
     */
    private $equipGenders;

    /**
     * @var integer
     *
     * @ORM\Column(name="equip_locations", type="integer", nullable=true)
     */
    private $equipLocations;

    /**
     * @var integer
     *
     * @ORM\Column(name="weapon_level", type="smallint", nullable=true)
     */
    private $weaponLevel;

    /**
     * @var integer
     *
     * @ORM\Column(name="equip_level", type="smallint", nullable=true)
     */
    private $equipLevel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="refineable", type="boolean", nullable=true)
     */
    private $refineable;

    /**
     * @var integer
     *
     * @ORM\Column(name="view", type="smallint", nullable=true)
     */
    private $view;

    /**
     * @var string
     *
     * @ORM\Column(name="script", type="text", length=65535, nullable=true)
     */
    private $script;

    /**
     * @var string
     *
     * @ORM\Column(name="equip_script", type="text", length=65535, nullable=true)
     */
    private $equipScript;

    /**
     * @var string
     *
     * @ORM\Column(name="unequip_script", type="text", length=65535, nullable=true)
     */
    private $unequipScript;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNameEnglish()
    {
        return $this->nameEnglish;
    }

    /**
     * @param string $nameEnglish
     */
    public function setNameEnglish($nameEnglish)
    {
        $this->nameEnglish = $nameEnglish;
    }

    /**
     * @return string
     */
    public function getNameJapanese()
    {
        return $this->nameJapanese;
    }

    /**
     * @param string $nameJapanese
     */
    public function setNameJapanese($nameJapanese)
    {
        $this->nameJapanese = $nameJapanese;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getPriceBuy()
    {
        return $this->priceBuy;
    }

    /**
     * @param int $priceBuy
     */
    public function setPriceBuy($priceBuy)
    {
        $this->priceBuy = $priceBuy;
    }

    /**
     * @return int
     */
    public function getPriceSell()
    {
        return $this->priceSell;
    }

    /**
     * @param int $priceSell
     */
    public function setPriceSell($priceSell)
    {
        $this->priceSell = $priceSell;
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return int
     */
    public function getAttack()
    {
        return $this->attack;
    }

    /**
     * @param int $attack
     */
    public function setAttack($attack)
    {
        $this->attack = $attack;
    }

    /**
     * @return int
     */
    public function getDefence()
    {
        return $this->defence;
    }

    /**
     * @param int $defence
     */
    public function setDefence($defence)
    {
        $this->defence = $defence;
    }

    /**
     * @return int
     */
    public function getRange()
    {
        return $this->range;
    }

    /**
     * @param int $range
     */
    public function setRange($range)
    {
        $this->range = $range;
    }

    /**
     * @return int
     */
    public function getSlots()
    {
        return $this->slots;
    }

    /**
     * @param int $slots
     */
    public function setSlots($slots)
    {
        $this->slots = $slots;
    }

    /**
     * @return int
     */
    public function getEquipJobs()
    {
        return $this->equipJobs;
    }

    /**
     * @param int $equipJobs
     */
    public function setEquipJobs($equipJobs)
    {
        $this->equipJobs = $equipJobs;
    }

    /**
     * @return int
     */
    public function getEquipUpper()
    {
        return $this->equipUpper;
    }

    /**
     * @param int $equipUpper
     */
    public function setEquipUpper($equipUpper)
    {
        $this->equipUpper = $equipUpper;
    }

    /**
     * @return int
     */
    public function getEquipGenders()
    {
        return $this->equipGenders;
    }

    /**
     * @param int $equipGenders
     */
    public function setEquipGenders($equipGenders)
    {
        $this->equipGenders = $equipGenders;
    }

    /**
     * @return int
     */
    public function getEquipLocations()
    {
        return $this->equipLocations;
    }

    /**
     * @param int $equipLocations
     */
    public function setEquipLocations($equipLocations)
    {
        $this->equipLocations = $equipLocations;
    }

    /**
     * @return int
     */
    public function getWeaponLevel()
    {
        return $this->weaponLevel;
    }

    /**
     * @param int $weaponLevel
     */
    public function setWeaponLevel($weaponLevel)
    {
        $this->weaponLevel = $weaponLevel;
    }

    /**
     * @return int
     */
    public function getEquipLevel()
    {
        return $this->equipLevel;
    }

    /**
     * @param int $equipLevel
     */
    public function setEquipLevel($equipLevel)
    {
        $this->equipLevel = $equipLevel;
    }

    /**
     * @return boolean
     */
    public function isRefineable()
    {
        return $this->refineable;
    }

    /**
     * @param boolean $refineable
     */
    public function setRefineable($refineable)
    {
        $this->refineable = $refineable;
    }

    /**
     * @return int
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param int $view
     */
    public function setView($view)
    {
        $this->view = $view;
    }

    /**
     * @return string
     */
    public function getScript()
    {
        return $this->script;
    }

    /**
     * @param string $script
     */
    public function setScript($script)
    {
        $this->script = $script;
    }

    /**
     * @return string
     */
    public function getEquipScript()
    {
        return $this->equipScript;
    }

    /**
     * @param string $equipScript
     */
    public function setEquipScript($equipScript)
    {
        $this->equipScript = $equipScript;
    }

    /**
     * @return string
     */
    public function getUnequipScript()
    {
        return $this->unequipScript;
    }

    /**
     * @param string $unequipScript
     */
    public function setUnequipScript($unequipScript)
    {
        $this->unequipScript = $unequipScript;
    }

}

