<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PasswordRecovery
 *
 * @ORM\Table(name="web_password_recovery", indexes={@ORM\Index(name="master_id", columns={"master_id"})},indexes={@ORM\Index(name="code", columns={"code"})})
 * @ORM\Entity
 */
class PasswordRecovery
{
    /**
     * @var integer
     *
     * @ORM\Column(name="master_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $masterId = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=false)
     */
    private $code = '';

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\User")
     * @ORM\JoinColumn(name="master_id", referencedColumnName="master_id")
     */
    private $masterAccount;

    /**
     * AccRegNum constructor.
     * @param User $masterAccount
     */
    public function __construct($masterAccount)
    {
        $this->masterAccount = $masterAccount;
        $this->date = new \DateTime();
        $this->code = bin2hex(openssl_random_pseudo_bytes(16));
    }

    /**
     * @return int
     */
    public function getMasterId()
    {
        return $this->masterId;
    }

    /**
     * @param int $masterId
     */
    public function setMasterId($masterId)
    {
        $this->masterId = $masterId;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     *
     */
    public function setDate()
    {
        $this->date = new \DateTime();
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     *
     */
    public function setCode()
    {
        $this->code = bin2hex(openssl_random_pseudo_bytes(16));
    }

    /**
     * @return User
     */
    public function getMasterAccount()
    {
        return $this->masterAccount;
    }

    /**
     * @param User $masterAccount
     */
    public function setMasterAccount($masterAccount)
    {
        $this->masterAccount = $masterAccount;
    }

    /**
     * Generates a new code and date
     */
    public function update() {
        $this->setCode();
        $this->setDate();
    }


}
