<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class MenuBuilder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $factory;
    private $checker;

    public function __construct(FactoryInterface $factory, AuthorizationChecker $authorizationChecker)
    {
        $this->factory = $factory;
        $this->checker = $authorizationChecker;
    }

    public function leftMenu()
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttributes(array('class' => 'nav_ul pull-left'));

        $menu->addChild('trans.menu.home', array('route' => 'app_pages_index'));
            $menu->addChild('trans.menu.news', array('route' => 'app_pages_news'));
            $menu->addChild('trans.menu.shop', array('route' => 'app_pages_cashshop'));
            $menu->addChild('trans.menu.information', array('route' => 'app_pages_index'))
                ->setAttribute('dropdown', true);
            $menu['trans.menu.information']->addChild('trans.menu.server_information', array('uri' => 'https://riseofmidgard.com/forum/viewtopic.php?f=50&t=29'));
            $menu['trans.menu.information']->addChild('trans.menu.wiki', array('uri' => '/wiki'));
            $menu['trans.menu.information']->addChild('trans.menu.market', array('route' => 'app_pages_vendors'));
            $menu['trans.menu.information']->addChild('trans.menu.gm_logs', array('route' => 'app_pages_gmlogs'));
            $menu['trans.menu.information']->addChild('trans.menu.woe_stats', array('route' => 'app_woestats_index'));
            $menu['trans.menu.information']->addChild('trans.menu.who', array('uri' => 'https://riseofmidgard.com/forum/viewtopic.php?f=63&t=13'));
        return $menu;
    }

    public function rightMenu()
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttributes(array('class' => 'nav_ul'));
        $menu->addChild('trans.menu.forum', array('uri' => '/forum'));
        $menu->addChild('trans.menu.review_us', array('uri' => 'http://ratemyserver.net/index.php?page=detailedlistserver&serid=19323&itv=6&url_sname=Rise%20of%20Midgard'));
        if (!$this->checker->isGranted('ROLE_USER')) {
            $menu->addChild('trans.menu.login', [
                'uri' => '#login',
                'linkAttributes' => [
                    'data-toggle' => 'modal',
                    'data-backdrop' => 'true'
                ]
            ]);
            $menu->addChild('trans.menu.register', [
                'uri' => '#register',
                'linkAttributes' => [
                    'data-toggle' => 'modal',
                    'data-backdrop' => 'true'
                  ]
                ]);
        } else {
            $menu->addChild('trans.menu.support', array('route' => 'support_page'));
            $menu->addChild('trans.menu.control_panel', array('route' => 'app_pages_index'))
                ->setAttribute('dropdown', true);
            $menu['trans.menu.control_panel']->addChild('trans.menu.manage_master', array('route' => 'security_edit'));
            $menu['trans.menu.control_panel']->addChild('trans.menu.manage_gameaccount', array('route' => 'panel_index'));
            if ($this->checker->isGranted('ROLE_STAFF')) {
                $menu['trans.menu.control_panel']->addChild('trans.menu.admin_panel', array('route' => 'admin_index'));
            }
            $menu['trans.menu.control_panel']->addChild('trans.menu.vote', array('route' => 'voting_page'));
            $menu['trans.menu.control_panel']->addChild('trans.menu.logout', array('route' => 'security_logout'));
        }

        return $menu;
    }

}
