CREATE TABLE IF NOT EXISTS `master_account` (
  `master_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(39) NOT NULL,
  `password` varchar(60) NOT NULL,
  `roles` longtext NOT NULL,
  `state` varchar(45) NOT NULL,
  `birthdate` date NOT NULL,
  `allow_email` tinyint(1) NOT NULL,
  `money_spent` decimal(5,2) NOT NULL,
  `referral_code` varchar(45) NOT NULL,
  `country` char(2) NOT NULL,
  `registration_time` datetime NOT NULL,
  `registration_ip` varchar(45) NOT NULL,
  `last_login` datetime NOT NULL,
  `last_ip` varchar(45) NOT NULL,
  PRIMARY KEY (`master_id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4000000 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `paypal_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) unsigned DEFAULT '0',
  `credits` int(11) DEFAULT '0',
  `receiver_email` varchar(60) DEFAULT NULL,
  `item_name` varchar(100) DEFAULT NULL,
  `item_number` varchar(10) DEFAULT NULL,
  `quantity` varchar(6) DEFAULT NULL,
  `payment_status` varchar(20) DEFAULT NULL,
  `pending_reason` varchar(20) DEFAULT NULL,
  `payment_date` varchar(40) DEFAULT NULL,
  `mc_gross` varchar(20) DEFAULT NULL,
  `mc_fee` varchar(20) DEFAULT NULL,
  `tax` varchar(20) DEFAULT NULL,
  `mc_currency` varchar(3) DEFAULT NULL,
  `parent_txn_id` varchar(20) DEFAULT NULL,
  `txn_id` varchar(20) DEFAULT NULL,
  `txn_type` varchar(20) DEFAULT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(40) DEFAULT NULL,
  `address_street` varchar(50) DEFAULT NULL,
  `address_city` varchar(30) DEFAULT NULL,
  `address_state` varchar(30) DEFAULT NULL,
  `address_zip` varchar(20) DEFAULT NULL,
  `address_country` varchar(30) DEFAULT NULL,
  `address_status` varchar(11) DEFAULT NULL,
  `payer_email` varchar(60) DEFAULT NULL,
  `payer_status` varchar(10) DEFAULT NULL,
  `payment_type` varchar(10) DEFAULT NULL,
  `notify_version` varchar(10) DEFAULT NULL,
  `verify_sign` varchar(255) DEFAULT NULL,
  `referrer_id` varchar(10) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `hold_until` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `parent_txn_id` (`parent_txn_id`),
  KEY `txn_id` (`txn_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4407 DEFAULT CHARSET=latin1 COMMENT='All PayPal transactions that go through the IPN handler.';

/* Table created with server files too */
CREATE TABLE IF NOT EXISTS `pending_cashpoints` (
  `id` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) unsigned NOT NULL DEFAULT '0',
  `amount` int(11) NOT NULL DEFAULT '0',
  `received` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `web_action_log` (
  `id` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `src_master_id` int(11) unsigned NOT NULL DEFAULT '0',
  `target_master_id` int(11) unsigned NOT NULL DEFAULT '0',
  `target_account_id` int(11) unsigned NOT NULL DEFAULT '0',
  `type` varchar(255) NOT NULL DEFAULT '',
  `reason` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `src_master_id` (`src_master_id`),
  KEY `target_master_id` (`target_master_id`),
  KEY `target_account_id` (`target_account_id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

/* Table created with server files too */
CREATE TABLE IF NOT EXISTS `web_bans` (
  `account_id` int(11) unsigned NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `web_confirmations` (
  `master_id` int(11) unsigned NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `code` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`master_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `web_password_recovery` (
  `master_id` int(11) unsigned NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `code` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`master_id`),
  KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `ipn_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `txn_id` varchar(20) DEFAULT NULL,
  `complete` tinyint(1) unsigned DEFAULT '0',
  `message` varchar(255) DEFAULT NULL,
  `paypal_log_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `txn_id` (`txn_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='All IPN Logs';

CREATE TABLE IF NOT EXISTS `web_vote_sites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `url` varchar(100) NOT NULL,
  `img` varchar(100) NOT NULL,
  `points` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `delay` smallint(5) unsigned NOT NULL DEFAULT 720,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

REPLACE INTO `web_vote_sites` VALUES (1,'gtop100','http://www.gtop100.com/topsites/Ragnarok-Online/sitedetails/Rise-of-Midgard-91567?vote=1','',1,720);
REPLACE INTO `web_vote_sites` VALUES (2,'rotopserv','http://www.rotopserv.net/serveurs/rise-of-midgard/vote','',1,720);
REPLACE INTO `web_vote_sites` VALUES (3,'topservers200','http://www.topservers200.com/in.php?id=18862','',1,720);
REPLACE INTO `web_vote_sites` VALUES (4,'topg','http://topg.org/ragnarok-private-servers/in-450282','',1,720);
REPLACE INTO `web_vote_sites` VALUES (5,'topragnarok','http://www.topragnarok.org/votar/id7425/','',1,720);
REPLACE INTO `web_vote_sites` VALUES (6,'xtremetop100','http://www.xtremetop100.com/in.php?site=1132361711','',1,720);

CREATE TABLE IF NOT EXISTS `web_vote_times` (
  `master_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `site_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `ip` VARCHAR(100) NOT NULL DEFAULT '',
  `time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`master_id`, `site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/* Table created with server files too */
CREATE TABLE IF NOT EXISTS `master_account_points` (
  `master_id` int(10) unsigned NOT NULL DEFAULT '0',
  `vote_points` int(10) unsigned NOT NULL DEFAULT '0',
  `referral_points` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`master_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `referrals` (
  `master_id` int(10) unsigned NOT NULL DEFAULT '0',
  `referrer` int(10) unsigned NOT NULL DEFAULT '0',
  `received` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`master_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

