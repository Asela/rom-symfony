DELIMITER //

CREATE FUNCTION ParseItemName(item_name VARCHAR(50), refine INT, item_type INT, slots INT, card0 INT, card1 INT)
  RETURNS VARCHAR(100)

  BEGIN
    DECLARE parsed_name VARCHAR(100);
    DECLARE stars INT DEFAULT 0;
    DECLARE attribute INT DEFAULT 0;

	IF (item_type = 4 OR item_type = 5) AND slots>=0 THEN SET parsed_name = CONCAT(item_name, ' [', slots, ']');
    ELSE SET parsed_name = item_name;
    END IF;

	IF card0 = 0x0FF 
    THEN 
		SET attribute = card1&0x0f;
		SET stars =  (card1>>8)/5;

        CASE attribute
			WHEN 4 THEN SET parsed_name = CONCAT("Wind ", parsed_name);
			WHEN 3 THEN SET parsed_name = CONCAT("Fire ", parsed_name);
            WHEN 2 THEN SET parsed_name = CONCAT("Earth ", parsed_name);
            WHEN 1 THEN SET parsed_name = CONCAT("Ice ", parsed_name);
            ELSE BEGIN END;
		END CASE;
        
        CASE stars
			WHEN 3 THEN SET parsed_name = CONCAT("Very, Very, Very Strong ", parsed_name);
            WHEN 2 THEN SET parsed_name = CONCAT("Very, Very Strong ", parsed_name);
            WHEN 1 THEN SET parsed_name = CONCAT("Very Strong ", parsed_name);
            ELSE BEGIN END;
		END CASE;
	END IF;

	IF refine > 0 THEN SET parsed_name = CONCAT('+', refine, ' ', parsed_name);
    ELSE SET parsed_name = parsed_name;
    END IF;

    RETURN parsed_name;
  END //

DELIMITER ;