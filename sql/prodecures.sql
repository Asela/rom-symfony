DELIMITER |

CREATE PROCEDURE vendinglist()
BEGIN
	SELECT  ParseItemName(ib.name_japanese, ci.refine, ib.`type`, ib.slots, ci.card0, ci.card1) as parsed_name,
			ci.nameid,
			FORMAT(vi.amount,0) as amount, 
			FORMAT(vi.price,0) as price, 
			v.title as shop_name,
			c.`name` as vendor, 
            CONCAT_WS(" ", v.map, v.x, v.y) as location,
			v.map as map,
            v.x as x,
            v.y as y,
			ib0.id as card0,
			ib0.name_japanese as card0_name,
			ib1.id as card1,
			ib1.name_japanese as card1_name,
			ib2.id as card2,
			ib2.name_japanese as card2_name,
			ib3.id as card3,
			ib3.name_japanese as card3_name
	FROM server.vending_items vi
	JOIN server.vendings v ON
		v.id = vi.vending_id
	JOIN server.`char` c ON
		c.char_id = v.char_id
	JOIN server.cart_inventory ci ON
		ci.id = vi.cartinventory_id
		AND ci.char_id = v.char_id
	JOIN server.item_db ib ON
		ci.nameid = ib.id
	LEFT JOIN server.item_db ib0 ON
		ib0.id=ci.card0 and ib0.`type`=6
	LEFT JOIN server.item_db ib1 ON
		ib1.id=ci.card1 and ib1.`type`=6
	LEFT JOIN server.item_db ib2 ON
		ib2.id=ci.card2 and ib2.`type`=6
	LEFT JOIN server.item_db ib3 ON
		ib3.id=ci.card3 and ib3.`type`=6
	WHERE vi.amount > 0 AND vi.price > 0;
END;
|

DELIMITER |

CREATE PROCEDURE buyingstorelist()
BEGIN
	SELECT 	bi.item_id as nameid, 
			ib.name_japanese as parsed_name,
			FORMAT(bi.amount,0) as amount, 
			FORMAT(bi.price,0) as price, 
			b.title as shop_name,
			c.`name` as buyer,
			CONCAT_WS(" ", b.map, b.x, b.y) as location,
			b.map as map,
			b.x as x, 
			b.y as y
	FROM server.buyingstore_items bi
	JOIN server.buyingstores b ON
			b.id = bi.buyingstore_id
	JOIN server.`char` c ON
			c.char_id = b.char_id
	JOIN server.item_db ib ON
			bi.item_id = ib.id
	WHERE bi.amount > 0 AND bi.price > 0;
END;
|

DELIMITER |

CREATE PROCEDURE getActiveAccountIds()
BEGIN
	SELECT l.account_id 
    FROM login l 
    WHERE l.master_id IN (
					SELECT DISTINCT(master_id) 
                    FROM login 
                    WHERE lastlogin BETWEEN NOW() - INTERVAL 14 DAY AND NOW()
					AND group_id <> 0
                    );
END;
|

DELIMITER |

CREATE PROCEDURE getGMLogs()
BEGIN
	SELECT atcommand_id, atcommand_date, char_name, map, command
	FROM server_logs.atcommandlog
	WHERE atcommand_date BETWEEN NOW() - INTERVAL 14 DAY AND NOW() - INTERVAL 15 MINUTE
    ORDER BY atcommand_date desc
    LIMIT 1000;
END;
|

DELIMITER |

CREATE PROCEDURE getPointsSpent()
BEGIN
	SELECT IFNULL(abs(sum(c.amount)), 0) as points_spent
	FROM server_logs.cashlog c
	WHERE c.time BETWEEN NOW() - INTERVAL 7 DAY AND NOW()
    AND c.amount < 0
    AND c.type = '$';
END;
|

DELIMITER |

CREATE PROCEDURE getZenyEarned()
BEGIN
	SELECT IFNULL(sum(z.amount), 0) as zeny_earned
	FROM server_logs.zenylog z
	WHERE z.time BETWEEN NOW() - INTERVAL 7 DAY AND NOW()
    AND z.`type` IN ('S', 'M', 'D');
END;
|

DELIMITER |

CREATE PROCEDURE getWeeklyBansIssued()
BEGIN
	SELECT IFNULL(count(p.punishment_id), 0) as bans_issued
	FROM server_logs.punishmentlog p
	WHERE p.punishment_date BETWEEN NOW() - INTERVAL 7 DAY AND NOW()
    AND p.command in ('@charban', '@ban', '@block');
END;
|

DELIMITER |

CREATE PROCEDURE getUserPunishment(IN masterID INT(11))
BEGIN
	SELECT p.punishment_date as date,
    p.src_char_name as gm,
	p.target_char_name as char_name,
    p.full_command as command
	FROM server_logs.punishmentlog p
	WHERE p.punishment_date BETWEEN NOW() - INTERVAL 12 MONTH AND NOW()
    AND p.target_master_id = masterID;
END;
|

DELIMITER |

CREATE PROCEDURE getLatestUserPunishment()
BEGIN
	SELECT p.punishment_date as date,
    p.src_char_name as gm,
	p.target_char_name as char_name,
    p.full_command as command
	FROM server_logs.punishmentlog p
	ORDER BY p.punishment_date DESC
    LIMIT 500;
END;
|

DELIMITER |

CREATE PROCEDURE getUserWebActionLogs(IN masterID INT(11))
BEGIN
	SELECT w.date as date,
    w.src_master_id as gm,
    w.type as type,
    w.reason as reason
	FROM server.web_action_log w
    WHERE w.target_master_id = masterID;
END;
|

DELIMITER |

CREATE PROCEDURE getLatestUserWebActionLogs()
BEGIN
	SELECT w.date as date,
    w.src_master_id as gm,
	w.target_master_id as user,
    w.type as type,
    w.reason as reason
	FROM server.web_action_log w
    ORDER BY w.date DESC
    LIMIT 500;
END;
|

DELIMITER |

CREATE PROCEDURE getMVPCards()
BEGIN
	set @cardID = '4121,4123,4128,4131,4132,4134,4135,4137,4142,4143,4144,4145,4146,4147,4148,4168,4236,4263,4276,4302,4305,4318,4324,4330,4342,4352,4357,4359,4361,4363,4365,4367,4372,4374,4376,4386,4399,4403,4407,4408,4419,4425,4430,4441';

	SELECT ib.name_japanese as item,
	ib0.name_japanese as card0,
	ib1.name_japanese as card1,
	ib2.name_japanese as card2,
	ib3.name_japanese as card3,
	c.master_id as master_id,
	c.name as char_name,
	ifnull(g.name, "") as guild,
	"Inventory" as location,
	inv.amount as count
	FROM server.inventory inv
	JOIN server.`char` c ON c.char_id = inv.char_id
	LEFT JOIN server.guild g ON g.guild_id = c.guild_id
	JOIN server.item_db ib ON ib.id = inv.nameid
	LEFT JOIN server.item_db ib0 ON ib0.id=inv.card0 and ib0.`type`=6
	LEFT JOIN server.item_db ib1 ON ib1.id=inv.card1 and ib1.`type`=6
	LEFT JOIN server.item_db ib2 ON ib2.id=inv.card2 and ib2.`type`=6
	LEFT JOIN server.item_db ib3 ON ib3.id=inv.card3 and ib3.`type`=6
	WHERE FIND_IN_SET(inv.nameid, @cardID) != 0
		OR FIND_IN_SET(inv.card0, @cardID) != 0
		OR FIND_IN_SET(inv.card1, @cardID) != 0
		OR FIND_IN_SET(inv.card2, @cardID) != 0
		OR FIND_IN_SET(inv.card3, @cardID) != 0

	UNION ALL

	SELECT ib.name_japanese as item,
	ib0.name_japanese as card0,
	ib1.name_japanese as card1,
	ib2.name_japanese as card2,
	ib3.name_japanese as card3,
	c.master_id as master_id,
	c.name as char_name,
	ifnull(g.name, "") as guild,
	"Cart Inventory" as location,
	cinv.amount as count
	FROM server.cart_inventory cinv
	JOIN server.`char` c ON c.char_id = cinv.char_id
	LEFT JOIN server.guild g ON g.guild_id = c.guild_id
	JOIN server.item_db ib ON ib.id = cinv.nameid
	LEFT JOIN server.item_db ib0 ON ib0.id=cinv.card0 and ib0.`type`=6
	LEFT JOIN server.item_db ib1 ON ib1.id=cinv.card1 and ib1.`type`=6
	LEFT JOIN server.item_db ib2 ON ib2.id=cinv.card2 and ib2.`type`=6
	LEFT JOIN server.item_db ib3 ON ib3.id=cinv.card3 and ib3.`type`=6
	WHERE FIND_IN_SET(cinv.nameid, @cardID) != 0
		OR FIND_IN_SET(cinv.card0, @cardID) != 0
		OR FIND_IN_SET(cinv.card1, @cardID) != 0
		OR FIND_IN_SET(cinv.card2, @cardID) != 0
		OR FIND_IN_SET(cinv.card3, @cardID) != 0

	UNION ALL

	SELECT ib.name_japanese as item,
	ib0.name_japanese as card0,
	ib1.name_japanese as card1,
	ib2.name_japanese as card2,
	ib3.name_japanese as card3,
	l.master_id as master_id,
	"" as char_name,
	"" as guild,
	"Storage" as location,
	st.amount as count
	FROM server.storage st
	JOIN server.login l ON l.account_id = st.account_id
	JOIN server.item_db ib ON ib.id = st.nameid
	LEFT JOIN server.item_db ib0 ON ib0.id=st.card0 and ib0.`type`=6
	LEFT JOIN server.item_db ib1 ON ib1.id=st.card1 and ib1.`type`=6
	LEFT JOIN server.item_db ib2 ON ib2.id=st.card2 and ib2.`type`=6
	LEFT JOIN server.item_db ib3 ON ib3.id=st.card3 and ib3.`type`=6
	WHERE FIND_IN_SET(st.nameid, @cardID) != 0
		OR FIND_IN_SET(st.card0, @cardID) != 0
		OR FIND_IN_SET(st.card1, @cardID) != 0
		OR FIND_IN_SET(st.card2, @cardID) != 0
		OR FIND_IN_SET(st.card3, @cardID) != 0    

	UNION ALL

	SELECT ib.name_japanese as item,
	ib0.name_japanese as card0,
	ib1.name_japanese as card1,
	ib2.name_japanese as card2,
	ib3.name_japanese as card3,
	"" as master_id,
	"" as char_name,
	g.name as guild,
	"Guild Storage" as location,
	gst.amount as count
	FROM server.guild_storage gst
	LEFT JOIN server.guild g ON g.guild_id = gst.guild_id
	JOIN server.item_db ib ON ib.id = gst.nameid
	LEFT JOIN server.item_db ib0 ON ib0.id=gst.card0 and ib0.`type`=6
	LEFT JOIN server.item_db ib1 ON ib1.id=gst.card1 and ib1.`type`=6
	LEFT JOIN server.item_db ib2 ON ib2.id=gst.card2 and ib2.`type`=6
	LEFT JOIN server.item_db ib3 ON ib3.id=gst.card3 and ib3.`type`=6
	WHERE FIND_IN_SET(gst.nameid, @cardID) != 0
		OR FIND_IN_SET(gst.card0, @cardID) != 0
		OR FIND_IN_SET(gst.card1, @cardID) != 0
		OR FIND_IN_SET(gst.card2, @cardID) != 0
		OR FIND_IN_SET(gst.card3, @cardID) != 0  
		
	UNION ALL

	SELECT ib.name_japanese as item,
	ib0.name_japanese as card0,
	ib1.name_japanese as card1,
	ib2.name_japanese as card2,
	ib3.name_japanese as card3,
	mst.master_id as master_id,
	"" as char_name,
	"" as guild,
	"Storage" as location,
	mst.amount as count
	FROM server.master_storage mst
	JOIN server.item_db ib ON ib.id = mst.nameid
	LEFT JOIN server.item_db ib0 ON ib0.id=mst.card0 and ib0.`type`=6
	LEFT JOIN server.item_db ib1 ON ib1.id=mst.card1 and ib1.`type`=6
	LEFT JOIN server.item_db ib2 ON ib2.id=mst.card2 and ib2.`type`=6
	LEFT JOIN server.item_db ib3 ON ib3.id=mst.card3 and ib3.`type`=6
	WHERE FIND_IN_SET(mst.nameid, @cardID) != 0
		OR FIND_IN_SET(mst.card0, @cardID) != 0
		OR FIND_IN_SET(mst.card1, @cardID) != 0
		OR FIND_IN_SET(mst.card2, @cardID) != 0
		OR FIND_IN_SET(mst.card3, @cardID) != 0;
END;
|        

DELIMITER |

CREATE PROCEDURE getMVPsKilled()
BEGIN
	SELECT md.kName, count(mvp.monster_id) as count
	FROM server_logs.mvplog mvp
	JOIN server.mob_db md ON md.id = mvp.monster_id
	GROUP BY mvp.monster_id
	ORDER BY count DESC;
END;
|        

DELIMITER |

CREATE PROCEDURE getTopMVPKillers()
BEGIN
	SELECT c.name, c.master_id, count(mvp.monster_id) as count
	FROM server_logs.mvplog mvp
	JOIN server.`char` c ON c.char_id = mvp.kill_char_id
	GROUP BY mvp.kill_char_id
	ORDER BY count DESC;
END;
|        

DELIMITER |

CREATE PROCEDURE getTopZenyHolders()
BEGIN
	SELECT l.master_id, FORMAT(sum(z.amount),0) as amount
	FROM server.zeny z
	JOIN server.login l ON l.account_id = z.account_id
	GROUP BY l.master_id
	ORDER BY sum(z.amount) DESC
	LIMIT 20;
END;
|        

DELIMITER |

CREATE PROCEDURE getTopPointsHolders()
BEGIN
	SELECT l.master_id, FORMAT(sum(a.value),0) as amount
	FROM server.acc_reg_num a
	JOIN server.login l ON l.account_id = a.account_id
	WHERE a.`key` = '#CASHPOINTS'
	GROUP BY l.master_id
	ORDER BY sum(a.value) DESC
	LIMIT 20;
END;
|        

DELIMITER |

CREATE PROCEDURE getDonationsCompleteLog()
BEGIN
	SELECT i.id, i.date, i.txn_id, i.complete, i.message, i.paypal_log_id, concat_ws(" ",p.mc_gross, p.mc_currency) as amount, p.account_id, l.master_id, p.payer_email
	FROM server.ipn_log i
	JOIN server.paypal_log p ON i.paypal_log_id = p.id
	LEFT JOIN server.login l ON p.account_id = l.account_id
    WHERE i.complete = 1
	ORDER BY i.date DESC
	LIMIT 1000;
END;
|     

DELIMITER |

CREATE PROCEDURE getDonationsFailLog()
BEGIN
	SELECT i.id, i.date, i.txn_id, i.complete, i.message, i.paypal_log_id, concat_ws(" ",p.mc_gross, p.mc_currency) as amount, p.account_id, l.master_id, p.payer_email
	FROM server.ipn_log i
	JOIN server.paypal_log p ON i.paypal_log_id = p.id
	LEFT JOIN server.login l ON p.account_id = l.account_id
    WHERE i.complete = 0
	ORDER BY i.date DESC
	LIMIT 1000;
END;
|     

DELIMITER |

CREATE PROCEDURE getUsersByState(IN state VARCHAR(45))
BEGIN
	SELECT m.master_id as master_id,
    m.registration_time as date,
    m.email as email
	FROM live_server.master_account m
    WHERE m.state = state
	ORDER BY m.registration_time DESC
    LIMIT 500;
END;
|